# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Javi Polo <javipolo@cyberjunkie.com>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-04-23 17:50+0200\n"
"PO-Revision-Date: 2021-06-07 00:16+0200\n"
"Last-Translator: Javi Polo <javipolo@cyberjunkie.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ZMORE"
msgstr "ZMORE"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "zmore - file perusal filter for crt viewing of compressed text"
msgstr ""
"zmore - Filtro para la visualización detallada de ficheros de texto "
"comprimidos a través de la pantalla"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<zmore> [ name ...  ]"
msgstr "B<zmore> [ nombre ...  ]"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<Zmore> is a filter which allows examination of compressed or plain text "
#| "files one screenful at a time on a soft-copy terminal.  I<zmore> works on "
#| "files compressed with I<compress>, I<pack> or I<gzip>, and also on "
#| "uncompressed files.  If a file does not exist, I<zmore> looks for a file "
#| "of the same name with the addition of a .gz, .z or .Z suffix."
msgid ""
"The I<zmore> command is a filter which allows examination of compressed or "
"plain text files one screenful at a time on a soft-copy terminal.  The "
"B<zmore> command works on files compressed with B<compress>, B<pack> or "
"B<gzip>, and also on uncompressed files.  If a file does not exist, B<zmore> "
"looks for a file of the same name with the addition of a .gz, .z or .Z "
"suffix."
msgstr ""
"I<Zmore> es un filtro que permite el examen de ficheros comprimidos o en "
"texto puro de forma que se para al mostrar una pantalla completa en una "
"terminal.  I<zmore> funciona en ficheros comprimidos con I<compress>, "
"I<pack> o I<gzip>, y tambien en ficheros sin comprimir.  Si un fichero no "
"existe, I<zmore> bisca un fichero con el mismo nombre, añadiendo la "
"extensión .gz, .z o .Z ."

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<Zmore> normally pauses after each screenful, printing --More-- at the "
#| "bottom of the screen.  If the user then types a carriage return, one more "
#| "line is displayed.  If the user hits a space, another screenful is "
#| "displayed.  Other possibilities are enumerated later."
msgid ""
"The B<zmore> command normally pauses after each screenful, printing --More-- "
"at the bottom of the screen.  If the user then types a carriage return, one "
"more line is displayed.  If the user hits a space, another screenful is "
"displayed.  Other possibilities are enumerated later."
msgstr ""
"I<Zmore> normalmente se para tras cada pantalla completa mostrada, indicando "
"--More-- al final de la pantalla.  Si el usuario entonces envía un retorno "
"de carro, se muestra una línea más.  Si el usuario envía un espacio, se "
"muestra otra pantalla completa. Luego enumeramos otras posibilidades."

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "I<Zmore> looks in the file I</etc/termcap> to determine terminal "
#| "characteristics, and to determine the default window size.  On a terminal "
#| "capable of displaying 24 lines, the default window size is 22 lines.  To "
#| "use a pager other than the default I<more>, set environment variable "
#| "PAGER to the name of the desired program, such as I<less>."
msgid ""
"The B<zmore> command looks in the file I</etc/termcap> to determine terminal "
"characteristics, and to determine the default window size.  On a terminal "
"capable of displaying 24 lines, the default window size is 22 lines.  To use "
"a pager other than the default B<more>, set environment variable PAGER to "
"the name of the desired program, such as B<less>."
msgstr ""
"I<Zmore> busca en el fichero I</etc/termcap> para determinar las "
"características de la terminal, y para determinar el tamaño por defecto de "
"la ventana.  En una terminal con capacidad para mostrar 24 líneas, el tamaño "
"por defecto de la ventana es de 22 líneas.  Para usar otro paginador "
"distinto del que se usa por defecto, I<more,> indícalo poniendole a la "
"variable de entorno PAGER el nombre del programa deseado, como por ejemplo "
"I<less>."

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "Other sequences which may be typed when I<zmore> pauses, and their "
#| "effects, are as follows (I<i> is an optional integer argument, defaulting "
#| "to 1) :"
msgid ""
"Other sequences which may be typed when B<zmore> pauses, and their effects, "
"are as follows (I<i> is an optional integer argument, defaulting to 1) :"
msgstr ""
"Otras secuencias que pueden enviarse cuando I<zmore> se para, y sus efectos, "
"son las siguientes (I<i> es un parámetro entero opcional, por defecto 1) :"

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>E<lt>spaceE<gt>"
msgstr "I<i\\^>E<lt>espacioE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "display I<i> more lines, (or another screenful if no argument is given)"
msgstr ""
"muestra I<i> líneas más, (o otra pantalla completa si no se indica ningún "
"parámetro)"

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "^D"
msgstr "^D"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"display 11 more lines (a ``scroll'').  If I<i> is given, then the scroll "
"size is set to I<i>."
msgstr ""
"muestra 11 líneas más (un ``scroll'').  Si I<i> se indíca, entonces el "
"tamaño del scroll se cambia a I<i>."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "d"
msgstr "d"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "same as ^D (control-D)"
msgstr "igual que ^D (control-D)"

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>z"
msgstr "I<i\\^>z"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "same as typing a space except that I<i\\|>, if present, becomes the new "
#| "window size.  Note that the window size reverts back to the default at "
#| "the end of the current file."
msgid ""
"same as typing a space except that I<i>, if present, becomes the new window "
"size."
msgstr ""
"igual que escribir un espacio, excepto que I<i\\|>, si existe, se convierte "
"en el nuevo tamaño de la ventana. Notese que el tamaño de la ventana vuelve "
"a ser el mismo que el tamaño por defecto al finalizar el fichero actual."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>s"
msgstr "I<i\\^>s"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "skip I<i> lines and print a screenful of lines"
msgstr "salta I<i> líneas y muestra una pantalla completa de líneas."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>f"
msgstr "I<i\\^>f"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "skip I<i> screenfuls and print a screenful of lines"
msgstr ""
"salta I<i> pantallas completas y muestrauna pantalla completa de líneas."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "q or Q"
msgstr "q o Q"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Quit."
msgstr ""

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "="
msgstr "="

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Display the current line number."
msgstr "Muestra el número de línea actual."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i>/expr"
msgstr "I<i>/expr"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "search for the I<i\\|>-th occurrence of the regular expression I<expr.> "
#| "If the pattern is not found, I<zmore> goes on to the next file (if any).  "
#| "Otherwise, a screenful is displayed, starting two lines before the place "
#| "where the expression was found.  The user's erase and kill characters may "
#| "be used to edit the regular expression.  Erasing back past the first "
#| "column cancels the search command."
msgid ""
"search for the I<i\\^>-th occurrence of the regular expression I<expr.> The "
"user's erase and kill characters may be used to edit the regular "
"expression.  Erasing back past the first column cancels the search command."
msgstr ""
"busca la cadena número I<i\\|> de la expresión regular I<expr.> Si no se "
"encuentra la cadena (patrón), I<zmore> pasa al siguiente fichero (si hay).  "
"En caso contrario, se muestra una pantalla completa, empezando dos líneas "
"antes de la línea donde se encontró la expresión.  Los carácteres de usuario "
"borrar y matar (kill) se pueden usar para editar la expresión regular.  "
"Borrar pasada la primera columna cancela la orden de búsqueda."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<i\\^>n"
msgstr "I<i\\^>n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"search for the I<i\\^>-th occurrence of the last regular expression entered."
msgstr ""
"busca la cadena número I<i\\^> de la última expresión regular indicada."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "!command"
msgstr "!orden"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"invoke a shell with I<command>.  The character `!' in \"command\" is "
"replaced with the previous shell command.  The sequence \"\\e!\" is replaced "
"by \"!\"."
msgstr ""
"invoca una shell con I<orden>.  El carácter `!' en \"orden \" se sustituye "
"por la última orden de la shell.  La secuencia \"\\e!\" se sustituye por \"!"
"\"."

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ":q or :Q"
msgstr ":q o :Q"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Quit (same as q or Q)."
msgstr ""

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "."
msgstr "."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "(dot) repeat the previous command."
msgstr "(punto) repite la orden anterior."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The commands take effect immediately, i.e., it is not necessary to type a "
"carriage return.  Up to the time when the command character itself is given, "
"the user may hit the line kill character to cancel the numerical argument "
"being formed.  In addition, the user may hit the erase character to "
"redisplay the --More-- message."
msgstr ""
"Las órdenes tienen efecto inmediatamente, es decir, no es necesario indicar "
"el final de una orden con un retorno de carro.  Al tiempo que el carácter de "
"la orden se da, el usuario puede enviar el carácter de matar la línea (line "
"kill) para cancelar el parámetro numérico que se está formando.  Además, el "
"usuario puede enviar el carácter de borrar, para volver a mostrar el mensaje "
"--More--."

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "At any time when output is being sent to the terminal, the user can hit "
#| "the quit key (normally control-\\e).  I<Zmore> will stop sending output, "
#| "and will display the usual --More-- prompt.  The user may then enter one "
#| "of the above commands in the normal manner.  Unfortunately, some output "
#| "is lost when this is done, due to the fact that any characters waiting in "
#| "the terminal's output queue are flushed when the quit signal occurs."
msgid ""
"At any time when output is being sent to the terminal, the user can hit the "
"quit key (normally control-\\e).  The I<zmore> command will stop sending "
"output, and will display the usual --More-- prompt.  The user may then enter "
"one of the above commands in the normal manner.  Unfortunately, some output "
"is lost when this is done, due to the fact that any characters waiting in "
"the terminal's output queue are flushed when the quit signal occurs."
msgstr ""
"En cualquier momento cuando se está enviando algo hacia la terminal, el "
"usuario puede enviar la tecla de salir (normalmente control-\\e).  I<Zmore> "
"parará de enviar a la terminal, y mostrará el típico mensaje --More--.  El "
"usuario puede entonces mandar cualquiera de las órdenes indicados arriba de "
"forma normal.  Desafortunadamente, algúnos carácteres enviados a la terminal "
"se pierden cuando se hace esto, debido a que los carácteres que estaban "
"esperando en la cola de salida de la terminal se borran cuando se produce la "
"señal de salir."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The terminal is set to I<noecho> mode by this program so that the output can "
"be continuous.  What you type will thus not show on your terminal, except "
"for the / and ! commands."
msgstr ""
"El programa cambia la terminal a modo I<noecho> para que la salida de "
"carácteres pueda ser continua.  Lo que escribes entoces no se mostrará en la "
"terminal, excepto las órdenes / y ! ."

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "If the standard output is not a teletype, then I<zmore> acts just like "
#| "I<zcat,> except that a header is printed before each file."
msgid ""
"If the standard output is not a teletype, then B<zmore> acts just like "
"B<zcat>, except that a header is printed before each file if there is more "
"than one file."
msgstr ""
"Si la salida estandard no es una teletype, I<zmore> se comporta como zcat "
"I<zcat,> excepto porque antes de cada fichero se imprime una cabecera."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "ARCHIVOS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "/etc/termcap"
msgstr "/etc/termcap"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Terminal data base"
msgstr "Base de datos de las teminales"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VÉASE TAMBIÉN"

#. type: Plain text
#: archlinux debian-unstable fedora-rawhide mageia-cauldron opensuse-tumbleweed
#, fuzzy
#| msgid "more(1), gzip(1), zdiff(1), zgrep(1), znew(1), zforce(1), gzexe(1)"
msgid ""
"B<more>(1), B<gzip>(1), B<zdiff>(1), B<zgrep>(1), B<znew>(1), B<zforce>(1), "
"B<gzexe>(1)"
msgstr "more(1), gzip(1), zdiff(1), zgrep(1), znew(1), zforce(1), gzexe(1)"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"I<Zmore> is a filter which allows examination of compressed or plain text "
"files one screenful at a time on a soft-copy terminal.  I<zmore> works on "
"files compressed with I<compress>, I<pack> or I<gzip>, and also on "
"uncompressed files.  If a file does not exist, I<zmore> looks for a file of "
"the same name with the addition of a .gz, .z or .Z suffix."
msgstr ""
"I<Zmore> es un filtro que permite el examen de ficheros comprimidos o en "
"texto puro de forma que se para al mostrar una pantalla completa en una "
"terminal.  I<zmore> funciona en ficheros comprimidos con I<compress>, "
"I<pack> o I<gzip>, y tambien en ficheros sin comprimir.  Si un fichero no "
"existe, I<zmore> bisca un fichero con el mismo nombre, añadiendo la "
"extensión .gz, .z o .Z ."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"I<Zmore> normally pauses after each screenful, printing --More-- at the "
"bottom of the screen.  If the user then types a carriage return, one more "
"line is displayed.  If the user hits a space, another screenful is "
"displayed.  Other possibilities are enumerated later."
msgstr ""
"I<Zmore> normalmente se para tras cada pantalla completa mostrada, indicando "
"--More-- al final de la pantalla.  Si el usuario entonces envía un retorno "
"de carro, se muestra una línea más.  Si el usuario envía un espacio, se "
"muestra otra pantalla completa. Luego enumeramos otras posibilidades."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"I<Zmore> looks in the file I</etc/termcap> to determine terminal "
"characteristics, and to determine the default window size.  On a terminal "
"capable of displaying 24 lines, the default window size is 22 lines.  To use "
"a pager other than the default I<more>, set environment variable PAGER to "
"the name of the desired program, such as I<less>."
msgstr ""
"I<Zmore> busca en el fichero I</etc/termcap> para determinar las "
"características de la terminal, y para determinar el tamaño por defecto de "
"la ventana.  En una terminal con capacidad para mostrar 24 líneas, el tamaño "
"por defecto de la ventana es de 22 líneas.  Para usar otro paginador "
"distinto del que se usa por defecto, I<more,> indícalo poniendole a la "
"variable de entorno PAGER el nombre del programa deseado, como por ejemplo "
"I<less>."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"Other sequences which may be typed when I<zmore> pauses, and their effects, "
"are as follows (I<i> is an optional integer argument, defaulting to 1) :"
msgstr ""
"Otras secuencias que pueden enviarse cuando I<zmore> se para, y sus efectos, "
"son las siguientes (I<i> es un parámetro entero opcional, por defecto 1) :"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"At any time when output is being sent to the terminal, the user can hit the "
"quit key (normally control-\\e).  I<Zmore> will stop sending output, and "
"will display the usual --More-- prompt.  The user may then enter one of the "
"above commands in the normal manner.  Unfortunately, some output is lost "
"when this is done, due to the fact that any characters waiting in the "
"terminal's output queue are flushed when the quit signal occurs."
msgstr ""
"En cualquier momento cuando se está enviando algo hacia la terminal, el "
"usuario puede enviar la tecla de salir (normalmente control-\\e).  I<Zmore> "
"parará de enviar a la terminal, y mostrará el típico mensaje --More--.  El "
"usuario puede entonces mandar cualquiera de las órdenes indicados arriba de "
"forma normal.  Desafortunadamente, algúnos carácteres enviados a la terminal "
"se pierden cuando se hace esto, debido a que los carácteres que estaban "
"esperando en la cola de salida de la terminal se borran cuando se produce la "
"señal de salir."

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, fuzzy
#| msgid ""
#| "If the standard output is not a teletype, then I<zmore> acts just like "
#| "I<zcat,> except that a header is printed before each file."
msgid ""
"If the standard output is not a teletype, then I<zmore> acts just like "
"I<zcat>, except that a header is printed before each file if there is more "
"than one file."
msgstr ""
"Si la salida estandard no es una teletype, I<zmore> se comporta como zcat "
"I<zcat,> excepto porque antes de cada fichero se imprime una cabecera."

#. type: Plain text
#: debian-bullseye fedora-36 opensuse-leap-15-4
msgid "more(1), gzip(1), zdiff(1), zgrep(1), znew(1), zforce(1), gzexe(1)"
msgstr "more(1), gzip(1), zdiff(1), zgrep(1), znew(1), zforce(1), gzexe(1)"
