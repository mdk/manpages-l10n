# Spanish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Javi Polo <javipolo@cyberjunkie.com>, 1998.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-06-16 17:17+0200\n"
"PO-Revision-Date: 2021-06-07 00:37+0200\n"
"Last-Translator: Javi Polo <javipolo@cyberjunkie.com>\n"
"Language-Team: Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "mrd"
msgstr "mrd"

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, no-wrap
msgid "04Jun22"
msgstr ""

#. type: TH
#: archlinux fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "mtools-4.0.39"
msgid "mtools-4.0.40"
msgstr "mtools-4.0.39"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Name"
msgstr "Nombre"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "mrd - remove an MSDOS subdirectory"
msgstr "mrd - Borra un subdirectorio MS-DOS"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Note\\ of\\ warning"
msgstr "Nota\\ de\\ advertencia"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation, and may not be entirely accurate or complete.  See the end of "
"this man page for details."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Description"
msgstr "Descripción"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The \\&CW<mrd> command is used to remove an MS-DOS subdirectory. Its syntax "
"is:"
msgstr ""
"La orden \\&CW<mrd> se usa para borrar subdirectorios de MS-DOS. Su sintáxis "
"es:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I<\\&>\\&CW<mrd> [\\&CW<-v>] I<msdosdirectory> [ I<msdosdirectories>\\&... ]\n"
msgstr "I<\\&>\\&CW<mrd> [\\&CW<-v>] I<directoriomsdos> [ I<directoriosmsdos>\\&... ]\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"\\&\\&CW<Mrd> removes a directory from an MS-DOS file system. An error "
"occurs if the directory does not exist or is not empty."
msgstr ""
"\\&\\&CW<Mrd> borra un directorio de un sistema de ficheros MS-DOS. Si el "
"directorio no existe o no está vacío se produce un error."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "See\\ Also"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Mtools' texinfo doc"
msgstr "Documentación texinfo de Mtools"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Viewing\\ the\\ texi\\ doc"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This manpage has been automatically generated from mtools's texinfo "
"documentation. However, this process is only approximative, and some items, "
"such as crossreferences, footnotes and indices are lost in this translation "
"process.  Indeed, these items have no appropriate representation in the "
"manpage format.  Moreover, not all information has been translated into the "
"manpage version.  Thus I strongly advise you to use the original texinfo "
"doc.  See the end of this manpage for instructions how to view the texinfo "
"doc."
msgstr ""
"Esta página de manual ha sido generada automáticamente a partir de la "
"documentación texinfo de mtools. Sin embargo, el proceso es solo aproximado, "
"y algunos elementos tales como referencias cruzadas, notas al pie e índices, "
"se pierden en este proceso de traducción.  De hecho, estos elementos no "
"tienen una representación adecuada en el formato de las páginas del manual. "
"Por otra parte, solo se han traducido los elemntos específicos de cada "
"orden, y se ha desechado de la versión de la página del manual la "
"información general de mtools. Por este motivo, le aviso encarecidamente que "
"use el documento texinfo original."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "* \\ \\ "
msgstr "* \\ \\ "

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"To generate a printable copy from the texinfo doc, run the following "
"commands:"
msgstr ""
"Para generar un copia imprimible del documento texinfo use las siguientes "
"órdenes:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make dvi; dvips mtools.dvi>\n"
msgstr "B<    ./configure; make dvi; dvips mtools.dvi>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "To generate a html copy, run:"
msgstr "Para generar una copia html, ejecute:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make html>\n"
msgstr "B<    ./configure; make html>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"\\&A premade html can be found at \\&\\&CW<\\(ifhttp://www.gnu.org/software/"
"mtools/manual/mtools.html\\(is>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "To generate an info copy (browsable using emacs' info mode), run:"
msgstr ""
"Para generar un copia info (visualizable usando el modo info de emacs), "
"ejecute:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<    ./configure; make info>\n"
msgstr "B<    ./configure; make info>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The texinfo doc looks most pretty when printed or as html.  Indeed, in the "
"info version certain examples are difficult to read due to the quoting "
"conventions used in info."
msgstr ""
"El documento texinfo parece más bonito en html o cuando se imprime. De "
"hecho, la versión info contiene ciertos ejemplos que son difíciles de leer "
"debido a las convenciones de notación usadas en info."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "28Nov20"
msgstr "28 de noviembre de 2020"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "mtools-4.0.26"
msgstr "mtools-4.0.26"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "10Jul21"
msgstr "10 de julio de 2021"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "mtools-4.0.32"
msgstr "mtools-4.0.32"

#. type: TH
#: fedora-36
#, no-wrap
msgid "03Mar22"
msgstr "3 de marzo de 2022"

#. type: TH
#: fedora-36
#, no-wrap
msgid "mtools-4.0.38"
msgstr "mtools-4.0.38"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "06Aug21"
msgstr "6 de agosto de 2021"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "mtools-4.0.35"
msgstr "mtools-4.0.35"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "10Apr22"
msgstr ""

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "mtools-4.0.39"
msgstr "mtools-4.0.39"
