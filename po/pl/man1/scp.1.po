# Polish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Przemek Borys <pborys@dione.ids.pl>, 2000.
# Robert Luberda <robert@debian.org>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-pl\n"
"POT-Creation-Date: 2022-04-23 17:38+0200\n"
"PO-Revision-Date: 2014-10-31 10:03+0100\n"
"Last-Translator: Robert Luberda <robert@debian.org>\n"
"Language-Team: Polish <manpages-pl-list@lists.sourceforge.net>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Lokalize 1.5\n"

#. type: Dd
#: archlinux debian-unstable
#, fuzzy, no-wrap
#| msgid "$Mdocdate: April 30 2020 $"
msgid "$Mdocdate: February 23 2022 $"
msgstr "$Mdocdate: April 30 2020 $"

#. type: Dt
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SCP 1"
msgstr "SCP 1"

#. type: Sh
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr "NAZWA"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid "E<.Nm scp>"
msgstr "E<.Nm scp>"

#. type: Nd
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "OpenSSH secure file copy"
msgstr ""

#. type: Sh
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr "SKŁADNIA"

#. type: Plain text
#: archlinux debian-unstable
#, fuzzy
#| msgid ""
#| "E<.Nm scp> E<.Bk -words> E<.Op Fl 12346BCpqrv> E<.Op Fl c Ar cipher> E<."
#| "Op Fl F Ar ssh_config> E<.Op Fl i Ar identity_file> E<.Op Fl l Ar limit> "
#| "E<.Op Fl o Ar ssh_option> E<.Op Fl P Ar port> E<.Op Fl S Ar program> E<."
#| "Sm off> E<.Oo> E<.Op Ar user No @> E<.Ar host1 No>: E<.Oc Ar file1> E<.Sm "
#| "on> E<.Ar ...> E<.Sm off> E<.Oo> E<.Op Ar user No @> E<.Ar host2 No>: E<."
#| "Oc Ar file2> E<.Sm on> E<.Ek>"
msgid ""
"E<.Nm scp> E<.Op Fl 346ABCOpqRrsTv> E<.Op Fl c Ar cipher> E<.Op Fl D Ar "
"sftp_server_path> E<.Op Fl F Ar ssh_config> E<.Op Fl i Ar identity_file> E<."
"Op Fl J Ar destination> E<.Op Fl l Ar limit> E<.Op Fl o Ar ssh_option> E<.Op "
"Fl P Ar port> E<.Op Fl S Ar program> E<.Ar source ... target>"
msgstr ""
"E<.Nm scp> E<.Bk -words> E<.Op Fl 12346BCpqrv> E<.Op Fl c Ar szyfr> E<.Op Fl "
"F Ar plik_konfig_ssh> E<.Op Fl i Ar plik_tożsamości> E<.Op Fl l Ar limit> E<."
"Op Fl o Ar opcja_ssh> E<.Op Fl P Ar port> E<.Op Fl S Ar program> E<.Sm off> "
"E<.Oo> E<.Op Ar użytkownik No @> E<.Ar komp1 No>: E<.Oc Ar plik1> E<.Sm on> "
"E<.Ar ...> E<.Sm off> E<.Oo> E<.Op Ar użytkownik No @> E<.Ar komp2 No>: E<."
"Oc Ar plik2> E<.Sm on> E<.Ek>"

#. type: Sh
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr "OPIS"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid "E<.Nm> copies files between hosts on a network."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "E<.Nm> copies files between hosts on a network.  It uses E<.Xr ssh 1> for "
#| "data transfer, and uses the same authentication and provides the same "
#| "security as E<.Xr ssh 1>.  E<.Nm> will ask for passwords or passphrases "
#| "if they are needed for authentication."
msgid ""
"It uses E<.Xr ssh 1> for data transfer, and uses the same authentication and "
"provides the same security as a login session."
msgstr ""
"E<.Nm> kopiuje pliki między komputerami w sieci. Do transferu danych używa "
"E<.Xr ssh 1> i wykorzystuje tę samą autoryzację oraz daje takie samo "
"bezpieczeństwo jak E<.Xr ssh 1>. E<.Nm> pyta w razie potrzeby o hasła "
"uwierzytelniające."

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"E<.Nm> will ask for passwords or passphrases if they are needed for "
"authentication."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"The E<.Ar source> and E<.Ar target> may be specified as a local pathname, a "
"remote host with optional path in the form E<.Sm off> E<.Oo user @ Oc host : "
"Op path>, E<.Sm on> or a URI in the form E<.Sm off> E<.No scp:// Oo user @ "
"Oc host Oo : port Oc Op / path>.  E<.Sm on> Local file names can be made "
"explicit using absolute or relative pathnames to avoid E<.Nm> treating file "
"names containing E<.Sq :\\&> as host specifiers."
msgstr ""

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"When copying between two remote hosts, if the URI format is used, a E<.Ar "
"port> cannot be specified on the E<.Ar target> if the E<.Fl R> option is "
"used."
msgstr ""

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid "The options are as follows:"
msgstr "Dostępne są następujące opcje:"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl 3"
msgstr "Fl 3"

#. type: Plain text
#: archlinux debian-unstable
#, fuzzy
#| msgid ""
#| "Copies between two remote hosts are transferred through the local host.  "
#| "Without this option the data is copied directly between the two remote "
#| "hosts.  Note that this option disables the progress meter."
msgid ""
"Copies between two remote hosts are transferred through the local host.  "
"Without this option the data is copied directly between the two remote "
"hosts.  Note that, when using the legacy SCP protocol (via the E<.Fl O> "
"flag), this option selects batch mode for the second host as E<.Nm> cannot "
"ask for passwords or passphrases for both hosts.  This mode is the default."
msgstr ""
"Kopiowanie pomiędzy dwoma zdalnymi komputerami jest wykonywane poprzez "
"komputer lokalny. Bez tej opcji dane są kopiowane bezpośrednio pomiędzy "
"dwoma komputerami. Proszę zauważyć, że ta opcja wyłącza pasek postępu."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl 4"
msgstr "Fl 4"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid "Forces E<.Nm> to use IPv4 addresses only."
msgstr "Wymusza na E<.Nm> używanie tylko adresów IPv4."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl 6"
msgstr "Fl 6"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid "Forces E<.Nm> to use IPv6 addresses only."
msgstr "Wymusza na E<.Nm> używanie tylko adresów IPv6."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl A"
msgstr "Fl A"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Allows forwarding of E<.Xr ssh-agent 1> to the remote system.  The default "
"is not to forward an authentication agent."
msgstr ""

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl B"
msgstr "Fl B"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid "Selects batch mode (prevents asking for passwords or passphrases)."
msgstr "Wybiera tryb wsadowy (nie pyta o hasła i frazy kodujące)."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl C"
msgstr "Fl C"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Compression enable.  Passes the E<.Fl C> flag to E<.Xr ssh 1> to enable "
"compression."
msgstr ""
"Włączenie kompresji. Przekazuje flagę E<.Fl C> do programu E<.Xr ssh 1>, "
"włączającą kompresję danych."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl c Ar cipher"
msgstr "Fl c Ar szyfr"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Selects the cipher to use for encrypting the data transfer.  This option is "
"directly passed to E<.Xr ssh 1>."
msgstr ""
"Wybiera szyfr używany do kodowania danych. Opcja ta jest przekazywana "
"bezpośrednio do E<.Xr ssh 1>."

#. type: It
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Fl D Ar sftp_server_path"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"When using the SFTP protocol support via E<.Fl M>, connect directly to a "
"local SFTP server program rather than a remote one via E<.Xr ssh 1>.  This "
"option may be useful in debugging the client and server."
msgstr ""

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl F Ar ssh_config"
msgstr "Fl F Ar plik_konfig_ssh"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Specifies an alternative per-user configuration file for E<.Nm ssh>.  This "
"option is directly passed to E<.Xr ssh 1>."
msgstr ""
"Określa alternatywny plik konfiguracyjny E<.Nm ssh>. Ta opcja jest "
"bezpośrednio przekazywana do E<.Xr ssh 1>."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl i Ar identity_file"
msgstr "Fl i Ar plik_tożsamości"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Selects the file from which the identity (private key) for public key "
"authentication is read.  This option is directly passed to E<.Xr ssh 1>."
msgstr ""
"Wybiera plik, z którego odczytywana jest tożsamość (klucz prywatny) dla "
"uwierzytelnienia za pomocą klucza publicznego. Opcja ta jest przekazywana "
"bezpośrednio do E<.Xr ssh 1>."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, fuzzy, no-wrap
#| msgid "Fl i Ar identity_file"
msgid "Fl J Ar destination"
msgstr "Fl i Ar plik_tożsamości"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Connect to the target host by first making an E<.Nm> connection to the jump "
"host described by E<.Ar destination> and then establishing a TCP forwarding "
"to the ultimate destination from there.  Multiple jump hops may be specified "
"separated by comma characters.  This is a shortcut to specify a E<.Cm "
"ProxyJump> configuration directive.  This option is directly passed to E<.Xr "
"ssh 1>."
msgstr ""

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl l Ar limit"
msgstr "Fl l Ar limit"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid "Limits the used bandwidth, specified in Kbit/s."
msgstr "Określa maksymalną prędkość transferu danych podaną w Kbit/s."

#. type: It
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Fl O"
msgstr "Fl O"

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Use the legacy SCP protocol for file transfers instead of the SFTP "
"protocol.  Forcing the use of the SCP protocol may be necessary for servers "
"that do not implement SFTP, for backwards-compatibility for particular "
"filename wildcard patterns and for expanding paths with a E<.Sq ~> prefix "
"for older SFTP servers."
msgstr ""

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl o Ar ssh_option"
msgstr "Fl o Ar opcje_ssh"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Can be used to pass options to E<.Nm ssh> in the format used in E<.Xr "
"ssh_config 5>.  This is useful for specifying options for which there is no "
"separate E<.Nm scp> command-line flag.  For full details of the options "
"listed below, and their possible values, see E<.Xr ssh_config 5>."
msgstr ""
"Może być użyte do przekazania opcji programowi E<.Nm ssh> w formacie "
"opisanym w E<.Xr ssh_config 5>. Opcja ta jest użyteczna do przekazywania "
"opcji, dla których nie ma osobnej flagi linii poleceń w programie E<.Nm "
"scp>. Szczegółowe informacje o podanych niżej opcjach i ich możliwych "
"wartościach można znaleźć w podręczniku E<.Xr ssh_config 5>."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "AddressFamily"
msgstr "AddressFamily"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "BatchMode"
msgstr "BatchMode"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "BindAddress"
msgstr "BindAddress"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "BindInterface"
msgstr "BindInterface"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CanonicalDomains"
msgstr "CanonicalDomains"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CanonicalizeFallbackLocal"
msgstr "CanonicalizeFallbackLocal"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CanonicalizeHostname"
msgstr "CanonicalizeHostname"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CanonicalizeMaxDots"
msgstr "CanonicalizeMaxDots"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CanonicalizePermittedCNAMEs"
msgstr "CanonicalizePermittedCNAMEs"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CASignatureAlgorithms"
msgstr "CASignatureAlgorithms"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CertificateFile"
msgstr "CertificateFile"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "CheckHostIP"
msgstr "CheckHostIP"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Ciphers"
msgstr "Ciphers"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Compression"
msgstr "Compression"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ConnectionAttempts"
msgstr "ConnectionAttempts"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ConnectTimeout"
msgstr "ConnectTimeout"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ControlMaster"
msgstr "ControlMaster"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ControlPath"
msgstr "ControlPath"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ControlPersist"
msgstr "ControlPersist"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "GlobalKnownHostsFile"
msgstr "GlobalKnownHostsFile"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "GSSAPIAuthentication"
msgstr "GSSAPIAuthentication"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "GSSAPIDelegateCredentials"
msgstr "GSSAPIDelegateCredentials"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "HashKnownHosts"
msgstr "HashKnownHosts"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Host"
msgstr "Host"

#. type: It
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "HostbasedAcceptedAlgorithms"
msgstr "HostbasedAcceptedAlgorithms"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "HostbasedAuthentication"
msgstr "HostbasedAuthentication"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "HostKeyAlgorithms"
msgstr "HostKeyAlgorithms"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "HostKeyAlias"
msgstr "HostKeyAlias"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Hostname"
msgstr "Hostname"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "IdentitiesOnly"
msgstr "IdentitiesOnly"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "IdentityAgent"
msgstr "IdentityAgent"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "IdentityFile"
msgstr "IdentityFile"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "IPQoS"
msgstr "IPQoS"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "KbdInteractiveAuthentication"
msgstr "KbdInteractiveAuthentication"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "KbdInteractiveDevices"
msgstr "KbdInteractiveDevices"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "KexAlgorithms"
msgstr "KexAlgorithms"

#. type: It
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "KnownHostsCommand"
msgstr "KnownHostsCommand"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "LogLevel"
msgstr "LogLevel"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "MACs"
msgstr "MACs"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NoHostAuthenticationForLocalhost"
msgstr "NoHostAuthenticationForLocalhost"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "NumberOfPasswordPrompts"
msgstr "NumberOfPasswordPrompts"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "PasswordAuthentication"
msgstr "PasswordAuthentication"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "PKCS11Provider"
msgstr "PKCS11Provider"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Port"
msgstr "Port"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "PreferredAuthentications"
msgstr "PreferredAuthentications"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ProxyCommand"
msgstr "ProxyCommand"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ProxyJump"
msgstr "ProxyJump"

#. type: It
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "PubkeyAcceptedAlgorithms"
msgstr "PubkeyAcceptedAlgorithms"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "PubkeyAuthentication"
msgstr "PubkeyAuthentication"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "RekeyLimit"
msgstr "RekeyLimit"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SendEnv"
msgstr "SendEnv"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ServerAliveInterval"
msgstr "ServerAliveInterval"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "ServerAliveCountMax"
msgstr "ServerAliveCountMax"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SetEnv"
msgstr "SetEnv"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "StrictHostKeyChecking"
msgstr "StrictHostKeyChecking"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "TCPKeepAlive"
msgstr "TCPKeepAlive"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "UpdateHostKeys"
msgstr "UpdateHostKeys"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "User"
msgstr "User"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "UserKnownHostsFile"
msgstr "UserKnownHostsFile"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "VerifyHostKeyDNS"
msgstr "VerifyHostKeyDNS"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl P Ar port"
msgstr "Fl P Ar port"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Specifies the port to connect to on the remote host.  Note that this "
#| "option is written with a capital E<.Sq P>, because E<.Fl p> is already "
#| "reserved for preserving the times and modes of the file."
msgid ""
"Specifies the port to connect to on the remote host.  Note that this option "
"is written with a capital E<.Sq P>, because E<.Fl p> is already reserved for "
"preserving the times and mode bits of the file."
msgstr ""
"Podaje port, do którego należy się podłączyć na zdalnym hoście. Proszę "
"zauważyć, że opcja ta jest napisana jako wielkie E<.Sq P>, gdyż E<.Fl p> już "
"jest zarezerwowane dla zachowywania czasów modyfikacji i praw dostępu plików."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl p"
msgstr "Fl p"

#
#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Preserves modification times, access times, and modes from the original "
#| "file."
msgid ""
"Preserves modification times, access times, and file mode bits from the "
"source file."
msgstr "Zachowuje czasy modyfikacji oraz dostępu i prawa oryginalnego pliku."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl q"
msgstr "Fl q"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Quiet mode: disables the progress meter as well as warning and diagnostic "
"messages from E<.Xr ssh 1>."
msgstr ""
"Tryb cichy: wyłącza pasek postępu oraz ostrzeżenia i komunikaty "
"diagnostyczne z programu E<.Xr ssh 1>."

#. type: It
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "Fl R"
msgstr "Fl R"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"Copies between two remote hosts are performed by connecting to the origin "
"host and executing E<.Nm> there.  This requires that E<.Nm> running on the "
"origin host can authenticate to the destination host without requiring a "
"password."
msgstr ""

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl r"
msgstr "Fl r"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Recursively copy entire directories.  Note that E<.Nm> follows symbolic "
"links encountered in the tree traversal."
msgstr ""
"Rekursywnie kopiuje całe katalogi. Proszę zauważyć, że E<.Nm> podąża za "
"dowiązaniami symbolicznymi napotkanymi podczas przechodzenia po drzewie "
"katalogów."

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl S Ar program"
msgstr "Fl S Ar program"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Name of E<.Ar program> to use for the encrypted connection.  The program "
"must understand E<.Xr ssh 1> options."
msgstr ""
"Nazwa E<.Ar programu> używanego do tworzenia zakodowanego połączenia.  "
"Program ten musi przyjmować opcje E<.Xr ssh 1>,"

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl T"
msgstr "Fl T"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Disable strict filename checking.  By default when copying files from a "
"remote host to a local directory E<.Nm> checks that the received filenames "
"match those requested on the command-line to prevent the remote end from "
"sending unexpected or unwanted files.  Because of differences in how various "
"operating systems and shells interpret filename wildcards, these checks may "
"cause wanted files to be rejected.  This option disables these checks at the "
"expense of fully trusting that the server will not send unexpected filenames."
msgstr ""

#. type: It
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "Fl v"
msgstr "Fl v"

#
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"Verbose mode.  Causes E<.Nm> and E<.Xr ssh 1> to print debugging messages "
"about their progress.  This is helpful in debugging connection, "
"authentication, and configuration problems."
msgstr ""
"Tryb gadatliwy. Powoduje, że E<.Nm> i E<.Xr ssh 1> drukują komunikaty "
"debugowe o swoim działaniu. Jest to przydatne w diagnozowaniu problemów z "
"połączeniem, autoryzacją i konfiguracją."

#. type: Sh
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "EXIT STATUS"
msgstr "KOD ZAKOŃCZENIA"

# `E<.Ex -std scp>'  seems to be expanded into `
# The scp utility exits 0 on success, and >0 if an error occurs.'
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid "E<.Ex -std scp>"
msgstr ""
"E<.Nm> kończy pracę z kodem 0, jeżeli wszystko odbyło się pomyślnie, lub z "
"kodem E<gt>0 w razie wystąpienia błędu."

#. type: Sh
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr "ZOBACZ TAKŻE"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"E<.Xr sftp 1>, E<.Xr ssh 1>, E<.Xr ssh-add 1>, E<.Xr ssh-agent 1>, E<.Xr ssh-"
"keygen 1>, E<.Xr ssh_config 5>, E<.Xr sftp-server 8>, E<.Xr sshd 8>"
msgstr ""
"E<.Xr sftp 1>, E<.Xr ssh 1>, E<.Xr ssh-add 1>, E<.Xr ssh-agent 1>, E<.Xr ssh-"
"keygen 1>, E<.Xr ssh_config 5>, E<.Xr sftp-server 8>, E<.Xr sshd 8>"

# All that section was initially translated by PB. Readded with minor corrections by MK. --MK
#. type: Sh
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "HISTORY"
msgstr "HISTORIA"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"E<.Nm> is based on the rcp program in E<.Bx> source code from the Regents of "
"the University of California."
msgstr ""
"E<.Nm> jest oparty na programie rcp o kodzie źródłowym pochodzącym z E<.Bx>, "
"do którego prawa należą do władz Uniwersytetu Kalifornijskiego."

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"Since OpenSSH 8.8, E<.Nm> has use the SFTP protocol for transfers by default."
msgstr ""

#. type: Sh
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
#, no-wrap
msgid "AUTHORS"
msgstr "AUTORZY"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron
msgid ""
"E<.An Timo Rinne Aq Mt tri@iki.fi> E<.An Tatu Ylonen Aq Mt ylo@cs.hut.fi>"
msgstr ""
"E<.An Timo Rinne Aq Mt tri@iki.fi>, E<.An Tatu Ylonen Aq Mt ylo@cs.hut.fi>"

#. type: Sh
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "CAVEATS"
msgstr ""

#. type: Plain text
#: archlinux debian-unstable
msgid ""
"The legacy SCP protocol (selected by the E<.Fl O> flag) requires execution "
"of the remote user's shell to perform E<.Xr glob 3> pattern matching.  This "
"requires careful quoting of any characters that have special meaning to the "
"remote shell, such as quote characters."
msgstr ""

#. type: Dd
#: debian-bullseye
#, no-wrap
msgid "$Mdocdate: August 3 2020 $"
msgstr "$Mdocdate: August 3 2020 $"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "E<.Nm scp> E<.Bk -words> E<.Op Fl 12346BCpqrv> E<.Op Fl c Ar cipher> E<."
#| "Op Fl F Ar ssh_config> E<.Op Fl i Ar identity_file> E<.Op Fl l Ar limit> "
#| "E<.Op Fl o Ar ssh_option> E<.Op Fl P Ar port> E<.Op Fl S Ar program> E<."
#| "Sm off> E<.Oo> E<.Op Ar user No @> E<.Ar host1 No>: E<.Oc Ar file1> E<.Sm "
#| "on> E<.Ar ...> E<.Sm off> E<.Oo> E<.Op Ar user No @> E<.Ar host2 No>: E<."
#| "Oc Ar file2> E<.Sm on> E<.Ek>"
msgid ""
"E<.Nm scp> E<.Op Fl 346ABCpqrTv> E<.Op Fl c Ar cipher> E<.Op Fl F Ar "
"ssh_config> E<.Op Fl i Ar identity_file> E<.Op Fl J Ar destination> E<.Op Fl "
"l Ar limit> E<.Op Fl o Ar ssh_option> E<.Op Fl P Ar port> E<.Op Fl S Ar "
"program> E<.Ar source ... target>"
msgstr ""
"E<.Nm scp> E<.Bk -words> E<.Op Fl 12346BCpqrv> E<.Op Fl c Ar szyfr> E<.Op Fl "
"F Ar plik_konfig_ssh> E<.Op Fl i Ar plik_tożsamości> E<.Op Fl l Ar limit> E<."
"Op Fl o Ar opcja_ssh> E<.Op Fl P Ar port> E<.Op Fl S Ar program> E<.Sm off> "
"E<.Oo> E<.Op Ar użytkownik No @> E<.Ar komp1 No>: E<.Oc Ar plik1> E<.Sm on> "
"E<.Ar ...> E<.Sm off> E<.Oo> E<.Op Ar użytkownik No @> E<.Ar komp2 No>: E<."
"Oc Ar plik2> E<.Sm on> E<.Ek>"

#. type: Plain text
#: debian-bullseye
msgid ""
"E<.Nm> copies files between hosts on a network.  It uses E<.Xr ssh 1> for "
"data transfer, and uses the same authentication and provides the same "
"security as E<.Xr ssh 1>.  E<.Nm> will ask for passwords or passphrases if "
"they are needed for authentication."
msgstr ""
"E<.Nm> kopiuje pliki między komputerami w sieci. Do transferu danych używa "
"E<.Xr ssh 1> i wykorzystuje tę samą autoryzację oraz daje takie samo "
"bezpieczeństwo jak E<.Xr ssh 1>. E<.Nm> pyta w razie potrzeby o hasła "
"uwierzytelniające."

#. type: Plain text
#: debian-bullseye
msgid ""
"When copying between two remote hosts, if the URI format is used, a E<.Ar "
"port> may only be specified on the E<.Ar target> if the E<.Fl 3> option is "
"used."
msgstr ""

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Copies between two remote hosts are transferred through the local host.  "
#| "Without this option the data is copied directly between the two remote "
#| "hosts.  Note that this option disables the progress meter."
msgid ""
"Copies between two remote hosts are transferred through the local host.  "
"Without this option the data is copied directly between the two remote "
"hosts.  Note that this option disables the progress meter and selects batch "
"mode for the second host, since E<.Nm> cannot ask for passwords or "
"passphrases for both hosts."
msgstr ""
"Kopiowanie pomiędzy dwoma zdalnymi komputerami jest wykonywane poprzez "
"komputer lokalny. Bez tej opcji dane są kopiowane bezpośrednio pomiędzy "
"dwoma komputerami. Proszę zauważyć, że ta opcja wyłącza pasek postępu."

#. type: It
#: debian-bullseye
#, no-wrap
msgid "ChallengeResponseAuthentication"
msgstr "ChallengeResponseAuthentication"

#. type: It
#: debian-bullseye
#, no-wrap
msgid "HostbasedKeyTypes"
msgstr "HostbasedKeyTypes"

#. type: It
#: debian-bullseye
#, no-wrap
msgid "PubkeyAcceptedKeyTypes"
msgstr "PubkeyAcceptedKeyTypes"

#. type: Plain text
#: debian-bullseye
msgid ""
"Specifies the port to connect to on the remote host.  Note that this option "
"is written with a capital E<.Sq P>, because E<.Fl p> is already reserved for "
"preserving the times and modes of the file."
msgstr ""
"Podaje port, do którego należy się podłączyć na zdalnym hoście. Proszę "
"zauważyć, że opcja ta jest napisana jako wielkie E<.Sq P>, gdyż E<.Fl p> już "
"jest zarezerwowane dla zachowywania czasów modyfikacji i praw dostępu plików."

#
#. type: Plain text
#: debian-bullseye
msgid ""
"Preserves modification times, access times, and modes from the original file."
msgstr "Zachowuje czasy modyfikacji oraz dostępu i prawa oryginalnego pliku."

#. type: Plain text
#: debian-bullseye
msgid ""
"E<.Xr sftp 1>, E<.Xr ssh 1>, E<.Xr ssh-add 1>, E<.Xr ssh-agent 1>, E<.Xr ssh-"
"keygen 1>, E<.Xr ssh_config 5>, E<.Xr sshd 8>"
msgstr ""
"E<.Xr sftp 1>, E<.Xr ssh 1>, E<.Xr ssh-add 1>, E<.Xr ssh-agent 1>, E<.Xr ssh-"
"keygen 1>, E<.Xr ssh_config 5>, E<.Xr sshd 8>"

#. type: Dd
#: fedora-36 fedora-rawhide mageia-cauldron
#, fuzzy, no-wrap
#| msgid "$Mdocdate: April 30 2020 $"
msgid "$Mdocdate: September 20 2021 $"
msgstr "$Mdocdate: April 30 2020 $"

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "E<.Nm scp> E<.Bk -words> E<.Op Fl 12346BCpqrv> E<.Op Fl c Ar cipher> E<."
#| "Op Fl F Ar ssh_config> E<.Op Fl i Ar identity_file> E<.Op Fl l Ar limit> "
#| "E<.Op Fl o Ar ssh_option> E<.Op Fl P Ar port> E<.Op Fl S Ar program> E<."
#| "Sm off> E<.Oo> E<.Op Ar user No @> E<.Ar host1 No>: E<.Oc Ar file1> E<.Sm "
#| "on> E<.Ar ...> E<.Sm off> E<.Oo> E<.Op Ar user No @> E<.Ar host2 No>: E<."
#| "Oc Ar file2> E<.Sm on> E<.Ek>"
msgid ""
"E<.Nm scp> E<.Op Fl 346ABCOpqRrTv> E<.Op Fl c Ar cipher> E<.Op Fl D Ar "
"sftp_server_path> E<.Op Fl F Ar ssh_config> E<.Op Fl i Ar identity_file> E<."
"Op Fl J Ar destination> E<.Op Fl l Ar limit> E<.Op Fl o Ar ssh_option> E<.Op "
"Fl P Ar port> E<.Op Fl S Ar program> E<.Ar source ... target>"
msgstr ""
"E<.Nm scp> E<.Bk -words> E<.Op Fl 12346BCpqrv> E<.Op Fl c Ar szyfr> E<.Op Fl "
"F Ar plik_konfig_ssh> E<.Op Fl i Ar plik_tożsamości> E<.Op Fl l Ar limit> E<."
"Op Fl o Ar opcja_ssh> E<.Op Fl P Ar port> E<.Op Fl S Ar program> E<.Sm off> "
"E<.Oo> E<.Op Ar użytkownik No @> E<.Ar komp1 No>: E<.Oc Ar plik1> E<.Sm on> "
"E<.Ar ...> E<.Sm off> E<.Oo> E<.Op Ar użytkownik No @> E<.Ar komp2 No>: E<."
"Oc Ar plik2> E<.Sm on> E<.Ek>"

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid ""
#| "Copies between two remote hosts are transferred through the local host.  "
#| "Without this option the data is copied directly between the two remote "
#| "hosts.  Note that this option disables the progress meter."
msgid ""
"Copies between two remote hosts are transferred through the local host.  "
"Without this option the data is copied directly between the two remote "
"hosts.  Note that, when using the original SCP protocol (via the E<.Fl O> "
"flag), this option selects batch mode for the second host as E<.Nm> cannot "
"ask for passwords or passphrases for both hosts.  This mode is the default."
msgstr ""
"Kopiowanie pomiędzy dwoma zdalnymi komputerami jest wykonywane poprzez "
"komputer lokalny. Bez tej opcji dane są kopiowane bezpośrednio pomiędzy "
"dwoma komputerami. Proszę zauważyć, że ta opcja wyłącza pasek postępu."

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"When using the SFTP protocol support via E<.Fl s>, connect directly to a "
"local SFTP server program rather than a remote one via E<.Xr ssh 1>.  This "
"option may be useful in debugging the client and server."
msgstr ""

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"Use the original SCP protocol for file transfers instead of the SFTP "
"protocol.  Forcing the use of the SCP protocol may be necessary for servers "
"that do not implement SFTP, for backwards-compatibility for particular "
"filename wildcard patterns and for expanding paths with a E<.Sq ~> prefix "
"for older SFTP servers."
msgstr ""

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"Usage of SCP protocol can be blocked by creating a world-readable E<.Ar /etc/"
"ssh/disable_scp> file. If this file exists, when SCP protocol is in use "
"(either remotely or via the E<.Fl O> option), the program will exit."
msgstr ""

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"The original scp protocol (selected by the E<.Fl O> flag) requires execution "
"of the remote user's shell to perform E<.Xr glob 3> pattern matching.  This "
"requires careful quoting of any characters that have special meaning to the "
"remote shell, such as quote characters."
msgstr ""

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"Since OpenSSH 8.8 (8.7 in Red Hat/Fedora builds), E<.Nm> has use the SFTP "
"protocol for transfers by default."
msgstr ""

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"The original SCP protocol (used by default) requires execution of the remote "
"user's shell to perform E<.Xr glob 3> pattern matching.  This requires "
"careful quoting of any characters that have special meaning to the remote "
"shell, such as quote characters."
msgstr ""
