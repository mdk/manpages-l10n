# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Kenneth Rohde Christiansen <kenneth@gnu.org>, 2000.
# Peter Makholm <peter@makholm.net>, 2000.
# Byrial Ole Jensen <byrial@vip.cybercity.dk>, 2000.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.2.0\n"
"POT-Creation-Date: 2020-11-24 18:45+01:00\n"
"PO-Revision-Date: 2000-06-29 00:21+0100\n"
"Last-Translator: Byrial Ole Jensen <byrial@vip.cybercity.dk>\n"
"Language-Team: Danish <debian-user-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: original/man1/chgrp.1:2
#, no-wrap
msgid "CHGRP"
msgstr "CHGRP"

#. type: TH
#: original/man1/chgrp.1:2
#, no-wrap
msgid "May 2000"
msgstr "Maj 2000"

#. type: TH
#: original/man1/chgrp.1:2
#, no-wrap
msgid "GNU fileutils 4.0l"
msgstr "GNU fileutils 4.0l"

#. type: TH
#: original/man1/chgrp.1:2
#, no-wrap
msgid "FSF"
msgstr "FSF"

#. type: SH
#: original/man1/chgrp.1:3
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: original/man1/chgrp.1:5
msgid "chgrp - change group ownership"
msgstr "chgrp - ændre gruppeejerskab"

#. type: SH
#: original/man1/chgrp.1:5
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: original/man1/chgrp.1:8
msgid "B<chgrp> [I<OPTION>]... I<GROUP FILE>..."
msgstr "B<chgrp> [I<OPTION>]...I< GRUPPE FIL>..."

#. type: Plain text
#: original/man1/chgrp.1:11
msgid "B<chgrp> [I<OPTION>]... I<--reference=RFILE FILE>..."
msgstr "B<chgrp> [I<OPTION>]...I< --reference=RFIL FIL>..."

#. type: SH
#: original/man1/chgrp.1:11
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: original/man1/chgrp.1:15
msgid "Change the group membership of each FILE to GROUP."
msgstr "Ændre gruppeejerskabet for enhver FIL til GRUPPE"

#. type: TP
#: original/man1/chgrp.1:15
#, no-wrap
msgid "B<-c>, B<--changes>"
msgstr "B<-c>, B<--changes>"

#. type: Plain text
#: original/man1/chgrp.1:18
msgid "like verbose but report only when a change is made"
msgstr "ligesom verbose men rapporterer kun når der bliver lavet ændringer"

#. type: TP
#: original/man1/chgrp.1:18
#, no-wrap
msgid "B<--dereference>"
msgstr "B<--dereference>"

#. type: Plain text
#: original/man1/chgrp.1:22
msgid ""
"affect the referent of each symbolic link, rather than the symbolic link "
"itself"
msgstr ""
"virk på referenten af hver symbolsk lænke (dette er standardopførsel) frem "
"for selve lænken"

#. type: TP
#: original/man1/chgrp.1:22
#, no-wrap
msgid "B<-h>, B<--no-dereference>"
msgstr "B<-h>, B<--no-dereference>"

#. type: Plain text
#: original/man1/chgrp.1:27
msgid ""
"affect symbolic links instead of any referenced file (available only on "
"systems that can change the ownership of a symlink)"
msgstr ""
"påvirker symbolske lænker i stedet for filen der er lænket til (kun "
"tilgængelig på systemer med lchown-systemkaldet)"

#. type: TP
#: original/man1/chgrp.1:27
#, no-wrap
msgid "B<-f>, B<--silent>, B<--quiet>"
msgstr "B<-f>, B<--silent>, B<--quiet>"

#. type: Plain text
#: original/man1/chgrp.1:30
msgid "suppress most error messages"
msgstr "undertrykker de fleste fejlbeskeder"

#. type: TP
#: original/man1/chgrp.1:30
#, no-wrap
msgid "B<--reference>=I<RFILE>"
msgstr "B<--reference>=I<RFIL>"

#. type: Plain text
#: original/man1/chgrp.1:34
msgid "use RFILE's group rather than the specified GROUP value"
msgstr "bruger RFILs gruppe i stedet for GRUPPE"

#. type: TP
#: original/man1/chgrp.1:34
#, no-wrap
msgid "B<-R>, B<--recursive>"
msgstr "B<-R>, B<--recursive>"

#. type: Plain text
#: original/man1/chgrp.1:37
msgid "operate on files and directories recursively"
msgstr "ændre filer og kataloger rekursivt"

#. type: TP
#: original/man1/chgrp.1:37
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: original/man1/chgrp.1:40
msgid "output a diagnostic for every file processed"
msgstr "skriver en besked for alle filer den undersøger"

#. type: TP
#: original/man1/chgrp.1:40
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: original/man1/chgrp.1:43
msgid "display this help and exit"
msgstr "viser hjælpetekst og stopper"

#. type: TP
#: original/man1/chgrp.1:43
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: original/man1/chgrp.1:46
msgid "output version information and exit"
msgstr "viser versionsinformation og stopper"

#. type: SH
#: original/man1/chgrp.1:46
#, no-wrap
msgid "AUTHOR"
msgstr "FORFATTER"

#. type: Plain text
#: original/man1/chgrp.1:48
msgid "Written by David MacKenzie."
msgstr "Skrevet af by David MacKenzie."

#. type: SH
#: original/man1/chgrp.1:48
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLMELDING"

#. type: Plain text
#: original/man1/chgrp.1:50
msgid "Report bugs to E<lt>bug-fileutils@gnu.orgE<gt>."
msgstr "Rapporter fejl til E<lt>bug-fileutils@gnu.orgE<gt>."

#. type: SH
#: original/man1/chgrp.1:50
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: original/man1/chgrp.1:52
msgid "Copyright \\(co 1999 Free Software Foundation, Inc."
msgstr "Copyright \\(co 1999 Free Software Foundation, Inc."

#. type: Plain text
#: original/man1/chgrp.1:55
msgid ""
"This is free software; see the source for copying conditions.  There is NO "
"warranty; not even for MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE."
msgstr ""
"Dette er frit programmel: du kan frit ændre og videredistribuere det. Der "
"gives INGEN GARANTI, i den grad som loven tillader dette."

#. type: SH
#: original/man1/chgrp.1:55
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: original/man1/chgrp.1:63
msgid ""
"The full documentation for B<chgrp> is maintained as a Texinfo manual.  If "
"the B<info> and B<chgrp> programs are properly installed at your site, the "
"command"
msgstr ""
"Hele dokumentationen for B<chgrp> bliver vedligeholdt som Texinfo manual. "
"Hvis B<info> and B<chgrp> programmerne er korrekt installeret på dit system "
"vil komandoen"

#. type: Plain text
#: original/man1/chgrp.1:65
msgid "B<info chgrp>"
msgstr "B<info chgrp>"

#. type: Plain text
#: original/man1/chgrp.1:66
msgid "should give you access to the complete manual."
msgstr "give dig adgang til den fulde manual."
