# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.14.0\n"
"POT-Creation-Date: 2022-02-10 21:29+0100\n"
"PO-Revision-Date: 2022-05-29 21:29+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "UNICODE_STOP"
msgstr "UNICODE_STOP"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "3 Feb 2001"
msgstr "3. feb 2001"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "kbd"
msgstr "kbd"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "unicode_stop - revert keyboard and console from unicode mode"
msgstr "unicode_stop - træk tastatur og konsol tilbage fra unicode-tilstand"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<unicode_stop>"
msgstr "B<unicode_stop>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: IX
#: archlinux fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "unicode_stop command"
msgstr "unicode_stop kommando"

#. type: IX
#: archlinux fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "\\fLunicode_stop\\fR command"
msgstr "\\fLunicode_stop\\fR kommando"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<unicode_stop> command will more-or-less undo the effect of "
"B<unicode_start>.  It puts the keyboard in ASCII (XLATE) mode, and clears "
"the console UTF-8 mode."
msgstr ""
"Kommandoen B<unicode_stop> vil mere eller mindre fortryde effekten af "
"B<unicode_start>. Kommandoen placerer tastaturet i ASCII-tilstand (XLATE), "
"og rydder konsollens UTF-8-tilstand."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<kbd_mode>(1), B<unicode_start>(1), B<utf-8>(7), B<setfont>(8)"
msgstr "B<kbd_mode>(1), B<unicode_start>(1), B<utf-8>(7), B<setfont>(8)"
