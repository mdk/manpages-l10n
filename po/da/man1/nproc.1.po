# Danish translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Joe Hansen <joedalton2@yahoo.dk>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.9.2\n"
"POT-Creation-Date: 2022-05-02 20:02+0200\n"
"PO-Revision-Date: 2021-12-21 17:36+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NPROC"
msgstr "NPROC"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Brugerkommandoer"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAVN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "nproc - print the number of processing units available"
msgstr "nproc - udskriv antallet af tilgængelige processorenheder"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<nproc> [I<\\,OPTION\\/>]..."
msgstr "B<nproc> [I<\\,FLAG\\/>]..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESKRIVELSE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Print the number of processing units available to the current process, which "
"may be less than the number of online processors"
msgstr ""
"Udskriv antallet af processorenheder, der er tilgængelige for den aktuelle "
"proces, hvilket kan være mindre end antallet af aktive processorer"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--all>"
msgstr "B<--all>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "print the number of installed processors"
msgstr "udskriv antallet af installerede processorer"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--ignore>=I<\\,N\\/>"
msgstr "B<--ignore>=I<\\,N\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "if possible, exclude N processing units"
msgstr "hvis muligt, så ekskludér N processorenheder"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "viser hjælpetekst og stopper"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "viser versionsinformation og stopper"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "FORFATTER"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Written by Giuseppe Scrivano."
msgstr "Skrevet af Giuseppe Scrivano."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEJLRAPPORTER"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Hjælp til GNU coreutils på nettet:: E<lt>https://www.gnu.org/software/"
"coreutils/E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Rapportér oversættelsesfejl til E<lt>https://translationproject.org/team/da."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "OPHAVSRET"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2022 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dette er et frit program: du kan frit ændre og videredistribuere det. Der "
"gives INGEN GARANTI, i den grad som loven tillader dette."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SE OGSÅ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/nprocE<gt>"
msgstr ""
"Fuld dokumentation E<lt>https://www.gnu.org/software/coreutils/nprocE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) nproc invocation\\(aq"
msgstr ""
"eller lokalt tilgængelig via: info \\(aq(coreutils) nproc invocation\\(aq"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "september 2020"

#. type: TH
#: debian-bullseye debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU coreutils 8.32"

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2020 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "October 2021"
msgstr "oktober 2021"

#. type: TH
#: fedora-36
#, no-wrap
msgid "March 2022"
msgstr "marts 2022"

#. type: TH
#: fedora-36 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.0"
msgstr "GNU coreutils 9.0"

#. type: Plain text
#: fedora-36 mageia-cauldron
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Ophavsret \\(co 2021 Free Software Foundation, Inc.  Licens GPLv3+: GNU GPL "
"version 3 eller nyere E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2021"
msgstr "september 2021"
