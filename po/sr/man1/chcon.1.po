# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2022-05-02 19:46+0200\n"
"PO-Revision-Date: 2021-09-03 20:06+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CHCON"
msgstr "CHCON"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2022"
msgstr "Априла 2022"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "ГНУ coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid "changing security context of %s\n"
msgid "chcon - change file security context"
msgstr "мењам садржај безбедности за „%s“"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<chcon> [I<\\,OPTION\\/>]... I<\\,CONTEXT FILE\\/>..."
msgstr "B<chcon> [I<\\,ОПЦИЈА\\/>]... I<\\,САДРЖАЈ ДАТОТЕКА\\/>..."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<chcon> [I<\\,OPTION\\/>]... [I<\\,-u USER\\/>] [I<\\,-r ROLE\\/>] [I<\\,-l "
"RANGE\\/>] [I<\\,-t TYPE\\/>] I<\\,FILE\\/>..."
msgstr ""
"B<chcon> [I<\\,ОПЦИЈА\\/>]... [I<\\,-u КОРИСНИК\\/>] [I<\\,-r УЛОГА\\/>] [I<"
"\\,-l ОПСЕГ\\/>] [I<\\,-t ВРСТА\\/>] I<\\,ДАТОТЕКА\\/>..."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<chcon> [I<\\,OPTION\\/>]... I<\\,--reference=RFILE FILE\\/>..."
msgstr ""
"B<chcon> [I<\\,ОПЦИЈА\\/>]... I<\\,--reference=РДАТОТЕКА ДАТОТЕКА\\/>..."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Change the SELinux security context of each FILE to CONTEXT.  With B<--"
"reference>, change the security context of each FILE to that of RFILE."
msgstr ""
"Измените безбедносни контекст СЕЛинукса сваке ДАТОТЕКЕ у САДРЖАЈ. Са B<--"
"reference>, измените безбедносни садржај сваке ДАТОТЕКЕ у онај из РДАТОТЕКЕ."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Mandatory arguments to long options are mandatory for short options too."
msgstr ""
"Обавезни аргументи за дуге опције су обавезни и за кратке опције такође."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--dereference>"
msgstr "B<--dereference>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"affect the referent of each symbolic link (this is the default), rather than "
"the symbolic link itself"
msgstr ""
"утиче на упуту сваке симболичке везе (ово је основно), уместо на саму "
"симболичку везу"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--no-dereference>"
msgstr "B<-h>, B<--no-dereference>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "affect symbolic links instead of any referenced file"
msgstr "утиче на симболичке везе уместо на неку упутну датотеку"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-u>, B<--user>=I<\\,USER\\/>"
msgstr "B<-u>, B<--user>=I<\\,КОРИСНИК\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "set user USER in the target security context"
msgstr "подешава корисника КОРИСНИК у циљном безбедносном садржају"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--role>=I<\\,ROLE\\/>"
msgstr "B<-r>, B<--role>=I<\\,УЛОГА\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "set role ROLE in the target security context"
msgstr "подешава улогу УЛОГА у циљном безбедносном садржају"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--type>=I<\\,TYPE\\/>"
msgstr "B<-t>, B<--type>=I<\\,ВРСТА\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "set type TYPE in the target security context"
msgstr "подешава врсту ВРСТА у циљном безбедносном садржају"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>, B<--range>=I<\\,RANGE\\/>"
msgstr "B<-l>, B<--range>=I<\\,ОПСЕГ\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "set range RANGE in the target security context"
msgstr "подешава опсег ОПСЕГ у циљном безбедносном садржају"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--no-preserve-root>"
msgstr "B<--no-preserve-root>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "do not treat '/' specially (the default)"
msgstr "не сматра „/“ посебним (основно)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--preserve-root>"
msgstr "B<--preserve-root>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "fail to operate recursively on '/'"
msgstr "не успева да ради дубински над „/“"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--reference>=I<\\,RFILE\\/>"
msgstr "B<--reference>=I<\\,РДАТОТЕКА\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "use RFILE's security context rather than specifying a CONTEXT value"
msgstr ""
"користи безбедносни садржај РДАТОТЕКЕ уместо да наводи врдедност САДРЖАЈА"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-R>, B<--recursive>"
msgstr "B<-R>, B<--recursive>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "operate on files and directories recursively"
msgstr "ради дубински над датотекама и директоријумима"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "output a diagnostic for every file processed"
msgstr "исписује дијагнозу за сваку обрађену датотеку"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The following options modify how a hierarchy is traversed when the B<-R> "
"option is also specified.  If more than one is specified, only the final one "
"takes effect."
msgstr ""
"Следеће опције мењају начин преласка хијерархије када је опција B<-R> такође "
"наведена.  Ако је наведена више од једне, само последња има дејства."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-H>"
msgstr "B<-H>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"if a command line argument is a symbolic link to a directory, traverse it"
msgstr ""
"ако је аргумент линије наредби симболичка веза ка директоријуму, прелази је"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-L>"
msgstr "B<-L>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "traverse every symbolic link to a directory encountered"
msgstr "прелази сваку симболичку везу ка директору на коју наиђе"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-P>"
msgstr "B<-P>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "do not traverse any symbolic links (default)"
msgstr "не прелази симболичке везе (основно)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "приказује ову помоћ и излази"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "исписује податке о издању и излази"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АУТОР"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Written by Russell Coker and Jim Meyering."
msgstr "Написали су Расел Кокер и Џим Мејерин."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Помоћ на мрежи за ГНУ coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Грешке у преводу пријавите на E<lt>https://translationproject.org/team/sr."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "АУТОРСКА ПРАВА"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ово је слободан софтвер: слободни сте да га мењате и расподељујете. Не "
"постоји НИКАКВА ГАРАНЦИЈА, у оквирима дозвољеним законом."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/chconE<gt>"
msgstr ""
"Сва документација се налази на E<lt>https://www.gnu.org/software/coreutils/"
"chconE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) chcon invocation\\(aq"
msgstr ""
"или је доступна на рачунару путем наредбе „info \\(aq(coreutils) chcon "
"invocation\\(aq“"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Септембра 2020"

#. type: TH
#: debian-bullseye debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "ГНУ coreutils 8.32"

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "October 2021"
msgstr "Октобра 2021"

#. type: TH
#: fedora-36
#, no-wrap
msgid "March 2022"
msgstr "Марта 2022"

#. type: TH
#: fedora-36 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.0"
msgstr "ГНУ coreutils 9.0"

#. type: Plain text
#: fedora-36 fedora-rawhide
#, fuzzy
#| msgid "changing security context of %s\n"
msgid "chcon - change file SELinux security context"
msgstr "мењам садржај безбедности за „%s“"

#. type: Plain text
#: fedora-36 mageia-cauldron
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2021"
msgstr "Септембра 2021"
