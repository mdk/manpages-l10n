# Serbian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.11.0\n"
"POT-Creation-Date: 2022-05-02 19:49+0200\n"
"PO-Revision-Date: 2021-09-03 20:06+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Serbian <>\n"
"Language: sr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "EXPR"
msgstr "EXPR"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2022"
msgstr "Априла 2022"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "ГНУ coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Корисничке наредбе"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "НАЗИВ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid "error in regular expression search"
msgid "expr - evaluate expressions"
msgstr "грешка у претрази регуларног израза"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "УВОД"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<expr> I<\\,EXPRESSION\\/>"
msgstr "B<expr> I<\\,ИЗРАЗ\\/>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<expr> I<\\,OPTION\\/>"
msgstr "B<expr> I<\\,ОПЦИЈА\\/>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "ОПИС"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "приказује ову помоћ и излази"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "исписује податке о издању и излази"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Print the value of EXPRESSION to standard output.  A blank line below "
"separates increasing precedence groups.  EXPRESSION may be:"
msgstr ""
"Испсиује вредност ИЗРАЗА на стандардни излаз.  Празан ред испод раздваја "
"повећавајуће првенство група.  ИЗРАЗ може бити:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 | ARG2"
msgstr "АРГ1 | АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 if it is neither null nor 0, otherwise ARG2"
msgstr "АРГ1 ако није ни ништаван ни 0, у супротном АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 & ARG2"
msgstr "АРГ1 & АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 if neither argument is null or 0, otherwise 0"
msgstr "АРГ1 ако ниједан аргумент није ништаван или 0, у супротном 0"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 E<lt> ARG2"
msgstr "АРГ1 E<lt> АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is less than ARG2"
msgstr "АРГ1 је мањи од АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 E<lt>= ARG2"
msgstr "АРГ1 E<lt>= АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is less than or equal to ARG2"
msgstr "АРГ1 је мањи или једнак АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 = ARG2"
msgstr "АРГ1 = АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is equal to ARG2"
msgstr "АРГ1 је једнак АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 != ARG2"
msgstr "АРГ1 != АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is unequal to ARG2"
msgstr "АРГ1 није једнак АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 E<gt>= ARG2"
msgstr "АРГ1 E<gt>= АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is greater than or equal to ARG2"
msgstr "АРГ1 је већи или једнак АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 E<gt> ARG2"
msgstr "АРГ1 E<gt> АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ARG1 is greater than ARG2"
msgstr "АРГ1 је већи од АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 + ARG2"
msgstr "АРГ1 + АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic sum of ARG1 and ARG2"
msgstr "аритметички збир АРГ1 и АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 - ARG2"
msgstr "АРГ1 - АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic difference of ARG1 and ARG2"
msgstr "аритметичка разлика АРГ1 и АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 * ARG2"
msgstr "АРГ1 * АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic product of ARG1 and ARG2"
msgstr "аритметички производ АРГ1 и АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 / ARG2"
msgstr "АРГ1 / АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic quotient of ARG1 divided by ARG2"
msgstr "аритметички коефицијенат АРГ1 подељеног са АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ARG1 % ARG2"
msgstr "АРГ1 % АРГ2"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "arithmetic remainder of ARG1 divided by ARG2"
msgstr "аритметички остатак АРГ1 подељеног са АРГ2"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "STRING : REGEXP"
msgstr "НИСКА : РЕГИЗР"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "anchored pattern match of REGEXP in STRING"
msgstr "прикачени шаблон поклапања РЕГИЗР-а у НИСЦИ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "match STRING REGEXP"
msgstr "match НИСКА РЕГИЗР"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "same as STRING : REGEXP"
msgstr "исто као НИСКА : РЕГИЗР"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "substr STRING POS LENGTH"
msgstr "substr НИСКА ПОЛ ДУЖИНА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "substring of STRING, POS counted from 1"
msgstr "подниска НИСКЕ, ПОЛОЖАЈ бројан од 1"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "index STRING CHARS"
msgstr "index НИСКА ЗНАЦИ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "index in STRING where any CHARS is found, or 0"
msgstr "попис у НИСЦИ где је сваки ЗНАК пронађен, или 0"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "length STRING"
msgstr "length НИСКА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "length of STRING"
msgstr "дужина НИСКЕ"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "+ TOKEN"
msgstr "+ ЕЛЕМЕНТ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "interpret TOKEN as a string, even if it is a"
msgstr "тумачи ЕЛЕМЕНТ као ниску, чак и ако је"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "keyword like 'match' or an operator like '/'"
msgstr "кључна реч као „match“ или оператор као „/“"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "( EXPRESSION )"
msgstr "( ИЗРАЗ )"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "value of EXPRESSION"
msgstr "вредност ИЗРАЗА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Beware that many operators need to be escaped or quoted for shells.  "
"Comparisons are arithmetic if both ARGs are numbers, else lexicographical.  "
"Pattern matches return the string matched between \\e( and \\e) or null; if "
"\\e( and \\e) are not used, they return the number of characters matched or "
"0."
msgstr ""
"Опрез јер многи оператори морају имати знак краја или морају бити цитирани "
"за шкољке. Поклапања шаблона дају ниску поклопљену између \\e( и \\e) или "
"ништа; ако се \\e( и \\e) не користе, дају број поклопљених знакова или 0."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Exit status is 0 if EXPRESSION is neither null nor 0, 1 if EXPRESSION is "
"null or 0, 2 if EXPRESSION is syntactically invalid, and 3 if an error "
"occurred."
msgstr ""
"Излазно стање је 0 ако ИЗРАЗ није ни ништа ни 0, 1 ако је ИЗРАЗ ништаванили "
"0, 2 ако је ИЗРАЗ садржајно неисправан, а 3 ако дође до грешке."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "АУТОР"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Written by Mike Parker, James Youngman, and Paul Eggert."
msgstr "Написали су Мајк Паркер, Џејмс Јангмен и Пол Егерт."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "ПРИЈАВЉИВАЊЕ ГРЕШАКА"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Помоћ на мрежи за ГНУ coreutils: E<lt>https://www.gnu.org/software/coreutils/"
"E<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Грешке у преводу пријавите на E<lt>https://translationproject.org/team/sr."
"htmlE<gt>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "АУТОРСКА ПРАВА"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Ово је слободан софтвер: слободни сте да га мењате и расподељујете. Не "
"постоји НИКАКВА ГАРАНЦИЈА, у оквирима дозвољеним законом."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "ВИДИТЕ ТАКОЂЕ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/exprE<gt>"
msgstr ""
"Сва документација се налази на E<lt>https://www.gnu.org/software/coreutils/"
"exprE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) expr invocation\\(aq"
msgstr ""
"или је доступна на рачунару путем наредбе „info \\(aq(coreutils) expr "
"invocation\\(aq“"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "Септембра 2020"

#. type: TH
#: debian-bullseye debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "ГНУ coreutils 8.32"

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "October 2021"
msgstr "Октобра 2021"

#. type: TH
#: fedora-36
#, no-wrap
msgid "March 2022"
msgstr "Марта 2022"

#. type: TH
#: fedora-36 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.0"
msgstr "ГНУ coreutils 9.0"

#. type: Plain text
#: fedora-36 mageia-cauldron
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc. Лиценца ОЈЛв3+: ГНУ ОЈЛ "
"издање 3 или касније E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2021"
msgstr "Септембра 2021"
