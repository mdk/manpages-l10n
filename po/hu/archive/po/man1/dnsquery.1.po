# Hungarian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Kovács Emese <emese@eik.bme.hu>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-07-11 08:57+0200\n"
"PO-Revision-Date: 2001-01-05 12:34+0100\n"
"Last-Translator: Kovács Emese <emese@eik.bme.hu>\n"
"Language-Team: Hungarian <>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: Dd
#: original/man1/dnsquery.1:18
#, no-wrap
msgid "March 10, 1990"
msgstr "1990. március 10."

#. type: Dt
#: original/man1/dnsquery.1:19
#, no-wrap
msgid "DNSQUERY 1"
msgstr "DNSQUERY 1"

#. type: Os
#: original/man1/dnsquery.1:20
#, no-wrap
msgid "BSD"
msgstr "BSD"

#. type: Os
#: original/man1/dnsquery.1:20
#, no-wrap
msgid "4"
msgstr "4"

#. type: Sh
#: original/man1/dnsquery.1:21
#, no-wrap
msgid "NAME"
msgstr "NÉV"

#. type: Plain text
#: original/man1/dnsquery.1:23
msgid "E<.Nm dnsquery >"
msgstr "E<.Nm dnsquery >"

#. type: Nd
#: original/man1/dnsquery.1:23
#, no-wrap
msgid "query domain name servers using resolver"
msgstr "tartomány névkiszolgálókat kérdez le, a resolver (név feloldó) segítségével"

#. type: Sh
#: original/man1/dnsquery.1:24
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÁTTEKINTÉS"

#. type: Plain text
#: original/man1/dnsquery.1:35
msgid ""
"E<.Nm dnsquery> E<.Op Fl n Ar nameserver> E<.Op Fl t Ar type> E<.Op Fl c Ar "
"class> E<.Op Fl r Ar retry> E<.Op Fl p Ar period> E<.Op Fl d> E<.Op Fl s> E<."
"Op Fl v> E<.Ar host>"
msgstr ""
"E<.Nm dnsquery> E<.Op Fl n Ar névkiszolgáló> E<.Op Fl t Ar típus> E<.Op Fl c "
"Ar osztály> E<.Op Fl r Ar újrapróbálás> E<.Op Fl p Ar időtartam> E<.Op Fl d> "
"E<.Op Fl s> E<.Op Fl v> E<.Ar host>"

#. type: Sh
#: original/man1/dnsquery.1:35
#, no-wrap
msgid "DESCRIPTION"
msgstr "LEÍRÁS"

#. type: Plain text
#: original/man1/dnsquery.1:48
msgid ""
"The E<.Ic dnsquery> program is a general interface to nameservers via BIND "
"resolver library calls.  The program supports queries to the nameserver with "
"an opcode of QUERY.  This program is intended to be a replacement or "
"supplement to programs like nstest, nsquery and nslookup.  All arguments "
"except for E<.Ar host> and E<.Ar nameserver> are treated without case-"
"sensitivity."
msgstr ""
"A E<.Ic dnsquery> program egy általános interface a névszerverek felé, a BIND "
"resolver (név feloldó) könyvtára segítségével.  A program támogatja a QUERY "
"használatát a lekerdezéseknél.  A program célja, hogy kiegészítse vagy "
"felváltsa a szokványos programokat, mint az nstest, nsquery vagy nslookup. A "
"program által elfogadott argumentumok, a E<.Ar host > és a E<.Ar "
"névkiszolgáló> kivételével, írhatók kis- és nagybetűvel egyaránt."

#. type: Sh
#: original/man1/dnsquery.1:48
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIÓK"

#. type: It
#: original/man1/dnsquery.1:50
#, no-wrap
msgid "Fl n Ar nameserver"
msgstr "Fl n Ar névkiszolgáló"

#. type: Plain text
#: original/man1/dnsquery.1:57
msgid ""
"The nameserver to be used in the query.  Nameservers can appear as either "
"Internet addresses of the form E<.Ar w.x.y.z > or can appear as domain "
"names.  (Default: as specified in E<.Pa /etc/resolv.conf>.)"
msgstr ""
"A megadott névkiszolgáló használandó a lekérdezéshez. A névkiszolgálót "
"megadhatjuk IP címmel ( E<.Ar w.x.y.z > formátum) vagy tartomány névvel.  "
"(Alapértelmezésben a E<.Pa /etc/resolv.conf>.  alapján )"

#. type: It
#: original/man1/dnsquery.1:57
#, no-wrap
msgid "Fl t Ar type"
msgstr "Fl t Ar típus"

#. type: Plain text
#: original/man1/dnsquery.1:59
msgid "The type of resource record of interest.  Types include:"
msgstr "A minket érdeklő erőforrás bejegyzés típusa. Ez lehet:"

#. type: It
#: original/man1/dnsquery.1:60
#, no-wrap
msgid "Ar A"
msgstr "Ar A"

#. type: Plain text
#: original/man1/dnsquery.1:62
msgid "address"
msgstr "cím"

#. type: It
#: original/man1/dnsquery.1:62
#, no-wrap
msgid "Ar NS"
msgstr "Ar NS"

#. type: Plain text
#: original/man1/dnsquery.1:64
msgid "nameserver"
msgstr "névkiszolgáló"

#. type: It
#: original/man1/dnsquery.1:64
#, no-wrap
msgid "Ar CNAME"
msgstr "Ar CNAME"

#. type: Plain text
#: original/man1/dnsquery.1:66
msgid "canonical name"
msgstr ""

#. type: It
#: original/man1/dnsquery.1:66
#, no-wrap
msgid "Ar \"PTR\t\""
msgstr "Ar \"PTR\t\""

#. type: Plain text
#: original/man1/dnsquery.1:68
msgid "domain name pointer"
msgstr "tartománynév mutató"

#. type: It
#: original/man1/dnsquery.1:68
#, no-wrap
msgid "Ar \"SOA\t\""
msgstr "Ar \"SOA\t\""

#. type: Plain text
#: original/man1/dnsquery.1:70
msgid "start of authority"
msgstr "\"start of authority\" azaz hiteles zóna kezdete"

#. type: It
#: original/man1/dnsquery.1:70
#, no-wrap
msgid "Ar \"WKS\t\""
msgstr "Ar \"WKS\t\""

#. type: Plain text
#: original/man1/dnsquery.1:72
msgid "well-known service"
msgstr "jól ismert szolgáltatás"

#. type: It
#: original/man1/dnsquery.1:72
#, no-wrap
msgid "Ar HINFO"
msgstr "Ar HINFO"

#. type: Plain text
#: original/man1/dnsquery.1:74
msgid "host information"
msgstr "host információ"

#. type: It
#: original/man1/dnsquery.1:74
#, no-wrap
msgid "Ar MINFO"
msgstr "Ar MINFO"

#. type: Plain text
#: original/man1/dnsquery.1:76
msgid "mailbox information"
msgstr "postafiók információ"

#. type: It
#: original/man1/dnsquery.1:76
#, no-wrap
msgid "Ar \"MX\t\""
msgstr "Ar \"MX\t\""

#. type: Plain text
#: original/man1/dnsquery.1:78
msgid "mail exchange"
msgstr ""

#. type: It
#: original/man1/dnsquery.1:78
#, no-wrap
msgid "Ar \"RP\t\""
msgstr "Ar \"RP\t\""

#. type: Plain text
#: original/man1/dnsquery.1:80
msgid "responsible person"
msgstr "felelős személy"

#. type: It
#: original/man1/dnsquery.1:80
#, no-wrap
msgid "Ar \"MG\t\""
msgstr "Ar \"MG\t\""

#. type: Plain text
#: original/man1/dnsquery.1:82
msgid "mail group member"
msgstr "levelezési csoport tag"

#. type: It
#: original/man1/dnsquery.1:82
#, no-wrap
msgid "Ar \"AFSDB\t\""
msgstr "Ar \"AFSDB\t\""

#. type: Plain text
#: original/man1/dnsquery.1:84
msgid "DCE or AFS server"
msgstr "DCE vagy AFS szerver"

#. type: It
#: original/man1/dnsquery.1:84 original/man1/dnsquery.1:100
#, no-wrap
msgid "Ar \"ANY\t\""
msgstr "Ar \"ANY\t\""

#. type: Plain text
#: original/man1/dnsquery.1:86 original/man1/dnsquery.1:102
msgid "wildcard"
msgstr "joker, helyettesítő típus"

#. type: Plain text
#: original/man1/dnsquery.1:90
msgid "Note that any case may be used.  (Default: E<.Ar ANY>.)"
msgstr ""
"Megjegyzés: használhatunk kis- vagy nagybetűket.  (Alapértelmezett típus: E<."
"Ar ANY>.)"

#. type: It
#: original/man1/dnsquery.1:90
#, no-wrap
msgid "Fl c Ar class"
msgstr "Fl c Ar osztály"

#. type: Plain text
#: original/man1/dnsquery.1:93
msgid "The class of resource records of interest.  Classes include:"
msgstr "A minket érdeklő erőforrás bejegyzés osztálya.  Az osztály lehet:"

#. type: It
#: original/man1/dnsquery.1:94
#, no-wrap
msgid "Ar \"IN\t\""
msgstr "Ar \"IN\t\""

#. type: Plain text
#: original/man1/dnsquery.1:96
msgid "Internet"
msgstr "Internet"

#. type: It
#: original/man1/dnsquery.1:96
#, no-wrap
msgid "Ar \"HS\t\""
msgstr "Ar \"HS\t\""

#. type: Plain text
#: original/man1/dnsquery.1:98
msgid "Hesiod"
msgstr "Hesiod"

#. type: It
#: original/man1/dnsquery.1:98
#, no-wrap
msgid "Ar CHAOS"
msgstr "Ar CHAOS"

#. type: Plain text
#: original/man1/dnsquery.1:100
msgid "Chaos"
msgstr "Chaos"

#. type: Plain text
#: original/man1/dnsquery.1:106
msgid "Note that any case may be used.  (Default: E<.Ar IN>.)"
msgstr ""
"Megjegyzés: használhatunk kis- vagy nagybetűket.  (Alapértelmezett típus: E<."
"Ar IN>.)"

#. type: It
#: original/man1/dnsquery.1:106
#, no-wrap
msgid "Fl r Ar retry"
msgstr "Fl r Ar újraprobálás"

#. type: Plain text
#: original/man1/dnsquery.1:109
msgid ""
"The number of times to retry if the nameserver is not responding.  (Default: "
"4.)"
msgstr ""
"Az próbálkozások száma, ha a névkiszolgáló nem válaszol.  (Alapértelmezésben "
"4.)"

#. type: It
#: original/man1/dnsquery.1:109
#, no-wrap
msgid "Fl p Ar period"
msgstr "Fl p Ar időtartam"

#. type: Plain text
#: original/man1/dnsquery.1:112
msgid "Period to wait before timing out.  (Default: E<.Dv RES_TIMEOUT>.)"
msgstr ""
"Időtartam, amit a program vár, mielőtt időtúllépési hibával térne vissza.  "
"(Alapértelmezésben: E<.Dv RES_TIMEOUT>.)"

#. type: It
#: original/man1/dnsquery.1:112
#, no-wrap
msgid "Fl d"
msgstr "Fl d"

#. type: Plain text
#: original/man1/dnsquery.1:118
msgid ""
"Turn on debugging.  This sets the E<.Dv RES_DEBUG > bit of the resolver's E<."
"Ft options> field.  (Default: no debugging.)"
msgstr ""
"Hibakeresés (debugging) bekapcsolása. Ez beállítja a resolver E<.Ft options> "
"mezejének a E<.Dv RES_DEBUG > bitjeit.  (Alapértelmezés: nincs hibakeresés.)"

#. type: It
#: original/man1/dnsquery.1:118
#, no-wrap
msgid "Fl s"
msgstr "Fl s"

#. type: Plain text
#: original/man1/dnsquery.1:127
msgid ""
"Use a E<.Em stream> rather than a packet.  This uses a TCP stream connection "
"with the nameserver rather than a UDP datagram.  This sets the E<.Dv "
"RES_USEVC > bit of the resolver's E<.Ft options> field.  (Default: UDP "
"datagram.)"
msgstr ""
"E<.Em stream-et > használ csomagok helyett. Ezzel az opcióval elérhetjük, "
"hogy a program TCP stream-et (folyamot) használjon UDP datagrammok helyett. "
"Ez beállítja a resolver E<.Ft options > mezejének a E<.Dv RES_USEVC> bitjét.  "
"(Alapértelmezésben: UDP datagram.)"

#. type: It
#: original/man1/dnsquery.1:127
#, no-wrap
msgid "Fl v"
msgstr "Fl v"

#. type: Plain text
#: original/man1/dnsquery.1:131
msgid "Synonym for the E<.Dq Fl s > flag."
msgstr "A E<.Dq Fl s > opcióval egyenértékű."

#. type: It
#: original/man1/dnsquery.1:131
#, no-wrap
msgid "Ar host"
msgstr "Ar host"

#. type: Plain text
#: original/man1/dnsquery.1:133
msgid "The name of the host (or domain) of interest."
msgstr "A minket érdeklő host vagy tartomány neve."

#. type: Sh
#: original/man1/dnsquery.1:134
#, no-wrap
msgid "FILES"
msgstr "FÁJLOK"

#. type: It
#: original/man1/dnsquery.1:136
#, no-wrap
msgid "Pa /etc/resolv.conf"
msgstr "Pa /etc/resolv.conf"

#. type: Plain text
#: original/man1/dnsquery.1:138
msgid "to get the default ns and search lists"
msgstr ""
"az alapértelmezett névkiszolgálókhoz és a keresési listához (search domain)"

#. type: It
#: original/man1/dnsquery.1:138
#, no-wrap
msgid "Pa E<lt>arpa/nameser.hE<gt> \"\t\""
msgstr "Pa E<lt>arpa/nameser.hE<gt> \"\t\""

#. type: Plain text
#: original/man1/dnsquery.1:140
msgid "list of usable RR types and classes"
msgstr "használható erőforrás bejegyzés (RR) típusok és osztályok listája"

#. type: It
#: original/man1/dnsquery.1:140
#, no-wrap
msgid "Pa \"E<lt>resolv.hE<gt>\t\t\""
msgstr "Pa \"E<lt>resolv.hE<gt>\t\t\""

#. type: Plain text
#: original/man1/dnsquery.1:142
msgid "list of resolver flags"
msgstr "resolver flag-ek listája"

#. type: Sh
#: original/man1/dnsquery.1:143
#, no-wrap
msgid "DIAGNOSTICS"
msgstr "DIAGNOSZTIKA"

#. type: Plain text
#: original/man1/dnsquery.1:148
msgid ""
"If the resolver fails to answer the query and debugging has not been turned "
"on, E<.Ic dnsquery> will simply print a message like:"
msgstr ""
"Ha a resolver nem tudja megválaszolni a kérést és a hibakeresés nincs "
"bekapcsolva, a E<.Ic dnsquery> valami hasonlót fog kiírni:"

#. type: Dl
#: original/man1/dnsquery.1:148
#, no-wrap
msgid "Query failed (rc = 1) : Unknown host"
msgstr "Query failed (rc = 1) : Unknown host"

#. type: Plain text
#: original/man1/dnsquery.1:152
msgid "The value of the return code is supplied by E<.Ft h_errno>."
msgstr "A visszatérési értéket a E<.Ft h_errno > adja."

#. type: Sh
#: original/man1/dnsquery.1:152
#, no-wrap
msgid "SEE ALSO"
msgstr "LÁSD MÉG"

#. type: Plain text
#: original/man1/dnsquery.1:158
msgid ""
"E<.Xr nslookup 8>, E<.Xr nstest 1>, E<.Xr nsquery 1>, E<.Xr named 8>, E<.Xr "
"resolver 5>."
msgstr ""
"E<.Xr nslookup 8>, E<.Xr nstest 1>, E<.Xr nsquery 1>, E<.Xr named 8>, E<.Xr "
"resolver 5>."

#. type: Sh
#: original/man1/dnsquery.1:158
#, no-wrap
msgid "AUTHOR"
msgstr "SZERZŐ"

#. type: Plain text
#: original/man1/dnsquery.1:160
msgid "Bryan Beecher"
msgstr "Bryan Beecher"

#. type: Sh
#: original/man1/dnsquery.1:160
#, no-wrap
msgid "BUGS"
msgstr "HIBÁK"

#. type: Plain text
#: original/man1/dnsquery.1:168
msgid ""
"Queries of a class other than E<.Ar IN > can have interesting results since "
"ordinarily a nameserver only has a list of root nameservers for class E<.Ar "
"IN > resource records."
msgstr ""
"Az E<.Ar IN > osztálytól eltérő osztályokra való kérdezés érdekes eredménnyel "
"szolgálhat, mert a névkiszolgálók általában csak az E<.Ar IN> osztályba "
"tartozó erőforrás bejegyzéseket tároló gyökér (root) névkiszolgálókról tudnak."

#. type: Plain text
#: original/man1/dnsquery.1:178
msgid ""
"E<.Ic Dnsquery > uses a call to E<.Fn inet_addr > to determine if the "
"argument for the E<.Dq Fl n> option is a valid Internet address.  "
"Unfortunately, E<.Fn inet_addr > seems to cause a segmentation fault with "
"some (bad)  IP addresses (e.g., 1.2.3.4.5)."
msgstr ""
"A E<.Ic dnsquery> az E<.Fn inet_addr> függvényt hívja meg, hogy megállapítsa, "
"hogy a E<.Dq Fl n> opciónál megadott IP cím értelmezhető. Sajnos úgy tűnik az "
"E<.Fn inet_addr > segmentation fault-ot okoz egyes (rossz) IP címekkel (pl. "
"1.2.3.4.5)."
