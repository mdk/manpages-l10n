# Indonesian translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Teguh Adhi A. <adicom@thepentagon.com>, 1999.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-04-23 17:17+0200\n"
"PO-Revision-Date: 1999-06-20 10:49+0200\n"
"Last-Translator: Teguh Adhi A. <adicom@thepentagon.com>\n"
"Language-Team: Indonesian <>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "FDFORMAT"
msgstr "FDFORMAT"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "June 2020"
msgstr "Juni 2020"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr "util-linux"

#. type: TH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "System Administration"
msgstr ""

#. type: SH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NAMA"

#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "fdformat - low-level format a floppy disk"
msgstr "fdformat - memformat sebuah floppy disk secara low-level"

#. type: SH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "RINGKASAN"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "B<fdformat> B<[ -n ]> device"
msgid "B<fdformat> [options]I< device>"
msgstr "B<fdformat> B<[ -n ]> device"

#. type: SH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESKRIPSI"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<fdformat> does a low-level format on a floppy disk.  I<device> is usually "
"one of the following (for floppy devices the major = 2, and the minor is "
"shown for informational purposes only):"
msgstr ""
"B<fdformat> dapat digunakan untuk memformat floopy disk secara low-level.  "
"I<device> biasanya salah satu dari berikut ini (untuk floppy device, major = "
"2, dan minornya ditunjukkan hanya untuk keterangan) :"

#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"/dev/fd0d360  (minor = 4)\n"
"/dev/fd0h1200 (minor = 8)\n"
"/dev/fd0D360  (minor = 12)\n"
"/dev/fd0H360  (minor = 12)\n"
"/dev/fd0D720  (minor = 16)\n"
"/dev/fd0H720  (minor = 16)\n"
"/dev/fd0h360  (minor = 20)\n"
"/dev/fd0h720  (minor = 24)\n"
"/dev/fd0H1440 (minor = 28)\n"
msgstr ""
"/dev/fd0d360  (minor = 4)\n"
"/dev/fd0h1200 (minor = 8)\n"
"/dev/fd0D360  (minor = 12)\n"
"/dev/fd0H360  (minor = 12)\n"
"/dev/fd0D720  (minor = 16)\n"
"/dev/fd0H720  (minor = 16)\n"
"/dev/fd0h360  (minor = 20)\n"
"/dev/fd0h720  (minor = 24)\n"
"/dev/fd0H1440 (minor = 28)\n"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"/dev/fd1d360  (minor = 5)\n"
"/dev/fd1h1200 (minor = 9)\n"
"/dev/fd1D360  (minor = 13)\n"
"/dev/fd1H360  (minor = 13)\n"
"/dev/fd1D720  (minor = 17)\n"
"/dev/fd1H720  (minor = 17)\n"
"/dev/fd1h360  (minor = 21)\n"
"/dev/fd1h720  (minor = 25)\n"
"/dev/fd1H1440 (minor = 29)\n"
msgstr ""
"/dev/fd1d360  (minor = 5)\n"
"/dev/fd1h1200 (minor = 9)\n"
"/dev/fd1D360  (minor = 13)\n"
"/dev/fd1H360  (minor = 13)\n"
"/dev/fd1D720  (minor = 17)\n"
"/dev/fd1H720  (minor = 17)\n"
"/dev/fd1h360  (minor = 21)\n"
"/dev/fd1h720  (minor = 25)\n"
"/dev/fd1H1440 (minor = 29)\n"

#. type: Plain text
#: debian-bullseye
msgid ""
"The generic floppy devices, /dev/fd0 and /dev/fd1, will fail to work with "
"B<fdformat> when a non-standard format is being used, or if the format has "
"not been autodetected earlier.  In this case, use B<setfdprm>(8)  to load "
"the disk parameters."
msgstr ""
"Floppy device biasa, /dev/fd0 dan /dev/fd1, tidak dapat diformat dengan "
"B<fdformat> jika format yang tidak standard sedang digunakan, atau jika "
"format tidak diketahui sebelumnya. Dalam hal ini, gunakanlah B<setfdprm>(8)  "
"untuk mengambil parameter pada disk."

#. type: SH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "PILIHAN"

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-36: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-4: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-f>, B<--from> I<N>"
msgstr "B<-f>, B<--from> I<N>"

#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Start at the track I<N> (default is 0)."
msgstr ""

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-36: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-4: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--to> I<N>"
msgstr "B<-t>, B<--to> I<N>"

#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Stop at the track I<N>."
msgstr ""

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-36: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-4: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-r>, B<--repair> I<N>"
msgstr "B<-r>, B<--repair> I<N>"

#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "Try to repair tracks failed during the verification (max I<N> retries)."
msgstr ""

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-36: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-4: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-n>, B<--no-verify>"
msgstr "B<-n>, B<--no-verify>"

#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "No verify.  This option will disable the verification that is performed "
#| "after the format."
msgid "Skip the verification that is normally performed after the formatting."
msgstr ""
"Tidak melakukan verify.  Option ini akan melewati proses verifikasi yang "
"biasanya dilakukan setelah format selesai."

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-36: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-4: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: debian-bullseye fedora-36 opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid "Display version information."
msgid "Display version information and exit."
msgstr "Tampilkan informasi versi."

#. #-#-#-#-#  debian-bullseye: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: TP
#. #-#-#-#-#  fedora-36: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  fedora-rawhide: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  mageia-cauldron: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-leap-15-4: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#. #-#-#-#-#  opensuse-tumbleweed: fdformat.8.pot (PACKAGE VERSION)  #-#-#-#-#
#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr "B<-h>, B<--help>"

#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid "display this help and exit"
msgid "Display help text and exit."
msgstr "Menampilkan cara penggunaan program."

#. type: SH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "CATATAN"

#. type: Plain text
#: debian-bullseye
msgid ""
"This utility does not handle USB floppy disk drives. Use B<ufiformat>(8)  "
"instead."
msgstr ""

#. type: SH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "AUTHOR"
msgid "AUTHORS"
msgstr "PENGARANG"

#. type: Plain text
#: debian-bullseye
msgid "Werner Almesberger (almesber@nessie.cs.id.ethz.ch)"
msgstr "Werner Almesberger (almesber@nessie.cs.id.ethz.ch)"

#. type: SH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "LIHAT JUGA"

#. type: Plain text
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "B<fd>(4), B<emkfs>(8), B<mkfs>(8), B<setfdprm>(8), B<ufiformat>(8)"
msgstr "B<fd>(4), B<emkfs>(8), B<mkfs>(8), B<setfdprm>(8), B<ufiformat>(8)"

#. type: SH
#: debian-bullseye fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"The fdformat command is part of the util-linux package and is available from "
"https://www.kernel.org/pub/linux/utils/util-linux/."
msgstr ""

#. type: TH
#: fedora-36
#, no-wrap
msgid "2022-01-06"
msgstr "6 Januari 2022"

#. type: TH
#: fedora-36
#, no-wrap
msgid "util-linux 2.38-rc1"
msgstr "util-linux 2.38-rc1"

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid "B<fdformat> B<[ -n ]> device"
msgid "B<fdformat> [options] I<device>"
msgstr "B<fdformat> B<[ -n ]> device"

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<fdformat> does a low level format on a floppy disk.  I<device> is "
#| "usually one of the following (for floppy devices, the major = 2, and the "
#| "minor is shown for informational purposes only):"
msgid ""
"B<fdformat> does a low-level format on a floppy disk. I<device> is usually "
"one of the following (for floppy devices the major = 2, and the minor is "
"shown for informational purposes only):"
msgstr ""
"B<fdformat> dapat digunakan untuk memformat floopy disk secara low-level.  "
"I<device> biasanya salah satu dari berikut ini (untuk floppy device, major = "
"2, dan minornya ditunjukkan hanya untuk keterangan) :"

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"/dev/fd1d360 (minor = 5)\n"
"/dev/fd1h1200 (minor = 9)\n"
"/dev/fd1D360 (minor = 13)\n"
"/dev/fd1H360 (minor = 13)\n"
"/dev/fd1D720 (minor = 17)\n"
"/dev/fd1H720 (minor = 17)\n"
"/dev/fd1h360 (minor = 21)\n"
"/dev/fd1h720 (minor = 25)\n"
"/dev/fd1H1440 (minor = 29)\n"
msgstr ""
"/dev/fd1d360 (minor = 5)\n"
"/dev/fd1h1200 (minor = 9)\n"
"/dev/fd1D360 (minor = 13)\n"
"/dev/fd1H360 (minor = 13)\n"
"/dev/fd1D720 (minor = 17)\n"
"/dev/fd1H720 (minor = 17)\n"
"/dev/fd1h360 (minor = 21)\n"
"/dev/fd1h720 (minor = 25)\n"
"/dev/fd1H1440 (minor = 29)\n"

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"The generic floppy devices, I</dev/fd0> and I</dev/fd1>, will fail to work "
"with B<fdformat> when a non-standard format is being used, or if the format "
"has not been autodetected earlier. In this case, use B<setfdprm>(8) to load "
"the disk parameters."
msgstr ""
"Floppy device biasa, I</dev/fd0> dan I</dev/fd1>, tidak dapat diformat "
"dengan B<fdformat> jika format yang tidak standard sedang digunakan, atau "
"jika format tidak diketahui sebelumnya. Dalam hal ini, gunakanlah "
"B<setfdprm>(8)  untuk mengambil parameter pada disk."

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"This utility does not handle USB floppy disk drives. Use B<ufiformat>(8) "
"instead."
msgstr ""

#. type: SH
#: fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "MELAPORKAN BUG"

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid "For bug reports, use the issue tracker at"
msgstr ""

#. type: Plain text
#: fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed
msgid ""
"The B<fdformat> command is part of the util-linux package which can be "
"downloaded from"
msgstr ""

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "2022-02-17"
msgstr "17 Februari 2022"

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "util-linux 2.38"
msgstr "util-linux 2.38"

#. type: Plain text
#: fedora-rawhide mageia-cauldron
#, fuzzy
#| msgid "Print a help message and exit."
msgid "Print version and exit."
msgstr "Tampilkan sebuah pesan bantuan dan keluar."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2021-06-02"
msgstr "2 Juni 2021"

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "util-linux 2.37.2"
msgstr "util-linux 2.37.2"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "2022-02-14"
msgstr "14 Februari 2022"

#. type: TH
#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.37.4"
msgstr "util-linux 2.37.4"
