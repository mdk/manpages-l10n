# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Helge Kreutzmann <debian@helgefjell.de>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2022-06-16 17:21+0200\n"
"PO-Revision-Date: 2020-08-11 20:59+0200\n"
"Last-Translator: Helge Kreutzmann <debian@helgefjell.de>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "PSTORE\\&.CONF"
msgstr "PSTORE\\&.CONF"

#. type: TH
#: archlinux debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "systemd 251"
msgstr "systemd 251"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "pstore.conf"
msgstr "pstore.conf"

#.  -----------------------------------------------------------------
#.  * MAIN CONTENT STARTS HERE *
#.  -----------------------------------------------------------------
#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "pstore.conf, pstore.conf.d - PStore configuration file"
msgstr "pstore.conf, pstore.conf.d - PStore-Konfigurationsdatei"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "/etc/systemd/pstore\\&.conf /etc/systemd/pstore\\&.conf\\&.d/*"
msgstr "/etc/systemd/pstore\\&.conf /etc/systemd/pstore\\&.conf\\&.d/*"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"This file configures the behavior of B<systemd-pstore>(8), a tool for "
"archiving the contents of the persistent storage filesystem, "
"\\m[blue]B<pstore>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&."
msgstr ""
"Diese Datei konfiguriert das Verhalten von B<systemd-pstore>(8), einem "
"Werkzeug zur Archivierung der Inhalte des dauerhaften Speicherdateisystems "
"\\m[blue]B<pstore>\\m[]\\&\\s-2\\u[1]\\d\\s+2\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "CONFIGURATION DIRECTORIES AND PRECEDENCE"
msgstr "KONFIGURATIONSVERZEICHNISSE UND RANGFOLGE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The default configuration is set during compilation, so configuration is "
"only needed when it is necessary to deviate from those defaults\\&. "
"Initially, the main configuration file in /etc/systemd/ contains commented "
"out entries showing the defaults as a guide to the administrator\\&. Local "
"overrides can be created by editing this file or by creating drop-ins, as "
"described below\\&. Using drop-ins for local configuration is recommended "
"over modifications to the main configuration file\\&."
msgstr ""
"Die Standardkonfiguration wird während der Kompilierung gesetzt\\&. Daher "
"wird eine Konfiguration nur benötigt, wenn von diesen Vorgaben abgewichen "
"werden muss\\&. Anfänglich enthält die Hauptkonfigurationsdatei in /etc/"
"systemd/ die Vorgaben als auskommentierten Hinweis für den Administrator\\&. "
"Lokal können diese Einstellungen außer Kraft gesetzt werden, indem diese "
"Datei bearbeitet wird oder durch die Erstellung von Ergänzungen, wie "
"nachfolgend beschrieben\\&. Es wird empfohlen, Ergänzungen für lokale "
"Konfiguration zu verwenden, statt die Hauptkonfigurationsdatei zu "
"verändern\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In addition to the \"main\" configuration file, drop-in configuration "
"snippets are read from /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/, and /etc/systemd/*\\&.conf\\&.d/\\&. Those drop-ins "
"have higher precedence and override the main configuration file\\&. Files in "
"the *\\&.conf\\&.d/ configuration subdirectories are sorted by their "
"filename in lexicographic order, regardless of in which of the "
"subdirectories they reside\\&. When multiple files specify the same option, "
"for options which accept just a single value, the entry in the file sorted "
"last takes precedence, and for options which accept a list of values, "
"entries are collected as they occur in the sorted files\\&."
msgstr ""
"Zusätzlich zu der »Haupt«-Konfigurationsdatei, werden Ergänzungs-"
"Konfigurationsschnipsel aus /usr/lib/systemd/*\\&.conf\\&.d/, /usr/local/lib/"
"systemd/*\\&.conf\\&.d/ und /etc/systemd/*\\&.conf\\&.d/ gelesen\\&. Diese "
"Ergänzungen haben Vorrang vor der Hauptkonfigurationsdatei und setzen diese "
"außer Kraft\\&. Dateien in den Konfigurationsunterverzeichnissen *\\&."
"conf\\&.d/ werden in lexikographischer Reihenfolge nach ihrem Dateinamen "
"sortiert, unabhängig davon, in welchem Unterverzeichnis sie sich "
"befinden\\&. Bei Optionen, die nur einen einzelnen Wert akzeptieren, hat der "
"Eintrag in der Datei, die als letztes in der Sortierung folgt, Vorrang, "
"falls mehrere Dateien die gleiche Option angeben\\&. Bei Optionen, die eine "
"Liste von Werten akzeptieren, werden Einträge gesammelt, wie sie in den "
"sortierten Dateien auftauchen\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"When packages need to customize the configuration, they can install drop-ins "
"under /usr/\\&. Files in /etc/ are reserved for the local administrator, who "
"may use this logic to override the configuration files installed by vendor "
"packages\\&. Drop-ins have to be used to override package drop-ins, since "
"the main configuration file has lower precedence\\&. It is recommended to "
"prefix all filenames in those subdirectories with a two-digit number and a "
"dash, to simplify the ordering of the files\\&."
msgstr ""
"Wenn Pakete die Konfiguration anpassen müssen, können sie Ergänzungen unter /"
"usr/ installieren\\&. Dateien in /etc/ sind für den lokalen Administrator "
"reserviert, der diese Logik verwenden kann, um die durch die "
"Lieferantenpakete bereitgestellten Konfigurationsdateien außer Kraft zu "
"setzen\\&. Um Ergänzungen der Pakete außer Kraft zu setzen, müssen "
"Ergänzungen verwandt werden, da die Hauptkonfigurationsdatei die niedrigste "
"Priorität hat\\&. Es wird empfohlen, allen Dateinamen in diesen "
"Unterverzeichnissen eine zweistellige Zahl und einen Bindestrich "
"voranzustellen, um die Sortierung der Dateien zu vereinfachen\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"To disable a configuration file supplied by the vendor, the recommended way "
"is to place a symlink to /dev/null in the configuration directory in /etc/, "
"with the same filename as the vendor configuration file\\&."
msgstr ""
"Um eine vom Lieferanten bereitgestellte Konfigurationsdatei zu deaktivieren, "
"wird empfohlen, einen Symlink nach /dev/null in dem "
"Konfigurationsverzeichnis in /etc/ mit dem gleichen Dateinamen wie die "
"Konfigurationsdatei des Lieferanten abzulegen\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "All options are configured in the [PStore] section:"
msgstr "Alle Optionen werden im Abschnitt »[PStore]« konfiguriert:"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<Storage=>"
msgstr "I<Storage=>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Controls where to archive (i\\&.e\\&. copy) files from the pstore "
"filesystem\\&. One of \"none\", \"external\", and \"journal\"\\&. When "
"\"none\", the tool exits without processing files in the pstore "
"filesystem\\&. When \"external\" (the default), files are archived into /var/"
"lib/systemd/pstore/, and logged into the journal\\&. When \"journal\", "
"pstore file contents are logged only in the journal\\&."
msgstr ""
"Steuert, wohin Dateien aus dem Pstore-Dateisystem archiviert (d\\&.h\\&. "
"kopiert) werden\\&. Entweder »none«, »external« oder »journal«\\&. Falls "
"»none«, beendet sich das Werkzeug ohne Dateien im Pstore-Dateisystem zu "
"bearbeiten\\&. Falls »external« (die Vorgabe), werden Dateien nach /var/lib/"
"systemd/pstore/ archiviert und im Journal protokolliert\\&. Falls »journal«, "
"werden die Inhalte der Pstore-Dateien in das Journal protokolliert\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "I<Unlink=>"
msgstr "I<Unlink=>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"Controls whether or not files are removed from pstore after processing\\&. "
"Takes a boolean value\\&. When true, a pstore file is removed from the "
"pstore once it has been archived (either to disk or into the journal)\\&. "
"When false, processing of pstore files occurs normally, but the files remain "
"in the pstore\\&. The default is true in order to maintain the pstore in a "
"nearly empty state, so that the pstore has storage available for the next "
"kernel error event\\&."
msgstr ""
"Steuert, ob Dateien nach der Verarbeitung aus dem Pstore entfernt werden\\&. "
"Akzeptiert einen logischen Wert\\&. Falls wahr, wird eine Pstore-Datei aus "
"dem Pstore entfernt, sobald sie archiviert wurde (entweder auf Platte oder "
"in das Journal)\\&. Falls falsch, erfolgt die Verarbeitung von Pstore-"
"Dateien normal, aber die Dateien verbleiben in dem Pstore\\&. Die Vorgabe "
"ist wahr, um den Pstore in einem fast leeren Zustand zu halten, so dass der "
"Pstore über Speicherplatz für das nächste Kernelfehlerereignis verfügt\\&."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"The defaults for all values are listed as comments in the template /etc/"
"systemd/pstore\\&.conf file that is installed by default\\&."
msgstr ""
"Die Vorgaben für alle Werte sind als Kommentare in der standardmäßig "
"installierten Vorlagendatei /etc/systemd/pstore\\&.conf aufgeführt\\&."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "B<systemd-journald.service>(8)"
msgstr "B<systemd-journald.service>(8)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: IP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid " 1."
msgstr " 1."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "pstore"
msgstr "pstore"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid "\\%https://www.kernel.org/doc/Documentation/ABI/testing/pstore"
msgstr "\\%https://www.kernel.org/doc/Documentation/ABI/testing/pstore"

#. type: TH
#: debian-bullseye fedora-36 opensuse-tumbleweed
#, no-wrap
msgid "systemd 250"
msgstr "systemd 250"
