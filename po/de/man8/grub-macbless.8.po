# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2020, 2021.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n 4.0.0\n"
"POT-Creation-Date: 2022-06-16 17:09+0200\n"
"PO-Revision-Date: 2021-03-07 12:57+0100\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 20.12.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "GRUB-MACBLESS"
msgstr "GRUB-MACBLESS"

#. type: TH
#: archlinux debian-unstable
#, no-wrap
msgid "June 2022"
msgstr "Juni 2022"

#. type: TH
#: archlinux
#, no-wrap
msgid "GRUB 2:2.06.r261.g2f4430cc0-1"
msgstr "GRUB 2:2.06.r261.g2f4430cc0-1"

#. type: TH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "System Administration Utilities"
msgstr "Systemverwaltungswerkzeuge"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "grub-macbless - bless a mac file/directory"
msgstr ""
"grub-macbless - eine Mac-Datei oder -Verzeichnis als bootfähig markieren"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-macbless> [I<\\,OPTION\\/>...] I<\\,--ppc PATH|--x86 FILE\\/>"
msgstr "B<grub-macbless> [I<\\,OPTION\\/> …] I<\\,--ppc PFAD|--x86 DATEI\\/>"

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Mac-style bless on HFS or HFS+"
msgstr "Bootfähig markieren im Mac-Stil auf HFS oder HFS+."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-p>, B<--ppc>"
msgstr "B<-p>, B<--ppc>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "bless for ppc-based macs"
msgstr "nimmt die Bootfähig-Markierung für PPC-basierte Macs vor."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print verbose messages."
msgstr "gibt ausführliche Meldungen aus."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-x>, B<--x86>"
msgstr "B<-x>, B<--x86>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "bless for x86-based macs"
msgstr "nimmt die Bootfähig-Markierung für x86-basierte Macs vor."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "-?, B<--help>"
msgstr "B<-?>, B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give this help list"
msgstr "gibt eine kurze Hilfe aus."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<--usage>"
msgstr "B<--usage>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "give a short usage message"
msgstr "gibt eine kurze Meldung zur Verwendung aus."

#. type: TP
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "print program version"
msgstr "gibt die Programmversion aus."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "Report bugs to E<lt>bug-grub@gnu.orgE<gt>."
msgstr "Melden Sie Fehler (auf Englisch) an E<lt>bug-grub@gnu.orgE<gt>."

#. type: SH
#: archlinux debian-bullseye debian-unstable
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<grub-install>(1)"
msgstr "B<grub-install>(1)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid ""
"The full documentation for B<grub-macbless> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-macbless> programs are properly installed "
"at your site, the command"
msgstr ""
"Die vollständige Dokumentation für B<grub-macbless> wird als ein Texinfo-"
"Handbuch gepflegt. Wenn die Programme B<info>(1) und B<grub-macbless> auf "
"Ihrem Rechner ordnungsgemäß installiert sind, können Sie mit dem Befehl"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "B<info grub-macbless>"
msgstr "B<info grub-macbless>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable
msgid "should give you access to the complete manual."
msgstr "auf das vollständige Handbuch zugreifen."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "July 2021"
msgstr "Juli 2021"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "GRUB 2.04-20"
msgstr "GRUB 2.04-20"

#. type: TH
#: debian-unstable
#, no-wrap
msgid "GRUB 2.06-3"
msgstr "GRUB 2.06-3"
