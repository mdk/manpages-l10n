# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Karl Eichwalder <ke@suse.de>
# Lutz Behnke <lutz.behnke@gmx.de>
# Michael Piefel <piefel@debian.org>
# Michael Schmidt <michael@guug.de>
# Dr. Tobias Quathamer <toddy@debian.org>, 2010, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014, 2016, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2022-05-02 20:11+0200\n"
"PO-Revision-Date: 2022-04-23 21:24+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 21.12.3\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SHA1SUM"
msgstr "SHA1SUM"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "April 2022"
msgstr "April 2022"

#. type: TH
#: archlinux fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr "GNU coreutils 9.1"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "User Commands"
msgstr "Dienstprogramme für Benutzer"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "sha1sum - compute and check SHA1 message digest"
msgstr "sha1sum - SHA1-Prüfsummen berechnen und überprüfen"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<sha1sum> [I<\\,OPTION\\/>]... [I<\\,FILE\\/>]..."
msgstr "B<sha1sum> [I<\\,OPTION\\/>]… [I<\\,DATEI\\/>]…"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Print or check SHA1 (160-bit) checksums."
msgstr "überprüft MD5-Prüfsummen (160-bit) oder gibt diese aus."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "With no FILE, or when FILE is -, read standard input."
msgstr ""
"liest ohne I<DATEI> oder wenn I<DATEI> B<-> ist, aus der Standardeingabe."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-b>, B<--binary>"
msgstr "B<-b>, B<--binary>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "read in binary mode"
msgstr "liest im Binärmodus."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-c>, B<--check>"
msgstr "B<-c>, B<--check>"

#. type: Plain text
#: archlinux fedora-36 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid "read checksums from the FILEs and check them"
msgstr "liest Prüfsummen aus der DATEI und überprüft sie."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--tag>"
msgstr "B<--tag>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "create a BSD-style checksum"
msgstr "erstellt eine Prüfsumme im BSD-Stil."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-t>, B<--text>"
msgstr "B<-t>, B<--text>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "read in text mode (default)"
msgstr "liest im Textmodus (Voreinstellung)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-z>, B<--zero>"
msgstr "B<-z>, B<--zero>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"end each output line with NUL, not newline, and disable file name escaping"
msgstr ""
"schließt jede Ausgabezeile mit einem Nullbyte statt des Zeilenumbruchs ab "
"und deaktiviert Dateinamenmaskierung."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "The following five options are useful only when verifying checksums:"
msgstr "Die folgenden fünf Optionen sind nur beim Überprüfen sinnvoll:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--ignore-missing>"
msgstr "B<--ignore-missing>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "don't fail or report status for missing files"
msgstr "meldet keinen Fehlschlag oder Status für fehlende Dateien."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--quiet>"
msgstr "B<--quiet>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "don't print OK for each successfully verified file"
msgstr "gibt nicht für jede erfolgreich geprüfte Datei OK aus."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--status>"
msgstr "B<--status>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "don't output anything, status code shows success"
msgstr "gibt nichts aus, der Statuscode zeigt Erfolg an."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--strict>"
msgstr "B<--strict>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "exit non-zero for improperly formatted checksum lines"
msgstr ""
"bricht bei ungeeignet formatierten Prüfsummenzeilen mit von 0 verschiedenem "
"Fehlercode ab."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-w>, B<--warn>"
msgstr "B<-w>, B<--warn>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "warn about improperly formatted checksum lines"
msgstr "warnt bei ungeeignet formatierten Prüfsummenzeilen."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr "B<--help>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "display this help and exit"
msgstr "zeigt Hilfeinformationen an und beendet das Programm."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--version>"
msgstr "B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "output version information and exit"
msgstr "gibt Versionsinformationen aus und beendet das Programm."

#. type: Plain text
#: archlinux fedora-36 fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"The sums are computed as described in FIPS-180-1.  When checking, the input "
"should be a former output of this program.  The default mode is to print a "
"line with: checksum, a space, a character indicating input mode ('*' for "
"binary, ' ' for text or where binary is insignificant), and name for each "
"FILE."
msgstr ""
"Die Summen werden wie in FIPS-180-1 beschrieben berechnet. Beim Überprüfen "
"sollte die Eingabe eine frühere Ausgabe dieses Programms sein. Die normale "
"Arbeitsweise ist es, eine Zeile mit Prüfsumme, einem Zeichen, das den "
"Eingabemodus anzeigt (»*« für binär, Leerzeichen für Text oder wenn binär "
"nicht von Bedeutung ist), und dem Namen jeder DATEI auszugeben."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Note: There is no difference between binary mode and text mode on GNU "
"systems."
msgstr ""
"Hinweis: Auf GNU-Systemen gibt es keinen Unterschied zwischen Binärmodus und "
"Textmodus."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Do not use the SHA-1 algorithm for security related purposes.  Instead, use "
"an SHA-2 algorithm, implemented in the programs B<sha224sum>(1), "
"B<sha256sum>(1), B<sha384sum>(1), B<sha512sum>(1), or the BLAKE2 algorithm, "
"implemented in B<b2sum>(1)"
msgstr ""
"Der SHA1-Algorithmus sollte für sicherheitsrelevante Zwecke nicht mehr "
"verwandt werden. Stattdessen verwenden Sie einen SHA-2-Algorithmus, der in "
"den Programmen B<sha224sum>(1), B<sha256sum>(1), B<sha384sum>(1) und "
"B<sha512sum>(1) implementiert ist oder den in B<b2sum>(1) implementierten "
"BLAKE2-Algorithmus."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Written by Ulrich Drepper, Scott Miller, and David Madore."
msgstr "Geschrieben von Ulrich Drepper, Scott Miller und David Madore."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""
"Onlinehilfe für GNU coreutils: E<.UR https://www.gnu.org/software/coreutils/"
">E<.UE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""
"Melden Sie Fehler in der Übersetzung an E<.UR https://translationproject.org/"
"team/de.html> das deutschsprachige Team beim GNU Translation ProjectE<.UE .>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr "COPYRIGHT"

#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2022 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""
"Dies ist freie Software: Sie können sie verändern und weitergeben. Es gibt "
"KEINE GARANTIE, soweit gesetzlich zulässig."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

# WONTFIX missing markup // I'm not sure about marking up references like this.
#. type: Plain text
#: archlinux fedora-rawhide opensuse-tumbleweed
msgid "B<cksum>(1)"
msgstr "B<cksum>(1)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Full documentation E<lt>https://www.gnu.org/software/coreutils/sha1sumE<gt>"
msgstr ""
"Vollständige Dokumentation unter: E<lt>https://www.gnu.org/software/"
"coreutils/sha1sumE<gt>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "or available locally via: info \\(aq(coreutils) sha1sum invocation\\(aq"
msgstr ""
"oder lokal verfügbar mit: info \\(aq(coreutils) sha1sum invocation\\(aq"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr "September 2020"

#. type: TH
#: debian-bullseye debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr "GNU Coreutils 8.32"

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid "read SHA1 sums from the FILEs and check them"
msgstr "SHA1‐Prüfsummen aus DATEIen lesen und überprüfen"

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"The sums are computed as described in FIPS-180-1.  When checking, the input "
"should be a former output of this program.  The default mode is to print a "
"line with checksum, a space, a character indicating input mode ('*' for "
"binary, \\&' ' for text or where binary is insignificant), and name for each "
"FILE."
msgstr ""
"Die Summen werden wie in FIPS-180-1 beschrieben berechnet. Beim Überprüfen "
"sollte die Eingabe eine frühere Ausgabe dieses Programms sein. Die normale "
"Arbeitsweise ist es, eine Zeile mit Prüfsumme, einem Zeichen, das den "
"Eingabetyp anzeigt (»*« für binär, Leerzeichen für Text oder wenn binär "
"nicht von Bedeutung ist), und dem Namen jeder DATEI auszugeben."

#. type: Plain text
#: debian-bullseye debian-unstable fedora-36 mageia-cauldron opensuse-leap-15-4
msgid ""
"Do not use the SHA-1 algorithm for security related purposes.  Instead, use "
"an SHA-2 algorithm, implemented in the programs sha224sum(1), sha256sum(1), "
"sha384sum(1), sha512sum(1), or the BLAKE2 algorithm, implemented in b2sum(1)"
msgstr ""
"Der SHA1-Algorithmus sollte für sicherheitsrelevante Zwecke nicht mehr "
"verwandt werden. Stattdessen verwenden Sie einen SHA-2-Algorithmus, der in "
"den Programmen B<sha224sum>(1), B<sha256sum>(1), B<sha384sum>(1) und "
"B<sha512sum>(1) implementiert ist oder den in B<b2sum>(1) implementierten "
"BLAKE2-Algorithmus."

#. type: Plain text
#: debian-bullseye debian-unstable opensuse-leap-15-4
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2020 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

#. type: TH
#: debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "October 2021"
msgstr "Oktober 2021"

#. type: TH
#: fedora-36
#, no-wrap
msgid "March 2022"
msgstr "März 2022"

#. type: TH
#: fedora-36 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.0"
msgstr "GNU coreutils 9.0"

#. type: Plain text
#: fedora-36 mageia-cauldron
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""
"Copyright \\(co 2021 Free Software Foundation, Inc. Lizenz GPLv3+: E<.UR "
"https://gnu.org/licenses/gpl.html> GNU GPL Version 3 E<.UE> oder neuer."

# WONTFIX missing markup // I'm not sure about marking up references like this.
#. type: Plain text
#: fedora-36 mageia-cauldron
msgid "cksum(1)"
msgstr "B<cksum>(1)"

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "September 2021"
msgstr "September 2021"
