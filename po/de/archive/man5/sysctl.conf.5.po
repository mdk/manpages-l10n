# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Mario Blättermann <mario.blaettermann@gmail.com>, 2019, 2020.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2021-08-27 16:55+0200\n"
"PO-Revision-Date: 2020-05-22 12:13+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Bugs: Report translation errors to the Language-Team address.\n"
"X-Generator: Lokalize 20.04.1\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "SYSCTL.CONF"
msgstr "SYSCTL.CONF"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-04"
msgstr "4. Juni 2020"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "procps-ng"
msgstr "procps-ng"

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "File Formats"
msgstr "Dateiformate"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

#. type: Plain text
#: debian-bullseye
msgid "sysctl.conf - sysctl preload/configuration file"
msgstr "sysctl.conf - Vorlade-/Konfigurationsdatei für Sysctl"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: debian-bullseye
msgid ""
"B<sysctl.conf> is a simple file containing sysctl values to be read in and "
"set by B<sysctl>.  The syntax is simply as follows:"
msgstr ""
"B<sysctl.conf> ist eine einfache Datei, welche Sysctl-Werte enthält, die in "
"B<sysctl> gelesen und gesetzt werden. Die Syntax ist einfach, wie folgt:"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"# comment\n"
"; comment\n"
msgstr ""
"# Kommentar\n"
"; Kommentar\n"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid "token = value\n"
msgstr "Token = Wert\n"

# FIXME formatting
#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "Note that blank lines are ignored, and whitespace before and after a "
#| "token or value is ignored, although a value can contain whitespace "
#| "within.  Lines which begin with a # or ; are considered comments and "
#| "ignored."
msgid ""
"Note that blank lines are ignored, and whitespace before and after a token "
"or value is ignored, although a value can contain whitespace within.  Lines "
"which begin with a I<#> or I<;> are considered comments and ignored."
msgstr ""
"Beachten Sie, dass leere Zeilen sowie Leerräume vor und nach einem Token "
"oder einem Wert ignoriert werden, obwohl ein Token auch Leerraum enthalten "
"kann. Zeilen, die mit einem B<#> oder B<;> beginnen, werden als "
"Kommentarzeilen angesehen und ignoriert."

#. type: Plain text
#: debian-bullseye
msgid ""
"If a line begins with a single -, any attempts to set the value that fail "
"will be ignored."
msgstr ""

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: debian-bullseye
msgid ""
"As the B</etc/sysctl.conf> file is used to override default kernel parameter "
"values, only a small number of parameters is predefined in the file.  Use I</"
"sbin/sysctl\\ -a> or follow B<sysctl>(8)  to list all possible parameters. "
"The description of individual parameters can be found in the kernel "
"documentation."
msgstr ""
"Da die Datei B</etc/sysctl.conf> zur Außerkraftsetzung standardmäßiger Werte "
"der Kernelparameter verwendet wird, ist in der Datei nur eine kleine Anzahl "
"Parameter vordefiniert. Verwenden Sie I</sbin/sysctl\\ -a> oder folgen Sie "
"B<sysctl>(8), um alle möglichen Parameter aufzulisten. Die Beschreibungen "
"der einzelnen Parameter finden Sie in der Kerneldokumentation."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "EXAMPLE"
msgstr "BEISPIEL"

#. type: Plain text
#: debian-bullseye
#, no-wrap
msgid ""
"# sysctl.conf sample\n"
"#\n"
"  kernel.domainname = example.com\n"
"; this one has a space which will be written to the sysctl!\n"
"  kernel.modprobe = /sbin/mod probe\n"
msgstr ""
"# sysctl.conf sample\n"
"#\n"
"  kernel.domainname = example.com\n"
"; this one has a space which will be written to the sysctl!\n"
"  kernel.modprobe = /sbin/mod probe\n"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "FILES"
msgstr "DATEIEN"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "/etc/sysctl.d/*.conf"
msgid "I</etc/sysctl.d/*.conf>"
msgstr "/etc/sysctl.d/*.conf"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "/run/sysctl.d/*.conf"
msgid "I</run/sysctl.d/*.conf>"
msgstr "/run/sysctl.d/*.conf"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "/usr/local/lib/sysctl.d/*.conf"
msgid "I</usr/local/lib/sysctl.d/*.conf>"
msgstr "/usr/local/lib/sysctl.d/*.conf"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "/usr/lib/sysctl.d/*.conf"
msgid "I</usr/lib/sysctl.d/*.conf>"
msgstr "/usr/lib/sysctl.d/*.conf"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "/lib/sysctl.d/*.conf"
msgid "I</lib/sysctl.d/*.conf>"
msgstr "/lib/sysctl.d/*.conf"

#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid "/etc/sysctl.conf"
msgid "I</etc/sysctl.conf>"
msgstr "/etc/sysctl.conf"

# FIXME comand formatting
#. type: Plain text
#: debian-bullseye
#, fuzzy
#| msgid ""
#| "The paths where sysctl preload files usually exist.  See also sysctl "
#| "option I<--system>."
msgid ""
"The paths where B<sysctl> preload files usually exist.  See also B<sysctl> "
"option I<--system>."
msgstr ""
"Die Pfade, aus denen B<sysctl> Dateien vorlädt, existieren üblicherweise. "
"Siehe auch die Option B<--system> zu B<sysctl>."

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: debian-bullseye
msgid "B<sysctl>(8)"
msgstr "B<sysctl>(8)"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: debian-bullseye
msgid "E<.UR staikos@0wned.org> George Staikos E<.UE>"
msgstr "E<.UR staikos@0wned.org> George Staikos E<.UE>"

#. type: SH
#: debian-bullseye
#, no-wrap
msgid "REPORTING BUGS"
msgstr "FEHLER MELDEN"

#. type: Plain text
#: debian-bullseye
msgid "Please send bug reports to E<.UR procps@freelists.org> E<.UE>"
msgstr ""
"Bitte schicken Sie Fehlerberichte (auf Englisch) an E<.UR procps@freelists."
"org> E<.UE>"
