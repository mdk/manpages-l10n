# German translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Markus Schmitt <fw@math.uni-sb.de>, 1996.
# Martin Eberhard Schauer <Martin.E.Schauer@gmx.de>, 2012.
# Mario Blättermann <mario.blaettermann@gmail.com>, 2014.
msgid ""
msgstr ""
"Project-Id-Version: manpages-de\n"
"POT-Creation-Date: 2022-05-26 14:52+0200\n"
"PO-Revision-Date: 2021-04-05 08:08+0200\n"
"Last-Translator: Mario Blättermann <mario.blaettermann@gmail.com>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYSIGN"
msgstr "COPYSIGN"

#. type: TH
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22. März 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Linux-Programmierhandbuch"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "BEZEICHNUNG"

# The wording might not be perfect, but no better one has been suggested
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "copysign, copysignf, copysignl - copy sign of a number"
msgstr "copysign, copysignf, copysignl - kopiert das Vorzeichen einer Zahl"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "ÜBERSICHT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>math.hE<gt>>\n"
msgstr "B<#include E<lt>math.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<double copysign(double >I<x>B<, double >I<y>B<);>\n"
"B<float copysignf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double copysignl(long double >I<x>B<, long double >I<y>B<);>\n"
msgstr ""
"B<double copysign(double >I<x>B<, double >I<y>B<);>\n"
"B<float copysignf(float >I<x>B<, float >I<y>B<);>\n"
"B<long double copysignl(long double >I<x>B<, long double >I<y>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "Link with I<-lm>."
msgstr "Linken Sie mit der Option I<-lm>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr "Mit Glibc erforderliche Makros (siehe B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<copysign>(), B<copysignf>(), B<copysignl>():"
msgstr "B<copysign>(), B<copysignf>(), B<copysignl>():"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"    _ISOC99_SOURCE || _POSIX_C_SOURCE E<gt>= 200112L\n"
"        || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "BESCHREIBUNG"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"These functions return a value whose absolute value matches that of I<x>, "
"but whose sign bit matches that of I<y>."
msgstr ""
"Diese Funktionen liefern eine Zahl zurück, deren absoluter Wert gleich dem "
"von I<x> ist, deren Vorzeichen allerdings dem Vorzeichen von I<y> entspricht."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For example, I<copysign(42.0,\\ -1.0)> and I<copysign(-42.0, -1.0)> both "
"return -42.0."
msgstr ""
"Beispielsweise liefern I<copysign(42.0,\\ -1.0)> und I<copysign(-42.0, "
"-1.0)> beide -42.0 zurück."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "RÜCKGABEWERT"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"On success, these functions return a value whose magnitude is taken from "
"I<x> and whose sign is taken from I<y>."
msgstr ""
"Bei Erfolg geben diese Funktionen einen Wert zurück, dessen Betrag gleich "
"dem von I<x> ist und dessen Vorzeichen von I<y> stammt."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "If I<x> is a NaN, a NaN with the sign bit of I<y> is returned."
msgstr ""
"Falls I<x> ein NaN ist, wird ein NaN mit dem Vorzeichenbit von I<y> "
"zurückgegeben."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "FEHLER"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "No errors occur."
msgstr "Es treten keine Fehler auf."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Siehe B<attributes>(7) für eine Erläuterung der in diesem Abschnitt "
"verwandten Ausdrücke."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Schnittstelle"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Wert"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<copysign>(),\n"
"B<copysignf>(),\n"
"B<copysignl>()"
msgstr ""
"B<copysign>(),\n"
"B<copysignf>(),\n"
"B<copysignl>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Multithread-Fähigkeit"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "KONFORM ZU"

#.  4.3BSD.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"C99, POSIX.1-2001, POSIX.1-2008.  This function is defined in IEC 559 (and "
"the appendix with recommended functions in IEEE 754/IEEE 854)."
msgstr ""
"C99, POSIX.1-2001, POSIX.1-2008. Diese Funktion ist in IEC 559 (und dem "
"Anhang mit empfohlenen Funktionen in IEEE 754/IEEE 854) definiert."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "ANMERKUNGEN"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"On architectures where the floating-point formats are not IEEE 754 "
"compliant, these functions may treat a negative zero as positive."
msgstr ""
"Architekturen, auf denen die Fließkommaformate nicht zu IEEE 754 konform "
"sind, können diese Funktionen eine negative Null als positiv behandeln."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "SIEHE AUCH"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<signbit>(3)"
msgstr "B<signbit>(3)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "KOLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.13 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15. September 2017"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Since glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc versions E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"
msgstr ""
"_ISOC99_SOURCE || _POSIX_C_SOURCE\\ E<gt>=\\ 200112L\n"
"    || /* Seit Glibc 2.19: */ _DEFAULT_SOURCE\n"
"    || /* Glibc-Versionen E<lt>= 2.19: */ _BSD_SOURCE || _SVID_SOURCE\n"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 5.10 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Diese Seite ist Teil der Veröffentlichung 4.16 des Projekts Linux-I<man-"
"pages>. Eine Beschreibung des Projekts, Informationen, wie Fehler gemeldet "
"werden können sowie die aktuelle Version dieser Seite finden sich unter \\"
"%https://www.kernel.org/doc/man-pages/."
