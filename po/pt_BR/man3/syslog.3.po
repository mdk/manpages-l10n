# Brazilian Portuguese translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Valter Ferraz Sanches <vfs@ezlinux.cjb.net>, 2001.
# André Luiz Fassone <lonely_wolf@ig.com.br>, 2001.
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-05-26 15:21+0200\n"
"PO-Revision-Date: 2001-05-31 17:26+0200\n"
"Last-Translator: André Luiz Fassone <lonely_wolf@ig.com.br>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYSLOG"
msgstr "SYSLOG"

#. type: TH
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 março 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manual do Programador do Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "closelog, openlog, syslog, vsyslog - send messages to the system logger"
msgstr ""
"closelog, openlog, syslog, vsyslog - envia mensagens para o relator do "
"sistema"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSE"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>syslog.hE<gt>>"
msgid "B<#include E<lt>syslog.hE<gt>>\n"
msgstr "B<#include E<lt>syslog.hE<gt>>"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void openlog(const char *>I<ident>B<, int >I<option>B<, int >I<facility>B<);>"
msgid ""
"B<void openlog(const char *>I<ident>B<, int >I<option>B<, int >I<facility>B<);>\n"
"B<void syslog(int >I<priority>B<, const char *>I<format>B<, ...);>\n"
"B<void closelog(void);>\n"
msgstr "B<void openlog(const char *>I<ident>B<, int >I<option>B<, int >I<facility>B<);>"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<void vsyslog(int >I<priority>B<, const char *>I<format>B<, va_list >I<ap>B<);>"
msgid "B<void vsyslog(int >I<priority>B<, const char *>I<format>B<, va_list >I<ap>B<);>\n"
msgstr "B<void vsyslog(int >I<priority>B<, const char *>I<format>B<, va_list >I<ap>B<);>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Requisitos de macro de teste de recursos para o glibc (consulte "
"B<feature_test_macros>(7)):"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<syslog>(),\n"
#| "B<vsyslog>()"
msgid "B<vsyslog>():"
msgstr ""
"B<syslog>(),\n"
"B<vsyslog>()"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIÇÃO"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "openlog()"
msgstr "openlog()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<openlog>()  opens a connection to the system logger for a program."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The string pointed to by I<ident> is prepended to every message, and is "
"typically set to the program name.  If I<ident> is NULL, the program name is "
"used.  (POSIX.1-2008 does not specify the behavior when I<ident> is NULL.)"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The I<option> argument specifies flags which control the operation of "
"B<openlog>()  and subsequent calls to B<syslog>().  The I<facility> argument "
"establishes a default to be used if none is specified in subsequent calls to "
"B<syslog>().  The values that may be specified for I<option> and I<facility> "
"are described below."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The use of B<openlog>()  is optional; it will automatically be called by "
"B<syslog>()  if necessary, in which case I<ident> will default to NULL."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "syslog() and vsyslog()"
msgstr "syslog() e vsyslog()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<syslog>()  generates a log message, which will be distributed by "
"B<syslogd>(8)."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The I<priority> argument is formed by ORing together a I<facility> value and "
"a I<level> value (described below).  If no I<facility> value is ORed into "
"I<priority>, then the default value set by B<openlog>()  is used, or, if "
"there was no preceding B<openlog>()  call, a default of B<LOG_USER> is "
"employed."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The remaining arguments are a I<format>, as in B<printf>(3), and any "
"arguments required by the I<format>, except that the two-character sequence "
"B<%m> will be replaced by the error message string I<strerror>(I<errno>).  "
"The format string need not include a terminating newline character."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The function B<vsyslog>()  performs the same task as B<syslog>()  with the "
"difference that it takes a set of arguments which have been obtained using "
"the B<stdarg>(3)  variable argument list macros."
msgstr ""

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "closelog()"
msgstr "closelog()"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid ""
#| "B<closelog()> closes the descriptor being used to write to the system "
#| "logger.  The use of B<closelog()> is optional."
msgid ""
"B<closelog>()  closes the file descriptor being used to write to the system "
"logger.  The use of B<closelog>()  is optional."
msgstr ""
"B<closelog()> fecha o descritor usado para escrever para o relator do "
"sistema. O uso de B<closelog()> é opcional."

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Values for I<option>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid "The I<option> argument to B<openlog()> is an OR of any of these:"
msgid ""
"The I<option> argument to B<openlog>()  is a bit mask constructed by ORing "
"together any of the following values:"
msgstr "O argumento I<option> para B<openlog()> é um OR de qualquer destes:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_CONS>"
msgstr "B<LOG_CONS>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Write directly to the system console if there is an error while sending to "
"the system logger."
msgstr ""
"Escreve diretamente no console do sistema se ocorrer um erro durante o envio "
"da mensagem ao relator do sistema."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NDELAY>"
msgstr "B<LOG_NDELAY>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Open the connection immediately (normally, the connection is opened when the "
"first message is logged).  This may be useful, for example, if a subsequent "
"B<chroot>(2)  would make the pathname used internally by the logging "
"facility unreachable."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NOWAIT>"
msgstr "B<LOG_NOWAIT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Don't wait for child processes that may have been created while logging the "
"message.  (The GNU C library does not create a child process, so this option "
"has no effect on Linux.)"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_ODELAY>"
msgstr "B<LOG_ODELAY>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The converse of B<LOG_NDELAY>; opening of the connection is delayed until "
"B<syslog>()  is called.  (This is the default, and need not be specified.)"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_PERROR>"
msgstr "B<LOG_PERROR>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"(Not in POSIX.1-2001 or POSIX.1-2008.)  Also log the message to I<stderr>."
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_PID>"
msgstr "B<LOG_PID>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
#| msgid "include PID with each message"
msgid "Include the caller's PID with each message."
msgstr "inclui PID em cada mensagem"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Values for I<facility>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The I<facility> argument is used to specify what type of program is logging "
"the message.  This lets the configuration file specify that messages from "
"different facilities will be handled differently."
msgstr ""
"O argumento I<facility> é usado para especificar que tipo de programa está "
"relatando a mensagem. Isto deixa o arquivo de configuração especificar que "
"mensagens de diferentes facilities serão manipuladas de maneira diferenciada."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_AUTH>"
msgstr "B<LOG_AUTH>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "security/authorization messages"
msgstr "mensagens de segurança/autorização"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_AUTHPRIV>"
msgstr "B<LOG_AUTHPRIV>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "security/authorization messages (private)"
msgstr "mensagens de segurança/autorização (privadas)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_CRON>"
msgstr "B<LOG_CRON>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "clock daemon (B<cron> and B<at>)"
msgstr "daemon de relógio (B<cron> e B<at>)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_DAEMON>"
msgstr "B<LOG_DAEMON>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "system daemons without separate facility value"
msgstr "outros daemons de sistema"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_FTP>"
msgstr "B<LOG_FTP>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "ftp daemon"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_KERN>"
msgstr "B<LOG_KERN>"

#.  LOG_KERN has the value 0; if used as a facility, zero translates to:
#.  "use the default facility".
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "kernel messages (these can't be generated from user processes)"
msgstr ""

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_LOCAL0> through B<LOG_LOCAL7>"
msgstr "B<LOG_LOCAL0> até B<LOG_LOCAL7>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "reserved for local use"
msgstr "resevadas para uso local"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_LPR>"
msgstr "B<LOG_LPR>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "line printer subsystem"
msgstr "subsistema de impressora de linha"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_MAIL>"
msgstr "B<LOG_MAIL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "mail subsystem"
msgstr "subsistema de correio"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NEWS>"
msgstr "B<LOG_NEWS>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "USENET news subsystem"
msgstr "subsistema de notícias USENET"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_SYSLOG>"
msgstr "B<LOG_SYSLOG>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "messages generated internally by B<syslogd>(8)"
msgstr "mensagens geradas internamente por B<syslogd>(8)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_USER> (default)"
msgstr "B<LOG_USER> (padrão)"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "generic user-level messages"
msgstr "mensagens genéricas de nível de usuário"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_UUCP>"
msgstr "B<LOG_UUCP>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "UUCP subsystem"
msgstr "subsistema UUCP"

#. type: SS
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Values for I<level>"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This determines the importance of the message.  The levels are, in order of "
"decreasing importance:"
msgstr ""
"Isto determina a importância da mensagem. Os níveis (levels) são, em ordem "
"de importância decrescente:"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_EMERG>"
msgstr "B<LOG_EMERG>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "system is unusable"
msgstr "o sistema está inoperante"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_ALERT>"
msgstr "B<LOG_ALERT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "action must be taken immediately"
msgstr "ação deve ser tomada imediatamente"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_CRIT>"
msgstr "B<LOG_CRIT>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "critical conditions"
msgstr "condições críticas"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_ERR>"
msgstr "B<LOG_ERR>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "error conditions"
msgstr "condições de erro"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_WARNING>"
msgstr "B<LOG_WARNING>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "warning conditions"
msgstr "condições de aviso"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_NOTICE>"
msgstr "B<LOG_NOTICE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "normal, but significant, condition"
msgstr "condição normal mas significante"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_INFO>"
msgstr "B<LOG_INFO>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "informational message"
msgstr "mensagem informativa"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LOG_DEBUG>"
msgstr "B<LOG_DEBUG>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "debug-level message"
msgstr "mensagem em nível de depuração"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The function B<setlogmask>(3)  can be used to restrict logging to specified "
"levels only."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATRIBUTOS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Para uma explicação dos termos usados nesta seção, consulte B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Atributo"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valor"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<openlog>(),\n"
"B<closelog>()"
msgstr ""
"B<openlog>(),\n"
"B<closelog>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Thread safety"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<syslog>(),\n"
"B<vsyslog>()"
msgstr ""
"B<syslog>(),\n"
"B<vsyslog>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe env locale"
msgstr "MT-Safe env locale"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "DE ACORDO COM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The functions B<openlog>(), B<closelog>(), and B<syslog>()  (but not "
"B<vsyslog>())  are specified in SUSv2, POSIX.1-2001, and POSIX.1-2008."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"POSIX.1-2001 specifies only the B<LOG_USER> and B<LOG_LOCAL*> values for "
"I<facility>.  However, with the exception of B<LOG_AUTHPRIV> and B<LOG_FTP>, "
"the other I<facility> values appear on most UNIX systems."
msgstr ""

#.  .SH HISTORY
#.  A
#.  .BR syslog ()
#.  function call appeared in 4.2BSD.
#.  4.3BSD documents
#.  .BR openlog (),
#.  .BR syslog (),
#.  .BR closelog (),
#.  and
#.  .BR setlogmask ().
#.  4.3BSD-Reno also documents
#.  .BR vsyslog ().
#.  Of course early v* functions used the
#.  .I <varargs.h>
#.  mechanism, which is not compatible with
#.  .IR <stdarg.h> .
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<LOG_PERROR> value for I<option> is not specified by POSIX.1-2001 or "
"POSIX.1-2008, but is available in most versions of UNIX."
msgstr ""

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTAS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The argument I<ident> in the call of B<openlog>()  is probably stored as-"
"is.  Thus, if the string it points to is changed, B<syslog>()  may start "
"prepending the changed string, and if the string it points to ceases to "
"exist, the results are undefined.  Most portable is to use a string constant."
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Never pass a string with user-supplied data as a format, use the following "
"instead:"
msgstr ""

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "syslog(priority, \"%s\", string);\n"
msgstr "syslog(priority, \"%s\", string);\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VEJA TAMBÉM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<journalctl>(1), B<logger>(1), B<setlogmask>(3), B<syslog.conf>(5), "
"B<syslogd>(8)"
msgstr ""
"B<journalctl>(1), B<logger>(1), B<setlogmask>(3), B<syslog.conf>(5), "
"B<syslogd>(8)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOFÃO"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 5.13 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: TH
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15 setembro 2017"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<#include E<lt>syslog.hE<gt>>"
msgstr "B<#include E<lt>syslog.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"B<void openlog(const char *>I<ident>B<, int >I<option>B<, int "
">I<facility>B<);>"
msgstr ""
"B<void openlog(const char *>I<ident>B<, int >I<option>B<, int "
">I<facility>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<void syslog(int >I<priority>B<, const char *>I<format>B<, ...);>"
msgstr "B<void syslog(int >I<priority>B<, const char *>I<format>B<, ...);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<void closelog(void);>"
msgstr "B<void closelog(void);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"B<void vsyslog(int >I<priority>B<, const char *>I<format>B<, va_list "
">I<ap>B<);>"
msgstr ""
"B<void vsyslog(int >I<priority>B<, const char *>I<format>B<, va_list "
">I<ap>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"B<vsyslog>():\n"
"    Since glibc 2.19:\n"
"        _DEFAULT_SOURCE\n"
"    Glibc 2.19 and earlier:\n"
"        _BSD_SOURCE\n"
msgstr ""

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 5.10 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Esta página faz parte da versão 4.16 do projeto Linux I<man-pages>. Uma "
"descrição do projeto, informações sobre relatórios de bugs e a versão mais "
"recente desta página podem ser encontradas em \\%https://www.kernel.org/doc/"
"man-pages/."
