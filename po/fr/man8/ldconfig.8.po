# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012, 2013.
# Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>, 2021
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2022-06-16 17:13+0200\n"
"PO-Revision-Date: 2021-03-29 10:14+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: vim\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "LDCONFIG"
msgstr "LDCONFIG"

#. type: TH
#: archlinux debian-unstable fedora-36 fedora-rawhide opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 mars 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "ldconfig - configure dynamic linker run-time bindings"
msgstr "ldconfig - Configuration de l'éditeur de liens dynamiques"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B</sbin/ldconfig> [B<-nNvXV>] [B<-f> I<conf>] [B<-C> I<cache>] [B<-r> "
"I<root>] I<directory>..."
msgstr ""
"B</sbin/ldconfig> [B<-nNvXV>] [B<-f>\\ I<configuration>] [B<-C>\\ I<cache>] "
"[B<-r>\\ I<racine>] I<répertoire>\\ ..."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B</sbin/ldconfig> B<-l> [B<-v>] I<library>..."
msgstr "B</sbin/ldconfig> B<-l> [B<-v>] I<bibliothèque>\\ ..."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B</sbin/ldconfig> B<-p>"
msgstr "B</sbin/ldconfig> B<-p>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<ldconfig> creates the necessary links and cache to the most recent shared "
"libraries found in the directories specified on the command line, in the "
"file I</etc/ld.so.conf>, and in the trusted directories, I</lib> and I</usr/"
"lib> (on some 64-bit architectures such as x86-64, I</lib> and I</usr/lib> "
"are the trusted directories for 32-bit libraries, while I</lib64> and I</usr/"
"lib64> are used for 64-bit libraries)."
msgstr ""
"B<ldconfig> crée les liens nécessaires et met en cache les bibliothèques "
"partagées les plus récentes trouvées dans les répertoires indiqués sur la "
"ligne de commande, dans le fichier I</etc/ld.so.conf> et dans les "
"répertoires sûrs I</lib> et I</usr/lib> (sur certaines architectures 64 bits "
"telle x86-64, I</lib> et I</usr/lib> sont des répertoires sûrs pour les "
"bibliothèques 32 bits, tandis que I</lib64> et I</usr/lib64> sont utilisés "
"pour les bibliothèques 64 bits."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The cache is used by the run-time linker, I<ld.so> or I<ld-linux.so>.  "
"B<ldconfig> checks the header and filenames of the libraries it encounters "
"when determining which versions should have their links updated."
msgstr ""
"Le cache est utilisé par l’éditeur de liens, I<ld.so> ou I<ld-linux.so>. "
"B<ldconfig> vérifie les en-têtes et les noms de fichier des bibliothèques "
"qu'il trouve lors de la détermination des versions devant mettre à jour "
"leurs liens."

#.  The following sentence looks suspect
#.  (perhaps historical cruft) -- MTK, Jul 2005
#.  Therefore, when making dynamic libraries,
#.  it is wise to explicitly link against libc (use \-lc).
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<ldconfig> will attempt to deduce the type of ELF libraries (i.e., libc5 or "
"libc6/glibc)  based on what C libraries, if any, the library was linked "
"against."
msgstr ""
"B<ldconfig> essaye de déduire le type de bibliothèques ELF (c'est-à-dire, "
"libc5 ou libc6/glibc) en se basant sur quelle bibliothèque\\ C, si tel est "
"le cas, la bibliothèque est liée."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Some existing libraries do not contain enough information to allow the "
"deduction of their type.  Therefore, the I</etc/ld.so.conf> file format "
"allows the specification of an expected type.  This is used I<only> for "
"those ELF libraries which we can not work out.  The format is "
"\"dirname=TYPE\", where TYPE can be libc4, libc5, or libc6.  (This syntax "
"also works on the command line.)  Spaces are I<not> allowed.  Also see the "
"B<-p> option.  B<ldconfig> should normally be run by the superuser as it may "
"require write permission on some root owned directories and files."
msgstr ""
"Certaines bibliothèques existantes ne contiennent pas assez d'informations "
"pour déduire leur type. Par conséquent, le format du fichier I</etc/ld.so."
"conf> permet d'indiquer le type attendu. Cela ne doit servir B<que> pour les "
"bibliothèques ELF que nous ne pouvons pas déterminer. Le format est "
"« répertoire=TYPE », où TYPE peut être libc4, libc5 ou libc6. (Cette syntaxe "
"fonctionne aussi sur la ligne de commande). Les espaces ne sont B<pas> "
"autorisées. Consultez aussi l'option B<-p>. B<ldconfig> devrait normalement "
"être exécuté par le superutilisateur, car des permissions en écriture sont "
"nécessaires sur des répertoires et des fichiers qui lui appartiennent."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"Note that B<ldconfig> will only look at files that are named I<lib*.so*> "
"(for regular shared objects) or I<ld-*.so*> (for the dynamic loader "
"itself).  Other files will be ignored.  Also, B<ldconfig> expects a certain "
"pattern to how the symlinks are set up, like this example, where the middle "
"file (B<libfoo.so.1> here) is the SONAME for the library:"
msgstr ""
"Remarquez que B<ldconfig> ne recherchera que les fichiers dénommés I<lib*."
"so*> (pour les objets partagés normaux) ou I<ld-*.so*> (pour l’éditeur de "
"liens dynamiques lui-même). Les autres fichiers seront ignorés. Aussi, "
"B<ldconfig> s’attend à un certain modèle de configuration de liens "
"dynamiques, tel que cet exemple où le fichier du milieu (I<libtoto.so.1> "
"ici) est le SONAME de la bibliothèque :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-tumbleweed
#, no-wrap
msgid "libfoo.so -E<gt> libfoo.so.1 -E<gt> libfoo.so.1.12\n"
msgstr "libtoto.so -E<gt> libtoto.so.1 -E<gt> libtoto.so.1.12\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"Failure to follow this pattern may result in compatibility issues after an "
"upgrade."
msgstr ""
"Un manquement dans le suivi de ce modèle peut aboutir à des problèmes de "
"compatibilité après une mise à niveau."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "OPTIONS"
msgstr "OPTIONS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-c> I<fmt>, B<--format=>I<fmt>"
msgstr "B<-c> I<format>, B<--format=>I<format>"

#.  commit cad64f778aced84efdaa04ae64f8737b86f063ab
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-tumbleweed
msgid ""
"(Since glibc 2.2)  Cache format to use: I<old>, I<new>, or I<compat>.  Since "
"glibc 2.32, the default is I<new>.  Before that, it was I<compat>."
msgstr ""
"Depuis la version 2.2 de la glibc, le format de cache à utiliser est I<old>, "
"I<new> ou I<compat>. Depuis la version 2.32, la valeur par défaut est "
"I<new>. Avant cela, c’était I<compat>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-C >I<cache>"
msgstr "B<-C> I<cache>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Use I<cache> instead of I</etc/ld.so.cache>."
msgstr "Utiliser le I<cache> indiqué plutôt que I</etc/ld.so.cache>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-f >I<conf>"
msgstr "B<-f> I<configuration>"

#.  FIXME glibc 2.7 added -i
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Use I<conf> instead of I</etc/ld.so.conf>."
msgstr ""
"Utiliser le fichier I<configuration> indiqué plutôt que I</etc/ld.so.conf>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-i>, B<--ignore-aux-cache>"
msgstr "B<-i>, B<--ignore-aux-cache>"

#.              commit 27d9ffda17df4d2388687afd12897774fde39bcc
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "(Since glibc 2.7)  Ignore auxiliary cache file."
msgstr ""
"Depuis la version 2.7 de la glibc, ignorer le fichier auxiliaire de cache."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-l>"
msgstr "B<-l>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"(Since glibc 2.2)  Library mode.  Manually link individual libraries.  "
"Intended for use by experts only."
msgstr ""
"Depuis la version 2.2 de la glibc, mode bibliothèque. Lier manuellement les "
"bibliothèques particulières (experts uniquement)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-n>"
msgstr "B<-n>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Process only the directories specified on the command line.  Don't process "
"the trusted directories, nor those specified in I</etc/ld.so.conf>.  Implies "
"B<-N>."
msgstr ""
"Ne traiter que les répertoires indiqués sur la ligne de commande. Ne pas "
"s'occuper des répertoires sûrs ni de ceux indiqués dans I</etc/ld.so.conf>. "
"Implique l'option B<-N>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-N>"
msgstr "B<-N>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Don't rebuild the cache.  Unless B<-X> is also specified, links are still "
"updated."
msgstr ""
"Ne pas reconstruire le cache. Si l'option B<-X> n'est pas indiquée, les "
"liens sont quand même mis à jour."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-p>, B<--print-cache>"
msgstr "B<-p>, B<--print-cache>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Print the lists of directories and candidate libraries stored in the current "
"cache."
msgstr ""
"Afficher les listes des répertoires et des bibliothèques potentielles "
"enregistrées dans le cache actuel."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-r >I<root>"
msgstr "B<-r> I<racine>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Change to and use I<root> as the root directory."
msgstr "Se déplacer dans le répertoire I<racine> indiqué et l’utiliser."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr "B<-v>, B<--verbose>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Verbose mode.  Print current version number, the name of each directory as "
"it is scanned, and any links that are created.  Overrides quiet mode."
msgstr ""
"Mode détaillé. Afficher le numéro de version actuelle, le nom de chaque "
"répertoire au fur et à mesure du parcours, et les liens qui sont créés. Peut "
"surcharger le mode silencieux."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr "B<-V>, B<--version>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Print program version."
msgstr "Afficher la version du programme."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-X>"
msgstr "B<-X>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Don't update links.  Unless B<-N> is also specified, the cache is still "
"rebuilt."
msgstr ""
"Ne pas mettre à jour les liens. Si l'option B<-N> n'est pas indiquée, le "
"cache est quand même reconstruit."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "FILES"
msgstr "FICHIERS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I</lib/ld.so>"
msgstr "I</lib/ld.so>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "Run-time linker/loader."
msgstr "Éditeur de liens dynamiques"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/ld.so.conf>"
msgstr "I</etc/ld.so.conf>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"File containing a list of directories, one per line, in which to search for "
"libraries."
msgstr ""
"Fichier contenant une liste de répertoires, un par ligne, où chercher les "
"bibliothèques."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "I</etc/ld.so.cache>"
msgstr "I</etc/ld.so.cache>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"File containing an ordered list of libraries found in the directories "
"specified in I</etc/ld.so.conf>, as well as those found in the trusted "
"directories."
msgstr ""
"Fichier contenant une liste ordonnée des bibliothèques trouvées dans les "
"répertoires indiqués dans I</etc/ld.so.conf>, ainsi que celles trouvées dans "
"les répertoires sûrs."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<ldd>(1), B<ld.so>(8)"
msgstr "B<ldd>(1), B<ld.so>(8)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.13 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-06-09"
msgstr "9 juin 2020"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"(Since glibc 2.2)  Cache format to use: I<old>, I<new>, or I<compat> "
"(default)."
msgstr ""
"Depuis la version 2.2 de la glibc, le format de cache à utiliser est I<old>, "
"I<new> ou I<compat> (valeur par défaut)."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
