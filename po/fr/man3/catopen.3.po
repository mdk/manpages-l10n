# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2013, 2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2022-05-26 14:51+0200\n"
"PO-Revision-Date: 2018-09-10 20:54+0000\n"
"Last-Translator: Weblate Admin <jean-baptiste@holcroft.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CATOPEN"
msgstr "CATOPEN"

#. type: TH
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 mars 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "catopen, catclose - open/close a message catalog"
msgstr "catopen, catclose - Ouverture et fermeture d'un catalogue de messages"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<#include E<lt>nl_types.hE<gt>>"
msgid "B<#include E<lt>nl_types.hE<gt>>\n"
msgstr "B<#include E<lt>nl_types.hE<gt>>"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid "B<nl_catd catopen(const char *>I<name>B<, int >I<flag>B<);>"
msgid ""
"B<nl_catd catopen(const char *>I<name>B<, int >I<flag>B<);>\n"
"B<int catclose(nl_catd >I<catalog>B<);>\n"
msgstr "B<nl_catd catopen(const char *>I<name>B<, int >I<flag>B<);>"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The function B<catopen>()  opens a message catalog and returns a catalog "
"descriptor.  The descriptor remains valid until B<catclose>()  or "
"B<execve>(2).  If a file descriptor is used to implement catalog "
"descriptors, then the B<FD_CLOEXEC> flag will be set."
msgstr ""
"La fonction B<catopen>() ouvre un catalogue de messages, et en retourne un "
"descripteur. Celui-ci reste valide jusqu'à un appel B<catclose>() ou "
"B<execve>(2). Si un descripteur de fichier est utilisé pour implémenter le "
"descripteur de catalogue, le drapeau B<FD_CLOEXEC> sera défini."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The argument I<name> specifies the name of the message catalog to be "
"opened.  If I<name> specifies an absolute path (i.e., contains a \\(aq/"
"\\(aq), then I<name> specifies a pathname for the message catalog.  "
"Otherwise, the environment variable B<NLSPATH> is used with I<name> "
"substituted for B<%N> (see B<locale>(7)).  It is unspecified whether "
"B<NLSPATH> will be used when the process has root privileges.  If B<NLSPATH> "
"does not exist in the environment, or if a message catalog cannot be opened "
"in any of the paths specified by it, then an implementation defined path is "
"used.  This latter default path may depend on the B<LC_MESSAGES> locale "
"setting when the I<flag> argument is B<NL_CAT_LOCALE> and on the B<LANG> "
"environment variable when the I<flag> argument is 0.  Changing the "
"B<LC_MESSAGES> part of the locale may invalidate open catalog descriptors."
msgstr ""
"L'argument I<name> indique le nom du catalogue à ouvrir. Si I<name> indique "
"un chemin d'accès absolu (contenant un «\\ /\\ » )  alors il est employé "
"comme chemin d'accès au catalogue. Sinon, la variable d'environnement "
"B<NLSPATH> est utilisée, après avoir substitué I<name> à son argument B<%N> "
"(consultez B<locale>(7)). Lorsque le processus à des privilèges root, "
"l'emploi éventuel de B<NLSPATH> n'est pas garanti. Si B<NLSPATH> n'est pas "
"définie dans l'environnement, ou si le catalogue de messages ne peut être "
"ouvert dans aucun des chemins qu'elle contient, alors un chemin prédéfini, "
"dépendant de l'implémentation, est utilisé. Ce dernier peut dépendre de la "
"catégorie de localisation B<LC_MESSAGES> si l'argument I<flag> vaut "
"B<NL_CAT_LOCALE> ou de la variable d'environnement B<LANG> si l'argument "
"I<flag> vaut zéro. Changer la partie B<LC_MESSAGES> de la localisation, peut "
"rendre invalides les descripteurs de catalogues déjà ouverts."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The I<flag> argument to B<catopen>()  is used to indicate the source for the "
"language to use.  If it is set to B<NL_CAT_LOCALE>, then it will use the "
"current locale setting for B<LC_MESSAGES>.  Otherwise, it will use the "
"B<LANG> environment variable."
msgstr ""
"L'argument I<flag> de B<catopen>() indique l'origine du langage à utiliser. "
"S'il vaut B<NL_CAT_LOCALE> alors il utilisera la configuration actuelle de "
"la localisation définie par B<LC_MESSAGES>. Sinon, il utilisera la variable "
"d'environnement B<LANG>."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The function B<catclose>()  closes the message catalog identified by "
"I<catalog>.  It invalidates any subsequent references to the message catalog "
"defined by I<catalog>."
msgstr ""
"La fonction B<catclose>() ferme le catalogue identifié par I<catalog>. Ceci "
"invalide toute référence ultérieure au catalogue de message défini par le "
"descripteur I<catalog>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The function B<catopen>()  returns a message catalog descriptor of type "
"I<nl_catd> on success.  On failure, it returns I<(nl_catd)\\ -1> and sets "
"I<errno> to indicate the error.  The possible error values include all "
"possible values for the B<open>(2)  call."
msgstr ""
"La fonction B<catopen>() renvoie un descripteur de catalogue de messages du "
"type I<nl_catd> si elle réussit. En cas d'échec, elle renvoie I<(nl_catd)\\ "
"-1> et définit I<errno> avec le code d'erreur. Les erreurs possibles "
"incluent toutes celles que peut renvoyer B<open>(2)."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "The function B<catclose>()  returns 0 on success, or -1 on failure."
msgstr ""
"La fonction B<catclose>() renvoie 0 si elle réussit, ou -1 en cas d'échec."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr "ENVIRONNEMENT"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LC_MESSAGES>"
msgstr "B<LC_MESSAGES>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"May be the source of the B<LC_MESSAGES> locale setting, and thus determine "
"the language to use if I<flag> is set to B<NL_CAT_LOCALE>."
msgstr ""
"Peut-être la source de la définition de la localisation B<LC_MESSAGES> et "
"peut servir à déterminer le langage à utiliser, si I<flag> vaut "
"B<NL_CAT_LOCALE>."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<LANG>"
msgstr "B<LANG>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "The language to use if I<flag> is 0."
msgstr "Le langage à utiliser, si I<flag> vaut zéro."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<catopen>()"
msgstr ""

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe env"
msgstr "MT-Safe env"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy, no-wrap
msgid "B<catclose>()"
msgstr "B<duplocale>() :"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#.  In XPG 1987, Vol. 3 it says:
#.  .I "The flag argument of catopen is reserved for future use"
#.  .IR "and should be set to 0" .
#.  It is unclear what the source was for the constants
#.  .B MCLoadBySet
#.  and
#.  .B MCLoadAll
#.  (see below).
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr "POSIX.1-2001, POSIX.1-2008."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#.  (Compare
#.  .B MCLoadAll
#.  below.)
#.  .SS Linux notes
#.  These functions are available for Linux since libc 4.4.4c.
#.  In the case of linux libc4 and libc5, the catalog descriptor
#.  .I nl_catd
#.  is a
#.  .BR mmap (2)'ed
#.  area of memory and not a file descriptor.
#.  The
#.  .I flag
#.  argument to
#.  .BR catopen ()
#.  should be either
#.  .B MCLoadBySet
#.  (=0) or
#.  .B MCLoadAll
#.  (=1).
#.  The former value indicates that a set from the catalog is to be
#.  loaded when needed, whereas the latter causes the initial call to
#.  .BR catopen ()
#.  to load the entire catalog into memory.
#.  The default search path varies, but usually looks at a number of places below
#.  .I /etc/locale
#.  and
#.  .IR /usr/lib/locale .
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, fuzzy
msgid ""
"The above is the POSIX.1 description.  The glibc value for B<NL_CAT_LOCALE> "
"is 1.  The default path varies, but usually looks at a number of places "
"below I</usr/share/locale>."
msgstr ""
"Tout ceci correspond à la description POSIX.1-2001. La valeur de "
"B<NL_CAT_LOCALE> de la glibc est 1 (Comparez avec B<MCLoadAll> plus bas). Le "
"chemin par défaut varie, mais il inclut en général plusieurs emplacements "
"sous I</usr/share/locale>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<catgets>(3), B<setlocale>(3)"
msgstr "B<catgets>(3), B<setlocale>(3)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.13 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "2015-08-08"
msgstr "8 août 2015"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<#include E<lt>nl_types.hE<gt>>"
msgstr "B<#include E<lt>nl_types.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<nl_catd catopen(const char *>I<name>B<, int >I<flag>B<);>"
msgstr "B<nl_catd catopen(const char *>I<name>B<, int >I<flag>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<int catclose(nl_catd >I<catalog>B<);>"
msgstr "B<int catclose(nl_catd >I<catalog>B<);>"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
