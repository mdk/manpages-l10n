# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2022-05-26 15:06+0200\n"
"PO-Revision-Date: 2021-11-19 08:41+0800\n"
"Last-Translator: Grégoire Scano <gregoire.scano@malloc.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MKSTEMP"
msgstr "MKSTEMP"

#. type: TH
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 mars 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "mkstemp, mkostemp, mkstemps, mkostemps - create a unique temporary file"
msgstr ""
"mkstemp, mkostemp, mkstemps, mkostemps - Créer un fichier temporaire unique"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>stdlib.hE<gt>>\n"
msgstr "B<#include E<lt>stdlib.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<int mkstemp(char *>I<template>B<);>\n"
"B<int mkostemp(char *>I<template>B<, int >I<flags>B<);>\n"
"B<int mkstemps(char *>I<template>B<, int >I<suffixlen>B<);>\n"
"B<int mkostemps(char *>I<template>B<, int >I<suffixlen>B<, int >I<flags>B<);>\n"
msgstr ""
"B<int mkstemp(char *>I<template>B<);>\n"
"B<int mkostemp(char *>I<template>B<, int >I<flags>B<);>\n"
"B<int mkstemps(char *>I<template>B<, int >I<suffixlen>B<);>\n"
"B<int mkostemps(char *>I<template>B<, int >I<suffixlen>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""
"Exigences de macros de test de fonctionnalités pour la glibc (consulter "
"B<feature_test_macros>(7)) :"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<mkstemp>():"
msgstr "B<mkstemp>() :"

#.     || _XOPEN_SOURCE && _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Since glibc 2.12: */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    _XOPEN_SOURCE E<gt>= 500\n"
"        || /* Depuis la glibc 2.12 : */ _POSIX_C_SOURCE E<gt>= 200809L\n"
"        || /* Versions de la glibc E<lt>= 2.19 : */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<mkostemp>():"
msgstr "B<mkostemp>():"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "    _GNU_SOURCE\n"
msgstr "    _GNU_SOURCE\n"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<mkstemps>():"
msgstr "B<mkstemps>():"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"    /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"    /* Depuis la glibc 2.19 : */ _DEFAULT_SOURCE\n"
"        || /* Versions de la glibc E<lt>= 2.19 : */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid "B<mkostemps>():"
msgstr "B<mkostemps>():"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<mkstemp>()  function generates a unique temporary filename from "
"I<template>, creates and opens the file, and returns an open file descriptor "
"for the file."
msgstr ""
"La fonction B<mkstemp>() engendre un nom de fichier temporaire unique, à "
"partir du motif I<template>, crée et ouvre le fichier, et renvoie un "
"descripteur de fichier ouvert pour ce fichier."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The last six characters of I<template> must be \"XXXXXX\" and these are "
"replaced with a string that makes the filename unique.  Since it will be "
"modified, I<template> must not be a string constant, but should be declared "
"as a character array."
msgstr ""
"Les 6 derniers caractères de I<template> doivent être XXXXXX, et ils seront "
"remplacés par une chaîne rendant le nom de fichier unique. Comme il sera "
"modifié, I<template> ne doit pas être une chaîne constante, mais un tableau "
"de caractères."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The file is created with permissions 0600, that is, read plus write for "
"owner only.  The returned file descriptor provides both read and write "
"access to the file.  The file is opened with the B<open>(2)  B<O_EXCL> flag, "
"guaranteeing that the caller is the process that creates the file."
msgstr ""
"Le fichier est créé en mode 0600, c'est-à-dire en lecture et écriture pour "
"le propriétaire seulement. Le descripteur de fichier renvoyé fournit les "
"accès en lecture et en écriture sur le fichier. Le fichier est ouvert avec "
"l'attribut B<O_EXCL> de B<open>(2), garantissant que l'appelant soit le "
"processus qui a créé le fichier."

#.  Reportedly, FreeBSD
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<mkostemp>()  function is like B<mkstemp>(), with the difference that "
"the following bits\\(emwith the same meaning as for B<open>(2)\\(emmay be "
"specified in I<flags>: B<O_APPEND>, B<O_CLOEXEC>, and B<O_SYNC>.  Note that "
"when creating the file, B<mkostemp>()  includes the values B<O_RDWR>, "
"B<O_CREAT>, and B<O_EXCL> in the I<flags> argument given to B<open>(2); "
"including these values in the I<flags> argument given to B<mkostemp>()  is "
"unnecessary, and produces errors on some systems."
msgstr ""
"La fonction B<mkostemp>() agit comme B<mkstemp>(), à la différence que les "
"bits suivants — avec la même signification que pour B<open>(2) — peuvent "
"être définis dans I<flags> : B<O_APPEND>, B<O_CLOEXEC> et B<O_SYNC>. "
"Remarquez qu’en créant le fichier, B<mkostemp>() contient les valeurs "
"B<O_RDWR>, B<O_CREAT> et B<O_EXCL> dans l’argument I<flags> donné à "
"B<open>(2) ; inclure ces valeurs dans l’argument I<flags> donné à "
"B<mkostemp>() n’est pas nécessaire et produit des erreurs sur certains "
"systèmes."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<mkstemps>()  function is like B<mkstemp>(), except that the string in "
"I<template> contains a suffix of I<suffixlen> characters.  Thus, I<template> "
"is of the form I<prefixXXXXXXsuffix>, and the string XXXXXX is modified as "
"for B<mkstemp>()."
msgstr ""
"La fonction B<mkstemps>() agit comme B<mkstemp>() sauf que la chaîne dans "
"I<template> contient un suffixe de I<suffixlen> caractères. Ainsi, "
"I<template> est de la forme I<préfixeXXXXXXsuffixe> et la chaîne XXXXXX est "
"modifiée comme dans B<mkstemp>()."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<mkostemps>()  function is to B<mkstemps>()  as B<mkostemp>()  is to "
"B<mkstemp>()."
msgstr ""
"La fonction B<mkostemps>() est à B<mkstemps>() ce que B<mkostemp>() est à "
"B<mkstemp>()."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On success, these functions return the file descriptor of the temporary "
"file.  On error, -1 is returned, and I<errno> is set to indicate the error."
msgstr ""
"Si elles réussissent, ces fonctions renvoient le descripteur du fichier "
"temporaire créé ou B<-1> si elles échouent, auquel cas I<errno> contient le "
"code d'erreur."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EEXIST>"
msgstr "B<EEXIST>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Could not create a unique temporary filename.  Now the contents of "
"I<template> are undefined."
msgstr ""
"Impossible de créer un nom de fichier temporaire unique. Le contenu "
"I<template> est alors imprévisible."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For B<mkstemp>()  and B<mkostemp>(): The last six characters of I<template> "
"were not XXXXXX; now I<template> is unchanged."
msgstr ""
"Pour B<mkstemp>() et B<mkostemp>() : les six derniers caractères de "
"I<template> ne sont pas XXXXXX ; I<template> n'est pas modifié."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For B<mkstemps>()  and B<mkostemps>(): I<template> is less than I<(6 + "
"suffixlen)> characters long, or the last 6 characters before the suffix in "
"I<template> were not XXXXXX."
msgstr ""
"Pour B<mkstemps>() et B<mkostemps>() : I<template> a une longueur inférieure "
"à I<(6 + suffixlen)>, ou les six derniers caractères avant le suffixe ne "
"sont pas XXXXXX."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"These functions may also fail with any of the errors described for "
"B<open>(2)."
msgstr ""
"Ces fonctions peuvent également échouer avec une des erreurs décrites pour "
"B<open>(2)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr "VERSIONS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<mkostemp>()  is available since glibc 2.7.  B<mkstemps>()  and "
"B<mkostemps>()  are available since glibc 2.11."
msgstr ""
"B<mkostemp>() est disponible depuis la glibc 2.7. B<mkstemps>() et "
"B<mkostemps>() sont disponibles depuis la glibc 2.11."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid ""
"B<mkstemp>(),\n"
"B<mkostemp>(),\n"
"B<mkstemps>(),\n"
"B<mkostemps>()"
msgstr ""
"B<mkstemp>(),\n"
"B<mkostemp>(),\n"
"B<mkstemps>(),\n"
"B<mkostemps>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<mkstemp>(): 4.3BSD, POSIX.1-2001."
msgstr "B<mkstemp>()\\ : 4.3BSD, POSIX.1-2001."

#.  mkstemps() appears to be at least on the BSDs, Mac OS X, Solaris,
#.  and Tru64.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<mkstemps>(): unstandardized, but appears on several other systems."
msgstr ""
"B<mkstemps>()\\ : non standard, mais existe sur plusieurs autres systèmes."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<mkostemp>()  and B<mkostemps>(): are glibc extensions."
msgstr "B<mkostemp>() et B<mkostemps>() sont des extensions de la glibc."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"In glibc versions 2.06 and earlier, the file is created with permissions "
"0666, that is, read and write for all users.  This old behavior may be a "
"security risk, especially since other UNIX flavors use 0600, and somebody "
"might overlook this detail when porting programs.  POSIX.1-2008 adds a "
"requirement that the file be created with mode 0600."
msgstr ""
"Dans la version 2.06 de la glibc et auparavant, le fichier était créé en "
"mode 0666, c'est-à-dire en lecture et écriture pour tous les utilisateurs. "
"Cet ancien comportement est un trou de sécurité potentiel, surtout depuis "
"que les autres dérivés UNIX utilisent le mode 0600, et quelqu'un risque "
"d'oublier ce détail en effectuant un portage de programme. POSIX.1-2008 "
"ajoute l'obligation de créer ce fichier en mode 0600."

#.  The prototype for
#.  .BR mkstemp ()
#.  is in
#.  .I <unistd.h>
#.  for libc4, libc5, glibc1; glibc2 follows POSIX.1 and has the prototype in
#.  .IR <stdlib.h> .
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"More generally, the POSIX specification of B<mkstemp>()  does not say "
"anything about file modes, so the application should make sure its file mode "
"creation mask (see B<umask>(2))  is set appropriately before calling "
"B<mkstemp>()  (and B<mkostemp>())."
msgstr ""
"Plus généralement, la spécification POSIX de B<mkstemp>() ne dit rien des "
"modes des fichiers, ainsi les applications doivent s'assurer que la valeur "
"du masque de mode de création de fichiers (consultez B<umask>(2)) est "
"correcte avant d'appeler B<mkstemp>() (et B<mkostemp>())."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<mkdtemp>(3), B<mktemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3)"
msgstr ""
"B<mkdtemp>(3), B<mktemp>(3), B<tempnam>(3), B<tmpfile>(3), B<tmpnam>(3)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.13 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "B<int mkstemp(char *>I<template>B<);>\n"
msgstr "B<int mkstemp(char *>I<template>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "B<int mkostemp(char *>I<template>B<, int >I<flags>B<);>\n"
msgstr "B<int mkostemp(char *>I<template>B<, int >I<flags>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "B<int mkstemps(char *>I<template>B<, int >I<suffixlen>B<);>\n"
msgstr "B<int mkstemps(char *>I<template>B<, int >I<suffixlen>B<);>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "B<int mkostemps(char *>I<template>B<, int >I<suffixlen>B<, int >I<flags>B<);>\n"
msgstr "B<int mkostemps(char *>I<template>B<, int >I<suffixlen>B<, int >I<flags>B<);>\n"

#.     || _XOPEN_SOURCE\ &&\ _XOPEN_SOURCE_EXTENDED
#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Since glibc 2.12: */ _POSIX_C_SOURCE\\ E<gt>=\\ 200809L\n"
"    || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"_XOPEN_SOURCE\\ E<gt>=\\ 500\n"
"    || /* Depuis la glibc 2.12 : */ _POSIX_C_SOURCE\\ E<gt>=\\ 200809L\n"
"    || /* Versions de la glibc E<lt>= 2.19 : */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<mkostemp>(): _GNU_SOURCE"
msgstr "B<mkostemp>() : _GNU_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"B<mkstemps>():\n"
"    /* Glibc since 2.19: */ _DEFAULT_SOURCE\n"
"        || /* Glibc versions E<lt>= 2.19: */ _SVID_SOURCE || _BSD_SOURCE\n"
msgstr ""
"B<mkstemps>():\n"
"    /* Depuis la glibc 2.19 : */ _DEFAULT_SOURCE\n"
"        || /* Versions de la glibc E<lt>= 2.19 : */ _SVID_SOURCE || _BSD_SOURCE\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<mkostemps>(): _GNU_SOURCE"
msgstr "B<mkostemps>()\\ : _GNU_SOURCE"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"On success, these functions return the file descriptor of the temporary "
"file.  On error, -1 is returned, and I<errno> is set appropriately."
msgstr ""
"Si elles réussissent, ces fonctions renvoient le descripteur du fichier "
"temporaire créé ou B<-1> si elles échouent, auquel cas I<errno> contient le "
"code d'erreur."

#. type: tbl table
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"B<mkstemp>(),\n"
"B<mkostemp>(),\n"
msgstr ""
"B<mkstemp>(),\n"
"B<mkostemp>(),\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ".br\n"
msgstr ".br\n"

#. type: tbl table
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"B<mkstemps>(),\n"
"B<mkostemps>()"
msgstr ""
"B<mkstemps>(),\n"
"B<mkostemps>()"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
