# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010, 2012.
# Jean-Baptiste Holcroft <jean-baptiste@holcroft.fr>, 2018.
# Grégoire Scano <gregoire.scano@malloc.fr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2022-05-26 15:04+0200\n"
"PO-Revision-Date: 2019-08-12 14:34+0800\n"
"Last-Translator: Grégoire Scano <gregoire.scano@malloc.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "LSEARCH"
msgstr "LSEARCH"

#. type: TH
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 mars 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "GNU"
msgstr "GNU"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "lfind, lsearch - linear search of an array"
msgstr "lsearch, lfind - Recherche linéaire dans un tableau"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>search.hE<gt>>\n"
msgstr "B<#include E<lt>search.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, fuzzy, no-wrap
#| msgid ""
#| "B<void *lfind(const void *>I<key>B<, const void *>I<base>B<, size_t *>I<nmemb>B<,>\n"
#| "B<         size_t >I<size>B<, int(*>I<compar>B<)(const void *, const void *));>\n"
msgid ""
"B<void *lfind(const void *>I<key>B<, const void *>I<base>B<, size_t *>I<nmemb>B<,>\n"
"B<            size_t >I<size>B<, int(*>I<compar>B<)(const void *, const void *));>\n"
"B<void *lsearch(const void *>I<key>B<, void *>I<base>B<, size_t *>I<nmemb>B<,>\n"
"B<            size_t >I<size>B<, int(*>I<compar>B<)(const void *, const void *));>\n"
msgstr ""
"B<void *lfind(const void *>I<key>B<, const void *>I<base>B<, size_t *>I<nmemb>B<,>\n"
"B<         size_t >I<size>B<, int(*>I<compar>B<)(const void *, const void *));>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<lfind>()  and B<lsearch>()  perform a linear search for I<key> in the "
"array I<base> which has I<*nmemb> elements of I<size> bytes each.  The "
"comparison function referenced by I<compar> is expected to have two "
"arguments which point to the I<key> object and to an array member, in that "
"order, and which returns zero if the I<key> object matches the array member, "
"and nonzero otherwise."
msgstr ""
"Les fonctions B<lfind>() et B<lsearch>() effectuent une recherche linéaire "
"de la clé I<key> dans le tableau I<base> contenant I<*nmemb> éléments, "
"chacun de taille I<size> octets. La fonction de comparaison, pointée par "
"I<compar>, est supposée prendre deux arguments pointant sur l'objet I<key> "
"et sur un élément du tableau, dans cet ordre. La fonction de comparaison "
"renvoie zéro si I<key> correspond à cet élément du tableau, et une valeur "
"non nulle sinon."

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"If B<lsearch>()  does not find a matching element, then the I<key> object is "
"inserted at the end of the table, and I<*nmemb> is incremented.  In "
"particular, one should know that a matching element exists, or that more "
"room is available."
msgstr ""
"Si B<lsearch>() ne trouve pas d'élément correspondant, alors l'objet I<key> "
"est ajouté à la fin du tableau et I<*nmemb> est incrémenté. Il faut savoir "
"qu'un élément de correspondance existe, ou qu'il n'y a plus de place "
"disponible."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<lfind>()  returns a pointer to a matching member of the array, or NULL if "
"no match is found.  B<lsearch>()  returns a pointer to a matching member of "
"the array, or to the newly added member if no match is found."
msgstr ""
"B<lfind>() renvoie un pointeur sur l'élément du tableau correspondant, ou "
"NULL si aucune correspondance n'est trouvée. B<lsearch>() renvoie un "
"pointeur sur un élément correspondant du tableau, ou sinon sur le nouvel "
"élément ajouté."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr "ATTRIBUTS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""
"Pour une explication des termes utilisés dans cette section, consulter "
"B<attributes>(7)."

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr "Interface"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr "Attribut"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr "Valeur"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid ""
"B<lfind>(),\n"
"B<lsearch>()"
msgstr ""
"B<lfind>(),\n"
"B<lsearch>()"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr "Sécurité des threads"

#. type: tbl table
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr "MT-Safe"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD.  Present in libc since libc-4.6.27."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, SVr4, 4.3BSD. Fonctions présentes dans la libc "
"depuis la version 4.6.27."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "The naming is unfortunate."
msgstr "L'appellation est malheureuse."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<bsearch>(3), B<hsearch>(3), B<tsearch>(3)"
msgstr "B<bsearch>(3), B<hsearch>(3), B<tsearch>(3)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.13 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"B<void *lfind(const void *>I<key>B<, const void *>I<base>B<, size_t *>I<nmemb>B<,>\n"
"B<         size_t >I<size>B<, int(*>I<compar>B<)(const void *, const void *));>\n"
msgstr ""
"B<void *lfind(const void *>I<key>B<, const void *>I<base>B<, size_t *>I<nmemb>B<,>\n"
"B<         size_t >I<size>B<, int(*>I<compar>B<)(const void *, const void *));>\n"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid ""
"B<void *lsearch(const void *>I<key>B<, void *>I<base>B<, size_t *>I<nmemb>B<,>\n"
"B<         size_t >I<size>B<, int(*>I<compar>B<)(const void *, const void *));>\n"
msgstr ""
"B<void *lsearch(const void *>I<key>B<, void *>I<base>B<, size_t *>I<nmemb>B<,>\n"
"B<         size_t >I<size>B<, int(*>I<compar>B<)(const void *, const void *));>\n"

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
