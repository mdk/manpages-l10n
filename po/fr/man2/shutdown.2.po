# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013-2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010.
# David Prévot <david@tilapin.org>, 2010-2014.
# Jean-Philippe MENGUAL <jpmengual@debian.org>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2022-05-26 15:19+0200\n"
"PO-Revision-Date: 2022-03-27 22:17+0200\n"
"Last-Translator: Jean-Philippe MENGUAL <jpmengual@debian.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.1.1\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SHUTDOWN"
msgstr "SHUTDOWN"

#. type: TH
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 mars 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "shutdown - shut down part of a full-duplex connection"
msgstr "shutdown - Terminer une communication en full-duplex"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>sys/socket.hE<gt>>\n"
msgstr "B<#include E<lt>sys/socket.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int shutdown(int >I<sockfd>B<, int >I<how>B<);>\n"
msgstr "B<int shutdown(int >I<sockfd>B<, int >I<how>B<);>\n"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The B<shutdown>()  call causes all or part of a full-duplex connection on "
"the socket associated with I<sockfd> to be shut down.  If I<how> is "
"B<SHUT_RD>, further receptions will be disallowed.  If I<how> is B<SHUT_WR>, "
"further transmissions will be disallowed.  If I<how> is B<SHUT_RDWR>, "
"further receptions and transmissions will be disallowed."
msgstr ""
"La fonction B<shutdown>() termine tout ou partie d'une connexion full-duplex "
"sur la socket I<sockfd>. Si I<how> vaut B<SHUT_RD>, la réception est "
"désactivée. Si I<how> vaut B<SHUT_WR>, l'émission est désactivée. Si I<how> "
"vaut B<SHUT_RDWR>, l'émission et la réception sont désactivées."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"En cas de succès, zéro est renvoyé. En cas d'erreur, B<-1> est renvoyé et "
"I<errno> est définie pour préciser l'erreur."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr "B<EBADF>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "I<sockfd> is not a valid file descriptor."
msgstr "I<sockfd> n'est pas un descripteur de fichier valable."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr "B<EINVAL>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "An invalid value was specified in I<how> (but see BUGS)."
msgstr ""
"Une valeur incorrecte a été indiquée dans I<how> (mais consultez B<BOGUES>)."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTCONN>"
msgstr "B<ENOTCONN>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "The specified socket is not connected."
msgstr "La socket indiquée n'est pas connectée."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOTSOCK>"
msgstr "B<ENOTSOCK>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "The file descriptor I<sockfd> does not refer to a socket."
msgstr "Le descripteur de fichier I<sockfd> ne fait pas référence à un socket."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"POSIX.1-2001, POSIX.1-2008, 4.4BSD (B<shutdown>()  first appeared in 4.2BSD)."
msgstr ""
"POSIX.1-2001, POSIX.1-2008, 4.4BSD (B<shutdown>() est apparu dans 4.2BSD)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The constants B<SHUT_RD>, B<SHUT_WR>, B<SHUT_RDWR> have the value 0, 1, 2, "
"respectively, and are defined in I<E<lt>sys/socket.hE<gt>> since "
"glibc-2.1.91."
msgstr ""
"Les constantes B<SHUT_RD>, B<SHUT_WR>, B<SHUT_RDWR> ont pour valeur 0, 1, et "
"2 respectivement, et sont définies dans I<E<lt>sys/socket.hE<gt>> depuis la "
"glibc-2.1.91."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr "BOGUES"

#.  https://bugzilla.kernel.org/show_bug.cgi?id=47111
#.  commit fc61b928dc4d72176cf4bd4d30bf1d22e599aefc
#.  and for DECnet sockets in commit 46b66d7077b89fb4917ceef19b3f7dd86055c94a
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Checks for the validity of I<how> are done in domain-specific code, and "
"before Linux 3.7 not all domains performed these checks.  Most notably, UNIX "
"domain sockets simply ignored invalid values.  This problem was fixed for "
"UNIX domain sockets in Linux 3.7."
msgstr ""
"Les vérifications de validité pour I<how> sont faites dans le code "
"spécifique au domaine et, avant Linux 3.7, tous les domaines ne réalisaient "
"pas ces vérifications. Tout particulièrement, les sockets du domaine UNIX, "
"ignoraient simplement les valeurs incorrectes. Ce problème a été corrigé "
"pour les sockets du domaine UNIX dans Linux 3.7."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "B<close>(2), B<connect>(2), B<socket>(2), B<socket>(7)"
msgstr "B<close>(2), B<connect>(2), B<socket>(2), B<socket>(7)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.13 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-bullseye opensuse-leap-15-4
#, no-wrap
msgid "2018-04-30"
msgstr "30 avril 2018"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<#include E<lt>sys/socket.hE<gt>>"
msgstr "B<#include E<lt>sys/socket.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<int shutdown(int >I<sockfd>B<, int >I<how>B<);>"
msgstr "B<int shutdown(int >I<sockfd>B<, int >I<how>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"En cas de succès, zéro est renvoyé. En cas d'erreur, B<-1> est renvoyé et "
"I<errno> reçoit une valeur adéquate."

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
