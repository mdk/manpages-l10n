# French translation of manpages
# This file is distributed under the same license as the manpages-l10n package.
# Copyright © of this file:
# Christophe Blaess <https://www.blaess.fr/christophe/>, 1996-2003.
# Stéphan Rafin <stephan.rafin@laposte.net>, 2002.
# Thierry Vignaud <tvignaud@mandriva.com>, 1999, 2002.
# François Micaux, 2002.
# Alain Portal <aportal@univ-montp2.fr>, 2003-2008.
# Jean-Philippe Guérard <fevrier@tigreraye.org>, 2005-2006.
# Jean-Luc Coulon (f5ibh) <jean-luc.coulon@wanadoo.fr>, 2006-2007.
# Julien Cristau <jcristau@debian.org>, 2006-2007.
# Thomas Huriaux <thomas.huriaux@gmail.com>, 2006-2008.
# Nicolas François <nicolas.francois@centraliens.net>, 2006-2008.
# Florentin Duneau <fduneau@gmail.com>, 2006-2010.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2006, 2013, 2014.
# Denis Barbier <barbier@debian.org>, 2006, 2010, 2011.
# David Prévot <david@tilapin.org>, 2010, 2012-2014.
# Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: perkamon\n"
"POT-Creation-Date: 2022-06-16 17:37+0200\n"
"PO-Revision-Date: 2021-08-17 00:44+0200\n"
"Last-Translator: Jean-Pierre Giraud <jean-pierregiraud@neuf.fr>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Lokalize 20.12.0\n"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "USELIB"
msgstr "USELIB"

#. type: TH
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "2021-03-22"
msgstr "22 mars 2021"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr "Linux"

#. type: TH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr "Manuel du programmeur Linux"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NAME"
msgstr "NOM"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid "uselib - load shared library"
msgstr "uselib - Sélectionner une bibliothèque partagée"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SYNOPSIS"
msgstr "SYNOPSIS"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<#include E<lt>unistd.hE<gt>>\n"
msgstr "B<#include E<lt>unistd.hE<gt>>\n"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
#, no-wrap
msgid "B<int uselib(const char *>I<library>B<);>\n"
msgstr "B<int uselib(const char *>I<bibliothèque>B<);>\n"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"I<Note>: No declaration of this system call is provided in glibc headers; "
"see NOTES."
msgstr ""
"I<Remarque> : cet appel système n'est pas déclaré par les en-têtes de la "
"glibc ; consultez la section B<NOTES>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPTION"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The system call B<uselib>()  serves to load a shared library to be used by "
"the calling process.  It is given a pathname.  The address where to load is "
"found in the library itself.  The library can have any recognized binary "
"format."
msgstr ""
"L'appel système B<uselib>() permet de charger une bibliothèque partagée qui "
"sera utilisée par le processus appelant. Il prend un chemin de fichier comme "
"argument. L'adresse où charger la bibliothèque est trouvée dans la "
"bibliothèque elle-même. La bibliothèque peut avoir n'importe quel format de "
"binaire reconnu."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "RETURN VALUE"
msgstr "VALEUR RENVOYÉE"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-tumbleweed
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"to indicate the error."
msgstr ""
"En cas de succès, zéro est renvoyé. En cas d'erreur, B<-1> est renvoyé et "
"I<errno> est définie pour préciser l'erreur."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr "ERREURS"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"In addition to all of the error codes returned by B<open>(2)  and "
"B<mmap>(2), the following may also be returned:"
msgstr ""
"En plus de tous les codes d’erreurs renvoyés par B<open>(2) et B<mmap>(2), "
"les codes suivants peuvent aussi être renvoyés\\ :"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EACCES>"
msgstr "B<EACCES>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The library specified by I<library> does not have read or execute "
"permission, or the caller does not have search permission for one of the "
"directories in the path prefix.  (See also B<path_resolution>(7).)"
msgstr ""
"La bibliothèque indiquée par I<bibliothèque> n'est pas accessible en lecture "
"ou en écriture, ou l'appelant n'a pas la permission de parcours pour l'un "
"des répertoires du chemin. (Consultez aussi B<path_resolution>(7).)"

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<ENFILE>"
msgstr "B<ENFILE>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The system-wide limit on the total number of open files has been reached."
msgstr ""
"La limite du nombre total de fichiers ouverts pour le système entier a été "
"atteinte."

#. type: TP
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOEXEC>"
msgstr "B<ENOEXEC>"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"The file specified by I<library> is not an executable of a known type; for "
"example, it does not have the correct magic numbers."
msgstr ""
"Le fichier décrit par I<bibliothèque> n'est pas un exécutable de type connu "
"(par exemple n'a pas le bon nombre magique)."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr "CONFORMITÉ"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<uselib>()  is Linux-specific, and should not be used in programs intended "
"to be portable."
msgstr ""
"B<uselib>() est spécifique à Linux et ne devrait pas être employé dans des "
"programmes destinés à être portables."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr "NOTES"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"This obsolete system call is not supported by glibc.  No declaration is "
"provided in glibc headers, but, through a quirk of history, glibc versions "
"before 2.23 did export an ABI for this system call.  Therefore, in order to "
"employ this system call, it was sufficient to manually declare the interface "
"in your code; alternatively, you could invoke the system call using "
"B<syscall>(2)."
msgstr ""
"Cet appel système obsolète n'est pas pris en charge par la glibc. Il n'est "
"pas déclaré dans les en-têtes de la glibc mais, par un caprice de "
"l'histoire, les versions de glibc antérieures à 2.23 fournissaient une "
"interface binaire pour cet appel système. Ainsi, il suffisait de déclarer "
"manuellement l'interface dans votre code pour utiliser cet appel système. "
"Sinon, vous pouvez l'invoquer en utilisant B<syscall>(2)."

#.  .PP
#.  .\" libc 4.3.1f - changelog 1993-03-02
#.  Since libc 4.3.2, startup code tries to prefix these names
#.  with "/usr/lib", "/lib" and "" before giving up.
#.  .\" libc 4.3.4 - changelog 1993-04-21
#.  In libc 4.3.4 and later these names are looked for in the directories
#.  found in
#.  .BR LD_LIBRARY_PATH ,
#.  and if not found there,
#.  prefixes "/usr/lib", "/lib" and "/" are tried.
#.  .PP
#.  From libc 4.4.4 on only the library "/lib/ld.so" is loaded,
#.  so that this dynamic library can load the remaining libraries needed
#.  (again using this call).
#.  This is also the state of affairs in libc5.
#.  .PP
#.  glibc2 does not use this call.
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-tumbleweed
msgid ""
"In ancient libc versions (before glibc 2.0), B<uselib>()  was used to load "
"the shared libraries with names found in an array of names in the binary."
msgstr ""
"Dans les anciennes versions de la libc (avant la glibc 2.0), B<uselib>() "
"était utilisé pour charger les bibliothèques partagées dont les noms se "
"trouvaient dans un tableau de noms dans le binaire."

#.  commit 69369a7003735d0d8ef22097e27a55a8bad9557a
#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Since Linux 3.15, this system call is available only when the kernel is "
"configured with the B<CONFIG_USELIB> option."
msgstr ""
"Depuis Linux 3.15, cet appel système n'est disponible que si le noyau a été "
"configuré avec l'option B<CONFIG_USELIB>."

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "SEE ALSO"
msgstr "VOIR AUSSI"

#. type: Plain text
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"B<ar>(1), B<gcc>(1), B<ld>(1), B<ldd>(1), B<mmap>(2), B<open>(2), "
"B<dlopen>(3), B<capabilities>(7), B<ld.so>(8)"
msgstr ""
"B<ar>(1), B<gcc>(1), B<ld>(1), B<ldd>(1), B<mmap>(2), B<open>(2), "
"B<dlopen>(3), B<capabilities>(7), B<ld.so>(8)"

#. type: SH
#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4
#, no-wrap
msgid "COLOPHON"
msgstr "COLOPHON"

#. type: Plain text
#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.13 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: debian-bullseye
#, no-wrap
msgid "2020-12-21"
msgstr "21 décembre 2020"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<#include E<lt>unistd.hE<gt>>"
msgstr "B<#include E<lt>unistd.hE<gt>>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid "B<int uselib(const char *>I<library>B<);>"
msgstr "B<int uselib(const char *>I<bibliothèque>B<);>"

#. type: Plain text
#: debian-bullseye opensuse-leap-15-4
msgid ""
"On success, zero is returned.  On error, -1 is returned, and I<errno> is set "
"appropriately."
msgstr ""
"En cas de succès, zéro est renvoyé. En cas d'erreur, B<-1> est renvoyé et "
"I<errno> reçoit une valeur adéquate."

#. type: Plain text
#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 5.10 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."

#. type: TH
#: opensuse-leap-15-4
#, no-wrap
msgid "2017-09-15"
msgstr "15 septembre 2017"

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"In ancient libc versions, B<uselib>()  was used to load the shared libraries "
"with names found in an array of names in the binary."
msgstr ""
"Dans les anciennes versions de la libc, B<uselib>() était utilisé pour "
"charger les bibliothèques partagées dont les noms se trouvaient dans un "
"tableau de noms dans le binaire."

#.  libc 4.3.1f - changelog 1993-03-02
#.  libc 4.3.4 - changelog 1993-04-21
#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"Since libc 4.3.2, startup code tries to prefix these names with \"/usr/"
"lib\", \"/lib\" and \"\" before giving up.  In libc 4.3.4 and later these "
"names are looked for in the directories found in B<LD_LIBRARY_PATH>, and if "
"not found there, prefixes \"/usr/lib\", \"/lib\" and \"/\" are tried."
msgstr ""
"Depuis la libc 4.3.2, le code de démarrage essaie de préfixer ces noms avec "
"\"/usr/lib\", \"/lib\" puis \"\" avant d'abandonner. À partir de la "
"libc 4.3.4, ces noms sont recherchés dans les répertoires listés dans la "
"variable B<LD_LIBRARY_PATH> et les préfixes \"/usr/lib\", \"/lib\" et \"/\" "
"sont essayés ensuite si la bibliothèque n'a pas été trouvée."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"From libc 4.4.4 on only the library \"/lib/ld.so\" is loaded, so that this "
"dynamic library can load the remaining libraries needed (again using this "
"call).  This is also the state of affairs in libc5."
msgstr ""
"À partir de la libc 4.4.4, seule la bibliothèque «\\ /lib/ld.so\\ » est "
"chargée, afin que cette bibliothèque dynamique puisse charger les autres "
"bibliothèques requises (à nouveau avec cette fonction). Cela est également "
"valable pour la libc5."

#. type: Plain text
#: opensuse-leap-15-4
msgid "glibc2 does not use this call."
msgstr "La glibc2 n'utilise pas cette fonction."

#. type: Plain text
#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""
"Cette page fait partie de la publication 4.16 du projet I<man-pages> Linux. "
"Une description du projet et des instructions pour signaler des anomalies et "
"la dernière version de cette page peuvent être trouvées à l'adresse \\"
"%https://www.kernel.org/doc/man-pages/."
