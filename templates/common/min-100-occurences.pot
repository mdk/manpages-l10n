# Common msgids
msgid ""
msgstr ""
"Project-Id-Version: manpages-l10n\n"
"POT-Creation-Date: 2022-06-16 17:51+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: debian-bullseye debian-unstable archlinux fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed mageia-cauldron
#, no-wrap
msgid "*"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ATTRIBUTES"
msgstr ""

#: debian-bullseye debian-unstable fedora-36 fedora-rawhide opensuse-leap-15-4
#: opensuse-tumbleweed mageia-cauldron archlinux
#, no-wrap
msgid "AUTHOR"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "AUTHORS"
msgstr ""

#: archlinux fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed debian-bullseye debian-unstable
#, no-wrap
msgid "AVAILABILITY"
msgstr ""

#: debian-unstable fedora-rawhide opensuse-tumbleweed archlinux mageia-cauldron
#: opensuse-leap-15-4
#, no-wrap
msgid "April 2022"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Attribute"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<--help>"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "B<--version>"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-V>, B<--version>"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<-v>, B<--verbose>"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EBADF>"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EFAULT>"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EINVAL>"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<ENOMEM>"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "B<EPERM>"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "BUGS"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 mageia-cauldron opensuse-tumbleweed
#, no-wrap
msgid "COLOPHON"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "CONFORMING TO"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "COPYRIGHT"
msgstr ""

#: debian-bullseye debian-unstable opensuse-leap-15-4 archlinux fedora-36
#: fedora-rawhide mageia-cauldron opensuse-tumbleweed
msgid ""
"Copyright \\(co 2020 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#: fedora-36 mageia-cauldron archlinux fedora-rawhide opensuse-tumbleweed
msgid ""
"Copyright \\(co 2021 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#: fedora-rawhide opensuse-tumbleweed archlinux
msgid ""
"Copyright \\(co 2022 Free Software Foundation, Inc.  License GPLv3+: GNU GPL "
"version 3 or later E<lt>https://gnu.org/licenses/gpl.htmlE<gt>."
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#: archlinux fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed debian-bullseye debian-unstable
msgid "Display help text and exit."
msgstr ""

#: fedora-36 opensuse-leap-15-4 opensuse-tumbleweed debian-bullseye archlinux
#: debian-unstable fedora-rawhide mageia-cauldron
msgid "Display version information and exit."
msgstr ""

#: debian-bullseye debian-unstable mageia-cauldron archlinux fedora-36
#: fedora-rawhide opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ENVIRONMENT"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "ERRORS"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "EXAMPLE"
msgstr ""

#: debian-bullseye debian-unstable mageia-cauldron archlinux fedora-36
#: fedora-rawhide opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "EXAMPLES"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "EXIT STATUS"
msgstr ""

#: debian-bullseye debian-unstable fedora-36 fedora-rawhide opensuse-leap-15-4
#: opensuse-tumbleweed archlinux mageia-cauldron
#, no-wrap
msgid "FILES"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"Feature Test Macro Requirements for glibc (see B<feature_test_macros>(7)):"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
msgid ""
"For an explanation of the terms used in this section, see B<attributes>(7)."
msgstr ""

#: archlinux fedora-36 fedora-rawhide mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed debian-unstable
msgid "For bug reports, use the issue tracker at"
msgstr ""

#: debian-bullseye debian-unstable opensuse-leap-15-4
#, no-wrap
msgid "GNU coreutils 8.32"
msgstr ""

#: fedora-36 mageia-cauldron
#, no-wrap
msgid "GNU coreutils 9.0"
msgstr ""

#: fedora-rawhide opensuse-tumbleweed archlinux
#, no-wrap
msgid "GNU coreutils 9.1"
msgstr ""

#: debian-bullseye debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed archlinux
msgid ""
"GNU coreutils online help: E<lt>https://www.gnu.org/software/coreutils/E<gt>"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Interface"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Linux"
msgstr ""

#: debian-bullseye debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed archlinux
#, no-wrap
msgid "Linux Programmer's Manual"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "MT-Safe"
msgstr ""

#: fedora-36 debian-unstable fedora-rawhide mageia-cauldron
#, no-wrap
msgid "March 2022"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "NAME"
msgstr ""

#: archlinux debian-bullseye fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 debian-unstable opensuse-tumbleweed
#, no-wrap
msgid "NOTES"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "OPTIONS"
msgstr ""

#: debian-unstable opensuse-leap-15-4 fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "October 2021"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: opensuse-leap-15-4 opensuse-tumbleweed mageia-cauldron
msgid "POSIX.1-2001, POSIX.1-2008."
msgstr ""

#: archlinux debian-bullseye debian-unstable opensuse-leap-15-4
#: opensuse-tumbleweed fedora-rawhide mageia-cauldron fedora-36
msgid "Print version and exit."
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "RETURN VALUE"
msgstr ""

#: debian-bullseye debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed archlinux
msgid ""
"Report any translation bugs to E<lt>https://translationproject.org/team/E<gt>"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#: debian-bullseye
#, no-wrap
msgid "September 2020"
msgstr ""

#: debian-bullseye opensuse-leap-15-4 opensuse-tumbleweed mageia-cauldron
#: archlinux fedora-36 fedora-rawhide
#, no-wrap
msgid "September 2021"
msgstr ""

#: debian-bullseye debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed archlinux
msgid ""
"This is free software: you are free to change and redistribute it.  There is "
"NO WARRANTY, to the extent permitted by law."
msgstr ""

#: opensuse-leap-15-4
msgid ""
"This page is part of release 4.16 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#: debian-bullseye
msgid ""
"This page is part of release 5.10 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#: archlinux debian-unstable fedora-36 fedora-rawhide mageia-cauldron
msgid ""
"This page is part of release 5.13 of the Linux I<man-pages> project.  A "
"description of the project, information about reporting bugs, and the latest "
"version of this page, can be found at \\%https://www.kernel.org/doc/man-"
"pages/."
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Thread safety"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
#, no-wrap
msgid "User Commands"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "VERSIONS"
msgstr ""

#: archlinux debian-bullseye debian-unstable fedora-36 fedora-rawhide
#: mageia-cauldron opensuse-leap-15-4 opensuse-tumbleweed
#, no-wrap
msgid "Value"
msgstr ""

#: debian-bullseye debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed archlinux
msgid "display this help and exit"
msgstr ""

#: debian-bullseye debian-unstable fedora-36 fedora-rawhide mageia-cauldron
#: opensuse-leap-15-4 opensuse-tumbleweed archlinux
msgid "output version information and exit"
msgstr ""

#: archlinux debian-bullseye debian-unstable mageia-cauldron opensuse-leap-15-4
#: opensuse-tumbleweed fedora-36 fedora-rawhide
msgid "should give you access to the complete manual."
msgstr ""

#: opensuse-leap-15-4
#, no-wrap
msgid "systemd 249"
msgstr ""

#: debian-bullseye fedora-36 opensuse-tumbleweed
#, no-wrap
msgid "systemd 250"
msgstr ""

#: archlinux fedora-rawhide mageia-cauldron debian-unstable
#, no-wrap
msgid "systemd 251"
msgstr ""

#: debian-bullseye
#, no-wrap
msgid "util-linux"
msgstr ""

#: opensuse-leap-15-4
#, no-wrap
msgid "util-linux 2.37.2"
msgstr ""

#: opensuse-tumbleweed
#, no-wrap
msgid "util-linux 2.37.4"
msgstr ""

#: archlinux fedora-rawhide mageia-cauldron debian-unstable
#, no-wrap
msgid "util-linux 2.38"
msgstr ""

#: fedora-36
#, no-wrap
msgid "util-linux 2.38-rc1"
msgstr ""
