# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2022-06-16 17:09+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "GRUB-SET-PASSWORD"
msgstr ""

#. type: TH
#: fedora-36
#, no-wrap
msgid "Thu Jun 25 2015"
msgstr ""

#. type: SH
#: fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "NAME"
msgstr ""

#. type: Plain text
#: fedora-36
msgid ""
"B<grub-set-password> \\(em Generate the user.cfg file containing the hashed "
"grub bootloader password."
msgstr ""

#. type: SH
#: fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SYNOPSIS"
msgstr ""

#. type: Plain text
#: fedora-36
msgid "B<grub-set-password> [OPTION]"
msgstr ""

#. type: SH
#: fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "DESCRIPTION"
msgstr ""

#. type: Plain text
#: fedora-36
msgid ""
"B<grub-set-password> outputs the user.cfg file which contains the hashed "
"GRUB bootloader password. This utility only supports configurations where "
"there is a single root user."
msgstr ""

#. type: Plain text
#: fedora-36
msgid "The file has the format: GRUB2_PASSWORD=E<lt>I<hashed password>E<gt>."
msgstr ""

#. type: SH
#: fedora-36
#, no-wrap
msgid "OPTIONS"
msgstr ""

#. type: TP
#: fedora-36
#, no-wrap
msgid "-h, --help"
msgstr ""

#. type: Plain text
#: fedora-36
msgid "Display program usage and exit."
msgstr ""

#. type: TP
#: fedora-36
#, no-wrap
msgid "-v, --version"
msgstr ""

#. type: Plain text
#: fedora-36
msgid "Display the current version."
msgstr ""

#. type: TP
#: fedora-36
#, no-wrap
msgid "-o, --output=E<lt>I<DIRECTORY>E<gt>"
msgstr ""

#. type: Plain text
#: fedora-36
msgid "Choose the file path to which user.cfg will be written."
msgstr ""

#. type: SH
#: fedora-36 fedora-rawhide mageia-cauldron
#, no-wrap
msgid "SEE ALSO"
msgstr ""

#. type: Plain text
#: fedora-36
msgid "B<info grub>"
msgstr ""

#. type: Plain text
#: fedora-36
msgid "B<info grub2-mkpasswd-pbkdf2>"
msgstr ""

#. type: TH
#: fedora-rawhide
#, no-wrap
msgid "May 2022"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "The grub bootloader password may only be set by root."
msgstr ""

#. type: TH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "System Administration Utilities"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"grub-set-password - generate the user.cfg file containing the hashed grub "
"bootloader password"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<grub-set-password> [I<\\,OPTION\\/>]"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"\\&./grub-set-password prompts the user to set a password on the grub "
"bootloader. The password is written to a file named user.cfg which lives in "
"the GRUB directory located by default at I<\\,/boot/grub2\\/>."
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-h>, B<--help>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "print this message and exit"
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-v>, B<--version>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "print the version information and exit"
msgstr ""

#. type: TP
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "B<-o>, B<--output_path> E<lt>DIRECTORYE<gt>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "put user.cfg in a user-selected directory"
msgstr ""

#. type: SH
#: fedora-rawhide mageia-cauldron
#, no-wrap
msgid "REPORTING BUGS"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "Report bugs at https://bugzilla.redhat.com."
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"Usage: ./grub-set-password [OPTION] \\&./grub-set-password prompts the user "
"to set a password on the grub bootloader. The password is written to a file "
"named user.cfg which lives in the GRUB directory located by default at I<\\,/"
"boot/grub2\\/>."
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid ""
"The full documentation for B<grub-set-password> is maintained as a Texinfo "
"manual.  If the B<info> and B<grub-set-password> programs are properly "
"installed at your site, the command"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "B<info grub-set-password>"
msgstr ""

#. type: Plain text
#: fedora-rawhide mageia-cauldron
msgid "should give you access to the complete manual."
msgstr ""

#. type: TH
#: mageia-cauldron
#, no-wrap
msgid "June 2022"
msgstr ""
